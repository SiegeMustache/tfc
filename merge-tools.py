import sys
import os
import subprocess
import shutil
import requests
import logging
import io
import json

# gmr server projectId sourceBranch targetBranch title token 

try:
    logging.basicConfig(filename='merge.log', level=logging.INFO)
    if len(sys.argv) == 8:
        if sys.argv[1] == 'gmr':
            host = sys.argv[2]         
            projectId = sys.argv[3]
            sourceBranch = sys.argv[4]
            targetBranch = sys.argv[5]
            title = sys.argv[6]
            token = sys.argv[7]
            
            requestUrl = 'https://' + host + '/api/v4/projects/' + projectId + '/'
            logging.info(requestUrl)

            endPoint = requestUrl + 'merge_requests'            
            logging.info(endPoint)

            headers = {
                'Authorization': "Bearer " + token,
                'Content-Type': 'application/json'
            }

            payload = {
                'id': projectId,
                'source_branch': sourceBranch,
                'target_branch': targetBranch,
                'title': title
            }

            logging.info(endPoint)
            logging.info(payload)
            logging.info(headers)
            r = requests.post(url = endPoint, headers = headers, json = payload)
            logging.info(r)

            if r.status_code == 201:
                responseDict = json.loads(r.text)
                if responseDict['has_conflicts'] == True:
                    raise Exception('Unable to automatically merge')
                else:
                    endPoint = endPoint + "/" + str(responseDict['iid']) + "/merge"
                    approvePayload = {
                        'id': projectId,
                        'merge_request_iid': responseDict['iid']
                    }

                    logging.info(endPoint)
                    logging.info(approvePayload)
                    logging.info(headers)
                    r = requests.put(url = endPoint, headers = headers, json = approvePayload)
                    logging.info(r)

                    if r.status_code != 200:
                        raise Exception('Unable to approve merge request')
            else:
                raise Exception('Unable to create merge request')
        else:
            raise Exception('Unvalid Arguments gmr')
    else:
        raise Exception('Unvalid Arguments length')
except Exception as e:
    logging.error(str(e))
    sys.exit(-1)
sys.exit(0)