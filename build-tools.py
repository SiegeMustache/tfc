import sys
import os
import subprocess
import shutil
import requests
import json
import logging
import io

def execute(cmd):
    output = ""
    popen = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    for stdout_line in io.TextIOWrapper(popen.stdout, encoding="utf8"):
        value = stdout_line
        logging.info(value)
        output += value
    popen.stdout.close()
    return_code = popen.wait()
    if return_code:
        cmdOutput = ""
        for singleCmd in cmd:
            cmdOutput += singleCmd + " "
        raise subprocess.CalledProcessError(return_code, cmdOutput + "\n" + output)
    return output

def executeNoOutput(cmd):
    popen = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    return_code = popen.wait()
    if return_code:
        raise subprocess.CalledProcessError(return_code, cmd)
    
try:
    logging.basicConfig(filename='deploy.log', level=logging.INFO)
    if len(sys.argv) == 6:
        if sys.argv[1] == "build":
            buildName = sys.argv[2]
            buildFolder = "./Builds/" + buildName
            logging.info('Staring build process ...')
            logging.info('Creating builds directory ...')
            os.mkdir("./Builds")
            os.mkdir(buildFolder)     
            logging.info('Building')

            cmd = ['Unity', '-batchmode', '-projectPath', os.getcwd(), '-buildWindows64Player', buildFolder + "/" + buildName + ".exe" ,'-quit']
            execute(cmd)      
            logging.info('Get actual version value')

            targetItchProject = sys.argv[4]

            cmd = ['butler', 'status', targetItchProject + ":win"]
            totalOutput = execute(cmd).split("|")
            version = "0.0.0"
            for i in range(0, len(totalOutput)):
                if totalOutput[i].replace(" ", "") == "win":
                    version = totalOutput[i + 3].replace(" ", "")
                    logging.info(version)
                    break

            splittedVersion = version.split(".")

            major = int(splittedVersion[0])
            minor = int(splittedVersion[1])
            patch = int(splittedVersion[2])

            if sys.argv[3] == "patch":
                patch += 1
            elif sys.argv[3] == "minor":
                patch = 0
                minor += 1
            elif sys.argv[3] == "major":
                patch = 0
                minor = 0
                major += 1
            else:
                raise Exception('Unvalid Arguments')

            newVersion = str(major) + "." + str(minor) + "." + str(patch)
            logging.info('New calculated version: ' + newVersion)

            logging.info('Pushing to itch')
            cmd = ['butler', 'push', buildFolder, targetItchProject + ":win", '--userversion', newVersion]
            execute(cmd)
            logging.info('Done deploy to internal project')

            discordWebHookURL = sys.argv[5]
            data = {}
            data["content"] = "Hey there, a new build of " + buildName + " has been releseasd"

            data["embeds"] = []
            embed = {}
            embed["title"] = "Grab it here"
            splitProject = targetItchProject.split("/")
            embed["description"] = "https://" + splitProject[0] + ".itch.io/" + splitProject[1]
            data["embeds"].append(embed)

            result = requests.post(discordWebHookURL, data=json.dumps(data), headers={"Content-Type": "application/json"})
            result.raise_for_status()
        elif sys.argv[1] == "deploy":  
            logging.info('Starting deploy process')

            buildName = sys.argv[2]
            sourceProject = sys.argv[3]
            targetProject = sys.argv[4]
            logging.info('Collecting latest test build for ' + targetProject)

            cmd = ['butler', 'fetch', sourceProject + ":win", buildName]
            execute(cmd)

            logging.info('Acquiring build version')

            buildVersion = "0.0.0"

            cmd = ['butler', 'status', sourceProject + ":win"]
            totalOutput = execute(cmd).split("|")
            for i in range(0, len(totalOutput)):
                logging.info(totalOutput[i])
                if totalOutput[i].replace(" ", "") == "win":
                    buildVersion = totalOutput[i + 3].replace(" ", "")
                    logging.info('Calculated version ' + buildVersion)
                    break

            logging.info('Uploading build to release project')
            cmd = ['butler', 'push', "./" + buildName, targetProject + ":win", "--userversion", buildVersion]
            execute(cmd)

            logging.info('Notifying discord')
            discordWebHookURL = sys.argv[5]
            data = {}
            data["content"] = "Hey there, a new build of " + buildName + " has been releseasd"

            data["embeds"] = []
            embed = {}
            embed["title"] = "Grab it here"
            splitProject = targetProject.split("/")
            embed["description"] = "https://" + splitProject[0] + ".itch.io/" + splitProject[1]
            data["embeds"].append(embed)

            result = requests.post(discordWebHookURL, data=json.dumps(data), headers={"Content-Type": "application/json"})
            result.raise_for_status()
        else:
            raise Exception('Unvalid Arguments')   
    else:
        raise Exception('Unvalid Arguments')    
except Exception as e:
    logging.error(str(e))
    sys.exit(-1)
sys.exit(0)
