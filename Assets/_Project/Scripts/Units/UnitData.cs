﻿using System;
using TorkFramework.Serialization;
using UnityEngine;

namespace Kairos.Units
{
    [Serializable]
    public class UnitData
    {
        [XmlSerialized, SerializeField] private string m_TargetUnitId;
        [XmlSerialized, SerializeField] private int m_ActualLifePoints;

        public int ActualLifePoints
        {
            get => m_ActualLifePoints;
            set => m_ActualLifePoints = Mathf.Clamp(value, 0, TargetUnit.MaxLifePoints);
        }

        public UnitInfo TargetUnit => UnitDatabase.LoadedUnits[m_TargetUnitId];

        public UnitData()
        {
            m_TargetUnitId = "";
            m_ActualLifePoints = 0;
        }
        
        public UnitData(UnitInfo targetUnit)
        {
            m_TargetUnitId = targetUnit.UnitID;
            m_ActualLifePoints = targetUnit.MaxLifePoints;
        }
    }
}