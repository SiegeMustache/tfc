﻿using System.IO;
using TorkFramework.Editor.Utility;
using TorkFramework.ResourceManagement;
using UnityEditor;
using UnityEngine;

namespace Kairos.Units.Editor
{
    public class UnitEditorWindow : EditorWindow
    {
        private string m_SelectedUnitPath;
        private Vector2 m_ScrollPosition;
        
        public UnitInfo SelectedUnit;

        [MenuItem("Custom/Units/Reload Database")]
        public static void ReloadUnitDatabase()
        {
            UnitDatabase.Reload();
        }

        [MenuItem("Custom/Units/New Unit  %#u")]
        public static void CreateNewUnit()
        {
            var newUnit = UnitHelper.GenerateNewUnit();
            UnitHelper.SaveUnit(newUnit, Globals._UNITS_FOLDER+"/newUnit.unit");
            UnitDatabase.Reload();
        }
        
        [MenuItem("Custom/Units/Editor")]
        public static void Init()
        {
            var window = GetWindow<UnitEditorWindow>();
            window.name = "Unit Editor";
            window.titleContent = new GUIContent("Unit Editor");
            window.Show();
        }

        private void OnEnable()
        {
            OnSelectionChange();
        }

        private void OnSelectionChange()
        {
            if (Selection.objects.Length == 1 && Path.GetExtension(AssetDatabase.GetAssetPath(Selection.objects[0])).ToLower() == ".unit")
            {
                m_SelectedUnitPath = Application.dataPath.Substring(0, Application.dataPath.Length - 6) + AssetDatabase.GetAssetPath(Selection.objects[0]);
                SelectedUnit = UnitHelper.LoadUnit(m_SelectedUnitPath);
            }
            
            Repaint();
        }

        private void OnGUI()
        {
            if(SelectedUnit == null)
                return;

            m_ScrollPosition = EditorGUILayout.BeginScrollView(m_ScrollPosition);
            
            EditorGUI.BeginDisabledGroup(true);
            FieldUtils.AdaptiveField("Id", SelectedUnit.UnitID);
            EditorGUI.EndDisabledGroup();

            SelectedUnit.UnitName = FieldUtils.AdaptiveField("Name", SelectedUnit.UnitName) as string;
            SelectedUnit.Lore = FieldUtils.AdaptiveField("Lore", SelectedUnit.Lore) as string;
            SelectedUnit.MaxLifePoints = (int) FieldUtils.AdaptiveField("Life Points", SelectedUnit.MaxLifePoints);
            SelectedUnit.UnitPrefab = FieldUtils.AdaptiveField("Prefab", SelectedUnit.UnitPrefab) as ResourceReference;
            SelectedUnit.UnitRender = FieldUtils.AdaptiveField("Render", SelectedUnit.UnitRender) as ResourceReference;
            SelectedUnit.UnitIcon = FieldUtils.AdaptiveField("Icon", SelectedUnit.UnitIcon) as ResourceReference;
            SelectedUnit.HeroIcon = FieldUtils.AdaptiveField("Hero Icon", SelectedUnit.HeroIcon) as ResourceReference;
            
            GUI.color = Color.green;
            if(GUILayout.Button("Save", GUILayout.ExpandWidth(false)))
            {
                UnitHelper.SaveUnit(SelectedUnit, m_SelectedUnitPath);
                Repaint();
            }
            
            EditorGUILayout.EndScrollView();
        }
    }
}