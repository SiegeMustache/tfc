﻿using System;
using Kairos.Controllers;
using Kairos.Statuses;
using TorkFramework;
using TorkFramework.Events;
using TorkFramework.VFX;
using UnityEngine;

namespace Kairos.Units
{
    public enum Team
    {
        Player,
        Enemy
    }

    public enum UnitType
    {
        Hero,
        Sentinel,
        Enemy
    }

    public class UnitController : EntityBehaviour<GameEntity>
    {
        #region Fields

        [SerializeField] private Team m_Team;

        #endregion

        #region Private Variables

        [SerializeField] private UnitData m_ReferencedUnit;
        private StatModifierController m_StrengthModifierController = new StatModifierController();
        private StatModifierController m_ArmorModifierController = new StatModifierController();
        private StatModifierController m_SpeedModifierController = new StatModifierController();
        private StatModifierController m_HealingModifierController = new StatModifierController();
        private SentinelModifierController m_SentinelDamageBoostController = new SentinelModifierController();
        private int m_Poison = 0;
        private bool m_IsDead;
        private System.Collections.Generic.List<int> m_AppliedStatusIndexes = new System.Collections.Generic.List<int>();

        #endregion

        #region Properties

        public UnitData ReferencedUnit
        {
            get => m_ReferencedUnit;
            set { m_ReferencedUnit = value; }
        }

        public int StrengthModifier => m_StrengthModifierController.GetModifier();
        public int ArmorModifier => m_ArmorModifierController.GetModifier();
        public int SpeedModifer => m_SpeedModifierController.GetModifier();
        public int HealingModifier => m_HealingModifierController.GetModifier();
        public bool CheckSentinelDamageBoost(int timeframe) => m_SentinelDamageBoostController.CheckModifier(timeframe);
        public int GetSentinelDamageBoostAmount(int timeframe) => m_SentinelDamageBoostController.GetModifierAmount(timeframe);
        public int Poison => m_Poison;
        public bool IsDead => m_IsDead;
        public bool DisableCardPlay { get; set; } = false;

        public UnitType Type { get; set; }
        public BoardController Board { get; set; }
        public int TurnTakedDamage { get; private set; } = 0;

        public Team Team
        {
            get => m_Team;
            set => m_Team = value;
        }

        #endregion

        #region Interactions

        public void DealDamage(UnitController source, int amount, Action damageTakeDoneCallback)
        {
            if (IsDead)
            {
                damageTakeDoneCallback?.Invoke();
                return;
            }

            var effectiveDamage = amount + source.StrengthModifier - ArmorModifier;
            effectiveDamage = effectiveDamage < 0 ? 0 : effectiveDamage;
            Entity.GameManager.GetManager<EventManager>().TriggerEvent(EventId.UnitTakeDamage, this, () =>
            {
                ReferencedUnit.ActualLifePoints -= effectiveDamage;
                TurnTakedDamage += effectiveDamage;
                UnitStatsUpdated(() => { CheckDeath(damageTakeDoneCallback); });
            }, effectiveDamage);
        }

        public void DealDamageNoSource(int amount, Action damageTakenCallback)
        {
            if (IsDead)
            {
                damageTakenCallback?.Invoke();
                return;
            }

            var effectiveDamage = amount - ArmorModifier;
            effectiveDamage = effectiveDamage < 0 ? 0 : effectiveDamage;
            Entity.GameManager.GetManager<EventManager>().TriggerEvent(EventId.UnitTakeDamage, this, () =>
            {
                ReferencedUnit.ActualLifePoints -= effectiveDamage;
                TurnTakedDamage += effectiveDamage;
                UnitStatsUpdated(() =>
                    {
                        if (ReferencedUnit.ActualLifePoints <= 0)
                        {
                            m_IsDead = true;
                            gameObject.SetActive(false);
                            Entity.GameManager.GetManager<EventManager>().TriggerEvent(EventId.UnitDeath, this, damageTakenCallback);
                            damageTakenCallback?.Invoke();
                        }
                        else
                        {
                            damageTakenCallback?.Invoke();
                        }
                    }
                );
            }, effectiveDamage);
        }

        public void Heal(UnitController source, int amount, Action healDoneCallback)
        {
            if (IsDead)
            {
                healDoneCallback?.Invoke();
                return;
            }

            var effectiveHeal = amount + HealingModifier;
            effectiveHeal = effectiveHeal < 0 ? 0 : effectiveHeal;
            Entity.GameManager.GetManager<EventManager>().TriggerEvent(EventId.UnitHeal, this, () =>
                {
                    ReferencedUnit.ActualLifePoints = Mathf.Clamp(ReferencedUnit.ActualLifePoints + effectiveHeal, 0, ReferencedUnit.TargetUnit.MaxLifePoints);
                    UnitStatsUpdated(healDoneCallback);
                    //Entity.GameManager.GetManager<EventManager>().TriggerEvent(EventId.UnitStatsUpdated, this, healDoneCallback);
                }
            );
        }

        public void HealNoSource(int amount, Action healDoneCallback)
        {
            if (IsDead)
            {
                healDoneCallback?.Invoke();
                return;
            }

            var effectiveHeal = amount + HealingModifier;
            effectiveHeal = effectiveHeal < 0 ? 0 : effectiveHeal;
            Entity.GameManager.GetManager<EventManager>().TriggerEvent(EventId.UnitHeal, this, () =>
                {
                    ReferencedUnit.ActualLifePoints = Mathf.Clamp(ReferencedUnit.ActualLifePoints + effectiveHeal, 0, ReferencedUnit.TargetUnit.MaxLifePoints);
                    UnitStatsUpdated(healDoneCallback);
                }
            );
        }

        public void AddPoison(UnitController source, int amount, Action poisonAppliedCallback)
        {
            if (IsDead)
            {
                poisonAppliedCallback?.Invoke();
                return;
            }

            Entity.GameManager.GetManager<EventManager>().TriggerEvent(EventId.UnitPoisonTake, this, () =>
                {
                    m_Poison += amount;
                    UnitStatsUpdated(poisonAppliedCallback);
                }
            );
        }

        public void AddStrengthModifier(UnitController source, int amount, int turns, Action modiferAppliedCallback)
        {
            if (IsDead)
            {
                modiferAppliedCallback?.Invoke();
                return;
            }

            Entity.GameManager.GetManager<EventManager>().TriggerEvent(EventId.UnitStrengthModifierApplied, this, () =>
                {
                    m_StrengthModifierController.AddModifer(amount, turns);
                    UnitStatsUpdated(modiferAppliedCallback);
                    //Entity.GameManager.GetManager<EventManager>().TriggerEvent(EventId.UnitStatsUpdated, this, modiferAppliedCallback);
                }
            );
        }

        public void AddArmorModifier(UnitController source, int amount, int turns, Action modiferAppliedCallback)
        {
            if (IsDead)
            {
                modiferAppliedCallback?.Invoke();
                return;
            }

            Entity.GameManager.GetManager<EventManager>().TriggerEvent(EventId.UnitArmorModifierApplied, this, () =>
                {
                    m_ArmorModifierController.AddModifer(amount, turns);
                    UnitStatsUpdated(modiferAppliedCallback);
                    //Entity.GameManager.GetManager<EventManager>().TriggerEvent(EventId.UnitStatsUpdated, this, modiferAppliedCallback);
                }
            );
        }

        public void AddSpeedModifier(UnitController source, int amount, int turns, Action modiferAppliedCallback)
        {
            if (IsDead)
            {
                modiferAppliedCallback?.Invoke();
                return;
            }

            Entity.GameManager.GetManager<EventManager>().TriggerEvent(EventId.UnitSpeedModifierApplied, this, () =>
                {
                    m_SpeedModifierController.AddModifer(amount, turns);
                    UnitStatsUpdated(modiferAppliedCallback);
                    //Entity.GameManager.GetManager<EventManager>().TriggerEvent(EventId.UnitStatsUpdated, this, modiferAppliedCallback);
                }
            );
        }

        public void AddHealingModifier(UnitController source, int amount, int turns, Action modiferAppliedCallback)
        {
            if (IsDead)
            {
                modiferAppliedCallback?.Invoke();
                return;
            }

            Entity.GameManager.GetManager<EventManager>().TriggerEvent(EventId.UnitHealingModifierApplied, this, () =>
                {
                    m_HealingModifierController.AddModifer(amount, turns);
                    UnitStatsUpdated(modiferAppliedCallback);
                    //Entity.GameManager.GetManager<EventManager>().TriggerEvent(EventId.UnitStatsUpdated, this, modiferAppliedCallback);
                }
            );
        }

        public void AddSentinelBoostModifier(int amount, int targetTimeframe, Action modifierAppliedCallback)
        {
            if (IsDead)
            {
                modifierAppliedCallback?.Invoke();
                return;
            }

            m_SentinelDamageBoostController.AddModifier(amount, targetTimeframe);
            modifierAppliedCallback?.Invoke();
        }

        #endregion

        #region Life Cycle

        private void OnEnable()
        {
            Entity.GameManager.GetManager<EventManager>().AddListener(EventId.TurnEnded, OnTurnEnded);
        }

        private void OnDisable()
        {
            Entity.GameManager.GetManager<EventManager>().RemoveListener(EventId.TurnEnded, OnTurnEnded);
        }

        private void OnTurnEnded(GenericEventArgs args, Action listenerExecutedCallback)
        {
            if (IsDead)
            {
                listenerExecutedCallback?.Invoke();
                return;
            }

            ApplyPoison(() =>
                {
                    UpdateModifiers();
                    UnitStatsUpdated(listenerExecutedCallback);
                    DisableCardPlay = false;
                    TurnTakedDamage = 0;
                }
            );
        }

        private void ApplyPoison(Action poisonAppliedCallback)
        {
            if (Poison <= 0)
            {
                poisonAppliedCallback?.Invoke();
                return;
            }

            Entity.GameManager.GetManager<EventManager>().TriggerEvent(EventId.UnitPoisonApplied, this, () =>
            {
                ReferencedUnit.ActualLifePoints -= Poison;
                m_Poison--;

                Entity.GameManager.GetManager<EventManager>().TriggerEvent(EventId.UnitTakeDamage, this, () => { UnitStatsUpdated(() => { CheckDeath(poisonAppliedCallback); }); }, m_Poison + 1);
            });
        }

        private void UpdateModifiers()
        {
            m_StrengthModifierController.UpdateController();
            m_ArmorModifierController.UpdateController();
            m_SpeedModifierController.UpdateController();
            m_HealingModifierController.UpdateController();
            m_SentinelDamageBoostController.ClearModifiers();
        }

        private void UnitStatsUpdated(Action updatedCallback)
        {
            foreach (var appliedStatusIndex in m_AppliedStatusIndexes)
            {
                if (!StatusDatabase.Statuses[appliedStatusIndex].ApplyCheck.CheckStatus(this))
                {
                    if (StatusDatabase.Statuses[appliedStatusIndex].StatusAppliedVFX != null && StatusDatabase.Statuses[appliedStatusIndex].StatusAppliedVFX.Resource != null)
                    {
                        Entity.GameManager.GetManager<VFXManager>().UnbindVFX((StatusDatabase.Statuses[appliedStatusIndex].StatusAppliedVFX.Resource as GameObject).GetComponent<VFXEntity>(), transform, true);
                    }
                }
            }

            for (var i = 0; i < StatusDatabase.Statuses.Count; i++)
            {
                if (!m_AppliedStatusIndexes.Contains(i))
                {
                    if (StatusDatabase.Statuses[i].ApplyCheck.CheckStatus(this))
                    {
                        if (StatusDatabase.Statuses[i].StatusAppliedVFX != null && StatusDatabase.Statuses[i].StatusAppliedVFX.Resource != null)
                        {
                            Entity.GameManager.GetManager<VFXManager>().BindVFX((StatusDatabase.Statuses[i].StatusAppliedVFX.Resource as GameObject).GetComponent<VFXEntity>(), transform);
                        }
                    }
                }
            }

            Entity.GameManager.GetManager<EventManager>().TriggerEvent(EventId.UnitStatsUpdated, this, updatedCallback);
        }

        #endregion

        // public Color GetUnitColor()
        // {
        //     return Board.GetUnitColor(this);
        // }

        public Sprite GetUnitIcon()
        {
            return Board.GetUnitIcon(this);
        }

        private void CheckDeath(Action deathCheckedCallback)
        {
            if (ReferencedUnit.ActualLifePoints <= 0)
            {
                m_IsDead = true;
                gameObject.SetActive(false);
                Entity.GameManager.GetManager<EventManager>().TriggerEvent(EventId.UnitDeath, this, deathCheckedCallback);
            }
            else
            {
                deathCheckedCallback?.Invoke();
            }
        }
    }
}