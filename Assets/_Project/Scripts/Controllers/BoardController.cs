﻿using System;
using DG.Tweening;
using Kairos.UI.Units;
using Kairos.Units;
using TorkFramework;
using TorkFramework.Runtime.UI;
using UnityEngine;

namespace Kairos.Controllers
{
    public class BoardController : EntityBehaviour<GameEntity>
    {
        [SerializeField] private Transform m_HeroLocation;
        [SerializeField] private Transform[] m_EnemyLocations;

        //[SerializeField] private Color m_HeroColor;
        //[SerializeField] private Color[] m_EnemyColors;

        [SerializeField] private Sprite[] m_EnemyIcons;

        public UnitController HeroUnit { get; private set; }
        public UnitController[] EnemyUnits { get;private set; }

        private void Awake()
        {
            EnemyUnits = new UnitController[m_EnemyLocations.Length];
        }

        public void SpawnHero(UnitData unitToSpawn, Action<UnitController> unitSpawnedCallback)
        {
            if(HeroUnit != null)
                throw  new Exception("Hero is already spawn");

            HeroUnit = Entity.GameManager.Spawn<UnitController, GameEntity>((unitToSpawn.TargetUnit.UnitPrefab.Resource as GameObject).GetComponent<UnitController>());
            HeroUnit.transform.position = m_HeroLocation.position - Vector3.up;
            HeroUnit.transform.rotation = m_HeroLocation.rotation;
            HeroUnit.ReferencedUnit = unitToSpawn;
            HeroUnit.Type = UnitType.Hero;
            HeroUnit.Board = this;
            HeroUnit.Team = Team.Player;
            
            (Entity.GameManager.GetManager<UIManager>().QuerableEntities[typeof(UIBoardView)] as UIBoardView).AddHeroPanel(HeroUnit);
            
            HeroUnit.transform.DOMove(m_HeroLocation.position, .5f).onComplete += () => unitSpawnedCallback?.Invoke(HeroUnit);
        }
        
        public void SpawnEnemy(UnitData unitToSpawn, Action<UnitController> unitSpawnedCallback)
        {
            for (int i = 0; i < m_EnemyLocations.Length; i++)
            {
                if (EnemyUnits[i] == null)
                {
                    EnemyUnits[i] = Entity.GameManager.Spawn<UnitController, GameEntity>((unitToSpawn.TargetUnit.UnitPrefab.Resource as GameObject).GetComponent<UnitController>());
                    EnemyUnits[i].transform.position = m_EnemyLocations[i].position - Vector3.up;
                    EnemyUnits[i].transform.rotation = m_EnemyLocations[i].rotation;
                    EnemyUnits[i].ReferencedUnit = unitToSpawn;
                    EnemyUnits[i].Type = UnitType.Enemy;
                    EnemyUnits[i].Board = this;
                    EnemyUnits[i].Team = Team.Enemy;
                    
                    (Entity.GameManager.GetManager<UIManager>().QuerableEntities[typeof(UIBoardView)] as UIBoardView).AddEnemyPanel(EnemyUnits[i]);
                    
                    EnemyUnits[i].transform.DOMove(m_EnemyLocations[i].position, .5f).onComplete += () => unitSpawnedCallback?.Invoke(EnemyUnits[i]);
                    return;
                }
            }
        }

        // public Color GetUnitColor(UnitController unit)
        // {
        //     if (Array.Exists(EnemyUnits, x => x == unit))
        //         return m_EnemyColors[Array.IndexOf(EnemyUnits, unit)];
        //
        //     if (unit == HeroUnit)
        //         return m_HeroColor;
        //     
        //     return Color.white;
        // }

        public Sprite GetUnitIcon(UnitController unit)
        {
            if (Array.Exists(EnemyUnits, x => x == unit))
                return m_EnemyIcons[Array.IndexOf(EnemyUnits, unit)];

            if (unit == HeroUnit)
                return HeroUnit.ReferencedUnit.TargetUnit.HeroIcon.Resource as Sprite;
            
            return Sprite.Create(Texture2D.whiteTexture, new Rect(0, 0, 32, 32), new Vector2(.5f, .5f));
        }

        public UnitController GetUnit(GameObject obj)
        {
            if (HeroUnit.gameObject.GetInstanceID() == obj.GetInstanceID())
                return HeroUnit;
            else
            {
                for (var i = 0; i < EnemyUnits.Length; i++)
                {
                    if (EnemyUnits[i] != null && EnemyUnits[i].gameObject.GetInstanceID() == obj.GetInstanceID())
                        return EnemyUnits[i];
                }
            }
            return null;
        }
    }
}