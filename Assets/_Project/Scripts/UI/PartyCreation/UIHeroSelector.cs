﻿using System;
using Kairos.Cards;
using Kairos.Heroes;
using Kairos.Player;
using Kairos.UI.Cards;
using Kairos.Units;
using TMPro;
using TorkFramework;
using TorkFramework.Runtime.UI;
using UnityEngine;
using UnityEngine.UI;

namespace Kairos.UI.PartyCreation
{
    public class UIHeroSelector : EntityBehaviour<UIEntity>
    {
        [SerializeField] private TextMeshProUGUI m_NameText;
        [SerializeField] private TextMeshProUGUI m_TitleText;
        [SerializeField] private TextMeshProUGUI m_LoreText;
        [SerializeField] private TextMeshProUGUI m_DeckNameText;
        [SerializeField] private Image m_IllustrationImage;
        [SerializeField] private UIEntity m_DeckPreviewGroup;
        [SerializeField] private UICardView m_CardViewPrefab;
        [SerializeField] private GameObject[] m_Buttons;
        
        private int m_ActualIndex = 0;

        public string SelectedHero => Entity.GameManager.GetManager<PlayerManager>().ProgressionData.UnlockedHeroes[m_ActualIndex];
        
        public void IncreaseHero()
        {
            m_ActualIndex = m_ActualIndex + 1 >= Entity.GameManager.GetManager<PlayerManager>().ProgressionData.UnlockedHeroes.Count ? 0 : m_ActualIndex + 1;
            UpdatePreview();
        }

        public void DecreaseHero()
        {
            m_ActualIndex = m_ActualIndex - 1 < 0 ? m_ActualIndex =Entity.GameManager.GetManager<PlayerManager>().ProgressionData.UnlockedHeroes.Count - 1 : m_ActualIndex - 1;
            UpdatePreview();
        }

        private void OnEnable()
        {
            if (Entity.GameManager.HasManager<PlayerManager>())
            {
                var playerManager = Entity.GameManager.GetManager<PlayerManager>();
                m_ActualIndex = 0;
                if (playerManager.ProgressionData.UnlockedHeroes.Count <= 1)
                {
                    foreach (var button in m_Buttons)
                    {
                        button.SetActive(false);
                    }
                }
                else
                {
                    foreach (var button in m_Buttons)
                    {
                        button.SetActive(true);
                    }
                }
                UpdatePreview();
            }
        }

        private void UpdatePreview()
        {
            try
            {
                var hero = HeroDatabase.LoadedHeroes[Entity.GameManager.GetManager<PlayerManager>().ProgressionData.UnlockedHeroes[m_ActualIndex]];
                var unit = UnitDatabase.LoadedUnits[hero.HeroUnit];
                m_IllustrationImage.sprite = unit.UnitRender.Resource as Sprite;
                m_DeckNameText.text = hero.DeckName;
                m_NameText.text = unit.UnitName;
                m_TitleText.text = hero.Title;
                m_LoreText.text = unit.Lore;
                UpdateCardPreview(ref hero);
            }
            catch (Exception ex)
            {
                
            }
        }

        private void UpdateCardPreview(ref HeroInfo hero)
        {
            if (Entity != null && Entity.GameManager != null)
            {
                Entity.GameManager.KillChildren(m_DeckPreviewGroup);
                
                foreach (var card in hero.Cards)
                {
                    var newCard = Entity.GameManager.Spawn<UICardView, UIEntity>(m_CardViewPrefab);
                    newCard.transform.SetParent(m_DeckPreviewGroup.transform, false);
                    newCard.ReferencedCard = CardDatabase.LoadedCards[card];
                }
                
                LayoutRebuilder.MarkLayoutForRebuild(transform as RectTransform);
            }
        }
    }
}