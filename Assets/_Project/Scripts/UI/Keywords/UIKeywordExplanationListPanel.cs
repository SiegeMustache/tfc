﻿using System;
using DG.Tweening;
using Kairos.Keywords;
using Kairos.UI.Cards;
using TorkFramework;
using TorkFramework.Runtime.UI;
using UnityEngine;
using UnityEngine.UI;

namespace Kairos.UI.Keywords
{
    [RequireComponent(typeof(CanvasGroup))]
    public class UIKeywordExplanationListPanel : EntityBehaviour<UIEntity>
    {
        [SerializeField] private UIKeywordExplanationView m_ExplanationViewPrefab;

        private CanvasGroup m_Group;
        private UICardView m_TargetCard;
        private VerticalLayoutGroup m_LayoutGroup;
        private float m_Offset = 0f;

        public CanvasGroup Group
        {
            get
            {
                if (m_Group == null)
                    m_Group = GetComponent<CanvasGroup>();
                return m_Group;
            }
        }

        private void Start()
        {
            Entity.UIManager.AddQuerableEntity(this);
            m_LayoutGroup = GetComponent<VerticalLayoutGroup>();
            HideCardExplanation();
        }

        private void OnDestroy()
        {
            Entity.UIManager.ClearQuerableEntity(GetType());
        }

        private void Update()
        {
            if (m_TargetCard == null)
            {
                if(Group.alpha >= 1f)
                    HideCardExplanation();
                return;
            }
            
            var targetCardRect = (m_TargetCard.transform as RectTransform).rect;
            transform.position = m_TargetCard.transform.position;
            transform.localPosition += new Vector3(targetCardRect.width / 2f, m_Offset) * m_TargetCard.transform.localScale.x;
            transform.localScale = m_TargetCard.transform.localScale;
        }

        public void ShowCardExplanation(UICardView targetCard, int order)
        {
            m_LayoutGroup.childAlignment = order > 0 ? TextAnchor.UpperLeft : TextAnchor.LowerLeft;
            m_TargetCard = targetCard;
            var keywords = KeywordDatabase.GetKeywordsInText(targetCard.ReferencedCard.CardGeneratedDescription);

            var counter = 0;
            var height = 0f;
            m_Offset = 0;
            
            foreach (var keyword in keywords)
            {
                counter++;
                var newView = Entity.GameManager.Spawn<UIKeywordExplanationView, UIEntity>(m_ExplanationViewPrefab);
                newView.transform.SetParent(transform, false);
                newView.Keyword = keyword;
                height = (newView.transform as RectTransform).rect.height;
            }

            if (counter > 0)
            {
                (transform as RectTransform).SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, height * counter);
                if (order > 0)
                    m_Offset = height * (3 - counter);
            }
            
            LayoutRebuilder.MarkLayoutForRebuild(transform as RectTransform);

            DOTween.To(() => m_Group.alpha, x => m_Group.alpha = x, 1, .25f);
        }

        public void HideCardExplanation()
        {
            Entity.GameManager.KillChildren(Entity);
            DOTween.To(() => m_Group.alpha, x => m_Group.alpha = x, 0, .25f);
        }
    }
}