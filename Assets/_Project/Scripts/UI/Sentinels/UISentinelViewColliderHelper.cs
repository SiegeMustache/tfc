﻿using Kairos.Indicators;
using System.Collections;
using System.Collections.Generic;
using TorkFramework;
using TorkFramework.Runtime.UI;
using UnityEngine;

public class UISentinelViewColliderHelper : EntityBehaviour<UIEntity>
{
    public UISentinelView ParentSentinelComponent;
    [SerializeField] private CursorTrigger CursorTrigger;

    private void OnMouseEnter()
    {
        CursorTrigger.OnSelect();
        //ParentSentinelComponent.ShowDescription();
    }

    private void OnMouseExit()
    {
        CursorTrigger.OnDeselect();
        //ParentSentinelComponent.HideDescription();
    }

    private void OnMouseUpAsButton()
    {
        ParentSentinelComponent.SelectSentinel();
    }
}
