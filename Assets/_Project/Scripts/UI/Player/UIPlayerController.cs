﻿using System;
using System.Collections.Generic;
using Kairos.Cards;
using Kairos.Sentinels;
using Kairos.UI.Timeline;
using Kairos.Units;
using TorkFramework;
using TorkFramework.Runtime.UI;
using UnityEngine;
using UnityEngine.UI;

namespace Kairos.UI.Player
{
    public class UIPlayerController : EntityBehaviour<UIEntity>
    {
        [SerializeField] private UIPlayerHandView m_HandView;
        [SerializeField] private VerticalLayoutGroup m_SentinelViewGroup;
        [SerializeField] private UISentinelView m_SentinelViewPrefab;
        [SerializeField] private UITimelineHighlighter m_TimelineHighlighter;
        private List<UISentinelView> m_SentinelViews = new List<UISentinelView>();

        public UIPlayerHandView HandView => m_HandView;

        public enum UIPlayerControllerState
        {
            PlacingSentinel,
            Idle,
            WaitingTimeline
        }
        private UIPlayerControllerState m_ControllerState = UIPlayerControllerState.Idle;
        public UIPlayerControllerState ControllerState
        {
            get { return m_ControllerState; }
            set
            {
                m_ControllerState = value;
                OnStatusChange(m_ControllerState);
            }
        }
        
        private void Start()
        {
            Entity.UIManager.AddQuerableEntity(this);
        }

        private void OnDestroy()
        {
            Entity.UIManager.ClearQuerableEntity(typeof(UIPlayerController));
        }

        private void OnDisable()
        {
            ClearView();
        }

        public void DrawCard(CardInfo card, UnitController heroUnit, Action cardDrawCallback)
        {
            m_HandView.DrawCard(card, heroUnit, cardDrawCallback);
        }

        public void DiscardCard(CardInfo card, bool animate, Action cardDiscardedCallback)
        {
            m_HandView.DiscardCard(card, animate, cardDiscardedCallback);
        }

        public void ExhaustCard(CardInfo card, bool animate, Action cardExhaustedCallback)
        {
            m_HandView.ExhaustCard(card, animate,cardExhaustedCallback);
        }

        public void SpawnSentinelButtons(SentinelController sentinel)
        {
            Debug.Log("SPAWNED UI");
            var newSentinelView = Entity.GameManager.Spawn<UISentinelView, UIEntity>(m_SentinelViewPrefab);
            newSentinelView.transform.SetParent(m_SentinelViewGroup.transform, false);
            newSentinelView.GetComponent<UISentinelView>().SetupSentinelUIView(sentinel,m_TimelineHighlighter);
            m_SentinelViews.Add(newSentinelView);
        }

        public void ClearView()
        {
            List<UISentinelView> iterativeList = new List<UISentinelView>();

            foreach(UISentinelView view in m_SentinelViews)
            {
                iterativeList.Add(view);
            }

            if(iterativeList.Count > 0)
            {
                foreach(UISentinelView view in iterativeList)
                {
                    
                    m_SentinelViews.Remove(view);
                    DestroyImmediate(view.gameObject);
                }
            }
        }

        private void OnStatusChange(UIPlayerControllerState state)
        {
            
        }
    }
}