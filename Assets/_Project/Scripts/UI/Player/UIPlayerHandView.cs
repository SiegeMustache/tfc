﻿using System;
using System.Collections.Generic;
using DG.Tweening;
using Kairos.Cards;
using Kairos.Player;
using Kairos.Timeline;
using Kairos.UI.Cards;
using Kairos.Units;
using TorkFramework;
using TorkFramework.FMODSound;
using TorkFramework.Runtime.UI;
using UnityEngine;
using UnityEngine.UI;

namespace Kairos.UI.Player
{
    public class UIPlayerHandView : EntityBehaviour<UIEntity>
    {
        [Header("General")] [SerializeField] private UIHandCardView m_CardPrefab;

        [Header("Card Positioning")] [SerializeField]
        private Vector2 m_CardSize;

        [Header("Draw Animation")] [SerializeField]
        private Vector3 m_DrawStartPosition;

        [SerializeField] private float m_DrawStartScale;
        [SerializeField] private AnimationCurve m_DrawXMovement;
        [SerializeField] private AnimationCurve m_DrawYMovement;
        [SerializeField] private AnimationCurve m_DrawScaleMovement;

        [Header("Discard Animation")] [SerializeField]
        private Vector3 m_DiscardStartPosition;

        [SerializeField] private float m_DiscardStartScale;
        [SerializeField] private AnimationCurve m_DiscardXMovement;
        [SerializeField] private AnimationCurve m_DiscardYMovement;
        [SerializeField] private AnimationCurve m_DiscardScaleMovement;
        [SerializeField] private bool m_UseAnimation = false;

        [Header("Sounds")] 
        [SerializeField] private FMODEventEmitter m_DrawSound;

        [Header("Discard Pile")] 
        [SerializeField] private UICardView m_DiscardCardPrefab;
        [SerializeField] private Transform m_DiscardPanel;
        [SerializeField] private GameEntity m_DiscardGroup;
        [SerializeField] private Vector2 m_DiscardPanelShowPosition;
        [SerializeField] private float m_DiscardPanelShowTime;
        [SerializeField] private Ease m_DiscardPanelShowEase;
        [SerializeField] private Vector2 m_DiscardPanelHidePosition;
        [SerializeField] private float m_DiscardPanelHideTime;
        [SerializeField] private Ease m_DiscardPanelHideEase;
        
        
        private Dictionary<CardInfo, UIHandCardView> m_HandCards;
        private List<UIHandCardView> m_CardsToNotMove;

        private UnitController m_HeroUnit;

        public TimelineController Timeline;
        public PlayerController Player;

        private bool m_DiscardPanelToggled;
        
        private void Awake()
        {
            m_HandCards = new Dictionary<CardInfo, UIHandCardView>();
            m_CardsToNotMove = new List<UIHandCardView>();
        }

        private void OnEnable()
        {
            Entity.GameManager.KillChildren(m_DiscardGroup);
            (m_DiscardPanel.transform as RectTransform).anchoredPosition = m_DiscardPanelHidePosition;
            m_DiscardPanelToggled = false;
        }

        public void DrawCard(CardInfo card, UnitController heroUnit, Action cardDrawCallback)
        {
            m_HeroUnit = heroUnit;
            if (!m_HandCards.ContainsKey(card))
            {
                var newCard = Entity.GameManager.Spawn<UIHandCardView, UIEntity>(m_CardPrefab);
                newCard.transform.SetParent(transform, false);
                newCard.transform.localPosition = m_DrawStartPosition;
                newCard.transform.localScale = Vector3.one * m_DrawStartScale;
                newCard.ReferencedCard = card;
                UpdateCardUnitSnapshots();
                //newCard.TargetUnit = heroUnit;

                m_HandCards.Add(card, newCard);

                if (m_UseAnimation)
                {
                    m_CardsToNotMove.Add(newCard);
                    var drawSequence = DOTween.Sequence();
                    var timeSteps = new List<float>();
                    foreach (var keyframe in m_DrawXMovement.keys)
                    {
                        if (!timeSteps.Contains(keyframe.time))
                            timeSteps.Add(keyframe.time);
                    }

                    foreach (var keyframe in m_DrawYMovement.keys)
                    {
                        if (!timeSteps.Contains(keyframe.time))
                            timeSteps.Add(keyframe.time);
                    }

                    timeSteps.Sort();

                    timeSteps.Remove(0);
                    foreach (var timeStep in timeSteps)
                    {
                        drawSequence.Append(newCard.transform.DOLocalMove(new Vector3(m_DrawXMovement.Evaluate(timeStep), m_DrawYMovement.Evaluate(timeStep), 0), timeStep - drawSequence.Duration()));
                    }

                    drawSequence.Append(newCard.transform.DOLocalMove(CalculateChildPosition(m_HandCards.Count - 1), .25f));

                    float totalScaleTime = 0;
                    foreach (var timeStep in m_DrawScaleMovement.keys)
                    {
                        drawSequence.Insert(totalScaleTime, newCard.transform.DOScale(Vector3.one * timeStep.value, timeStep.time - totalScaleTime));
                        totalScaleTime = timeStep.time;
                    }

                    m_DrawSound.Play();
                    drawSequence.Play().onComplete += () =>
                    {
                        m_CardsToNotMove.Remove(newCard);
                        UpdateCardUnitSnapshots();
                        cardDrawCallback?.Invoke();
                    };
                }
                else
                {
                    m_DrawSound.Play();
                    newCard.transform.DOScale(1f, .25f).onComplete += () =>
                    {
                        UpdateCardUnitSnapshots();
                        cardDrawCallback?.Invoke();
                    };
                }
            }
        }

        public void UpdateCardUnitSnapshots()
        {
            var firstPlayableTimeframe = Timeline.GetFirstPlayableTimeframe(m_HeroUnit);
            foreach (var handCard in m_HandCards)
            {
                var effectiveSpeed = CardHelper.GetEffectiveCardSpeed(handCard.Key, Timeline.Snapshots[firstPlayableTimeframe].HeroSnapshot, out int changed);
                if (firstPlayableTimeframe + effectiveSpeed <= Globals.TIMELINE_SIZE)
                {
                    if(firstPlayableTimeframe + effectiveSpeed > 1 )
                        handCard.Value.TargetUnitSnapshot = Timeline.Snapshots[firstPlayableTimeframe + effectiveSpeed - 2].HeroSnapshot;
                    else
                        handCard.Value.TargetUnitSnapshot = new TimelineController.TimelineUnitSnapshot(m_HeroUnit);
                }
            }
        }

        public void DiscardCard(CardInfo card, bool animate, Action cardDiscardedCallback)
        {
            if (m_HandCards.ContainsKey(card))
            {
                Entity.GameManager.Kill(m_HandCards[card].Entity);
                m_HandCards.Remove(card);
            }

            cardDiscardedCallback?.Invoke();
        }

        public void ExhaustCard(CardInfo card, bool animate, Action cardExhaustedCallback)
        {
            if (m_HandCards.ContainsKey(card))
            {
                Entity.GameManager.Kill(m_HandCards[card].Entity);
                m_HandCards.Remove(card);
            }

            cardExhaustedCallback?.Invoke();
        }

        private Vector3 CalculateChildPosition(int index)
        {
            var startPosition = Vector3.right * ((m_HandCards.Count - m_CardsToNotMove.Count) / 2f) * m_CardSize.x;
            return startPosition - Vector3.right * m_CardSize.x * (index + 0.5f);
        }

        private void Update()
        {
            int counter = 0;
            foreach (var card in m_HandCards)
            {
                if (!m_CardsToNotMove.Contains(card.Value) && !card.Value.Selected)
                {
                    if (card.Value.HasOffset)
                        card.Value.transform.localPosition = Vector3.MoveTowards(card.Value.transform.localPosition, CalculateChildPosition(counter) + (Vector3) (Vector3.up * m_CardSize / 2f), 512f * Time.deltaTime);
                    else
                        card.Value.transform.localPosition = Vector3.MoveTowards(card.Value.transform.localPosition, CalculateChildPosition(counter), 512f* Time.deltaTime);
                    counter++;
                }
            }
        }

        private void OnDisable()
        {
            Clear();
        }

        private void Clear()
        {
            Entity.GameManager.KillChildren(Entity);
            m_HandCards.Clear();
            m_CardsToNotMove.Clear();
        }

        public void ToggleDiscardPanel()
        {
            var rectPanel = m_DiscardPanel as RectTransform;
            if (m_DiscardPanelToggled)
            {
                DOTween.To(() => rectPanel.anchoredPosition, x => rectPanel.anchoredPosition = x, m_DiscardPanelHidePosition, m_DiscardPanelHideTime).SetEase(m_DiscardPanelHideEase);
            }
            else
            {
                
                Entity.GameManager.KillChildren(m_DiscardGroup);
                
                var pile = new List<CardInfo>();
                
                foreach (var deckCard in Player.DeckPile)
                {
                    pile.Add(deckCard);
                }
                
                pile.Sort((x, y) =>
                {
                    if (x.Rarity == y.Rarity)
                        return x.CardName.CompareTo(y.CardName);
                    return x.Rarity.CompareTo(y.Rarity);
                });

                foreach (var deckCard in pile)
                {
                    var newCard = Entity.GameManager.Spawn<UICardView, UIEntity>(m_DiscardCardPrefab);
                    newCard.transform.SetParent(m_DiscardGroup.transform, false);
                    newCard.ReferencedCard = deckCard;
                }
                
                LayoutRebuilder.MarkLayoutForRebuild(m_DiscardGroup.transform as RectTransform);
                
                
                DOTween.To(() => rectPanel.anchoredPosition, x => rectPanel.anchoredPosition = x, m_DiscardPanelShowPosition, m_DiscardPanelShowTime).SetEase(m_DiscardPanelShowEase);
            }

            m_DiscardPanelToggled = !m_DiscardPanelToggled;
        }
    }
}