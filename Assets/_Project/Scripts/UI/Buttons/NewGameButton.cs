﻿using Kairos.UI.Popups;
using TorkFramework;
using TorkFramework.Runtime.UI;

namespace Kairos.UI
{
    public class NewGameButton : EntityBehaviour<UIEntity>
    {
        public void NewGame()
        {
            (Entity.UIManager.QuerableEntities[typeof(SingleQuestionPopupController)] as SingleQuestionPopupController).ShowConfirmBox("Start a new game?", "Yes", "No", (result) =>
                {
                    if (result)
                    {
                        Entity.GameManager.ChangeState(GameStates.PartyCreation);
                    }
                }
            );
        }
    }
}