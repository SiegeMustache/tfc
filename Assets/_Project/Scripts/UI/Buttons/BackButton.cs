﻿using TorkFramework;
using TorkFramework.Runtime.UI;

namespace Kairos.UI
{
    public class BackButton : EntityBehaviour<UIEntity>
    {
        public void GoBack()
        {
            Entity.GameManager.PopState();
        }
    }
}