﻿using Kairos.Player;
using TorkFramework;
using TorkFramework.Runtime.UI;

namespace Kairos.UI
{
    public class UIUsePotionButton : EntityBehaviour<UIEntity>
    {
        public void UsePotion()
        {
            Entity.GameManager.GetManager<PlayerManager>().UsePotion();
        }
    }
}