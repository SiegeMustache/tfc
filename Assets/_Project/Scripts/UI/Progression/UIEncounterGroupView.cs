﻿using System;
using Kairos.Progression;
using Kairos.Progression.Encounters;
using TorkFramework;
using TorkFramework.Runtime.UI;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Kairos.UI.Progression
{
    public class UIEncounterGroupView : EntityBehaviour<UIEntity>
    {
        [SerializeField] private UIEncounterView m_EncounterPrefab;
        [SerializeField] private RectTransform m_EncounterParent;

        private EncounterGroup m_ReferencedGroup;
        public int EncounterGroupIndex;
        
        public Action<int, int> EncounterClick;

        public UnityEvent OnSelect;
        public UnityEvent OnDeselect;

        private bool m_Selected = false;

        public bool Selected => m_Selected;

        public EncounterGroup ReferencedGroup
        {
            get => m_ReferencedGroup;
            set
            {
                m_ReferencedGroup = value;
                if (m_ReferencedGroup != null)
                {
                    for (int i = 0; i < m_ReferencedGroup.Encounters.Length; i++)
                    {
                        var index = i;
                        var newEncounter = Entity.GameManager.Spawn<UIEncounterView, UIEntity>(m_EncounterPrefab);
                        newEncounter.Group = this;
                        newEncounter.ReferencedEncounter = m_ReferencedGroup.Encounters[index];
                        newEncounter.transform.SetParent(m_EncounterParent, false);
                        newEncounter.EncounterClicked += () => { OnEncounterClick(index); };
                    }
                    
                    LayoutRebuilder.MarkLayoutForRebuild(m_EncounterParent);
                }
            }
        }

        private void OnEncounterClick(int index)
        {
            EncounterClick?.Invoke(EncounterGroupIndex, index);
        }

        private void Update()
        {
            if (!m_Selected && Entity.GameManager.GetManager<ProgressionManager>().ActualGroup == EncounterGroupIndex)
            {
                m_Selected = true;
                OnSelect?.Invoke();
                return;
            }

            if (m_Selected && Entity.GameManager.GetManager<ProgressionManager>().ActualGroup != EncounterGroupIndex)
            {
                m_Selected = false;
                OnDeselect?.Invoke();
                return;
            }
        }
    }
}