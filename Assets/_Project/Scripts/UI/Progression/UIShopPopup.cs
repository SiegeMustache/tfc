﻿using System;
using Kairos.Progression.Rewards;
using Kairos.UI.Cards;
using TMPro;
using TorkFramework;
using TorkFramework.Runtime.UI;
using UnityEngine;
using UnityEngine.UI;

namespace Kairos.UI.Progression{
public class UIShopPopup : EntityBehaviour<UIEntity>
{
    [SerializeField] private UIShopEntryView m_OptionPrefab;
    [SerializeField] private UIEntity m_OptionsTransform;
    
    private Action<Reward> m_ResultCallback;
    private bool m_Clicked = false;
    
    private void Start()
    {
        Entity.UIManager.AddQuerableEntity(this);
    }

    private void OnDestroy()
    {
        Entity.UIManager.ClearQuerableEntity(GetType());
    }

    public void ShowShopPopup(Action<Reward> answerSelectedCallback, params Reward[] options)
    {
        m_ResultCallback = answerSelectedCallback;
        for (int i = 0; i < options.Length; i++)
        {
            var newEntry = Entity.GameManager.Spawn<UIShopEntryView, UIEntity>(m_OptionPrefab);
            newEntry.transform.SetParent(m_OptionsTransform.transform, false);
            newEntry.TargetReward = options[i];
            newEntry.Selected += OnButtonClicked;
        }

        LayoutRebuilder.MarkLayoutForRebuild(m_OptionsTransform.transform as RectTransform);

        m_Clicked = false;
        Entity.GameManager.ChangeState(GameStates.Shop);
    }

    private void OnDisable()
    {
        Entity.GameManager.KillChildren(m_OptionsTransform);
    }

    private void OnButtonClicked(Reward reward)
    {
        if(m_Clicked)
            return;

        m_Clicked = true;
        Entity.GameManager.PopState(() =>
        {
            m_ResultCallback?.Invoke(reward);
            m_ResultCallback = null;
        });
    }

    public void OnSkipClick()
    {
        
        if(m_Clicked)
            return;

        m_Clicked = true;
        Entity.GameManager.PopState(() =>
        {
            m_ResultCallback?.Invoke(null);
            m_ResultCallback = null;
        });
    }
}
}