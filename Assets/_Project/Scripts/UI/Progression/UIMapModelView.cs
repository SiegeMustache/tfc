﻿using System;
using System.Collections.Generic;
using DG.Tweening;
using Kairos.Progression;
using Kairos.Progression.Encounters;
using TMPro;
using TorkFramework;
using TorkFramework.Events;
using TorkFramework.Runtime.UI;
using UnityEngine;
using UnityEngine.UI;

namespace Kairos.UI.Progression
{
    public class UIMapModelView : EntityBehaviour<UIEntity>
    {
        [SerializeField] private UIEncounterGroupView m_UIEncounterGroupPrefab;
        [SerializeField] private float m_ScrollAmount;
        [SerializeField] private float m_ScrollDuration = .5f;
        [SerializeField] private TextMeshProUGUI m_TotalTimeText;
        [SerializeField] private TextMeshProUGUI m_ActualTimeText;
        [SerializeField] private float[] m_TierBackgroundLevel;
        [SerializeField] private Image m_Background;

        private List<UIEncounterGroupView> m_SpawnedViews;

        private Vector3 m_StartPosition;

        private void Start()
        {
            Entity.GameManager.GetManager<EventManager>().AddListener(EventId.MapGenerated, OnMapGenerated);
            Entity.GameManager.GetManager<EventManager>().AddListener(EventId.ProgressionPositionChanged, OnProgressionPositionChanged);
            Entity.GameManager.GetManager<EventManager>().AddListener(EventId.EncounterGroupAdded, OnEncounterGroupAdded);
            Entity.GameManager.GetManager<EventManager>().AddListener(EventId.EncounterGroupRemove, OnEncounterGroupRemove);
            Entity.GameManager.GetManager<EventManager>().AddListener(EventId.ProgressionCostChanged, OnCostChanged);
            Entity.GameManager.GetManager<EventManager>().AddListener(EventId.TierChanged, OnTierChanged);
            m_StartPosition = transform.localPosition;
        }

        private void OnDestroy()
        {
            Entity.GameManager.GetManager<EventManager>().RemoveListener(EventId.MapGenerated, OnMapGenerated);
            Entity.GameManager.GetManager<EventManager>().RemoveListener(EventId.ProgressionPositionChanged, OnProgressionPositionChanged);
            Entity.GameManager.GetManager<EventManager>().RemoveListener(EventId.EncounterGroupAdded, OnEncounterGroupAdded);
            Entity.GameManager.GetManager<EventManager>().RemoveListener(EventId.EncounterGroupRemove, OnEncounterGroupRemove);
            Entity.GameManager.GetManager<EventManager>().RemoveListener(EventId.ProgressionCostChanged, OnCostChanged);
            Entity.GameManager.GetManager<EventManager>().RemoveListener(EventId.TierChanged, OnTierChanged);
        }

        private void OnEnable()
        {
            if (Entity != null && Entity.GameManager.HasManager<ProgressionManager>())
            {
                var progressionManager = Entity.GameManager.GetManager<ProgressionManager>();
                var tier = progressionManager.ActualTier;

                if (tier >= 0 && tier < m_TierBackgroundLevel.Length)
                {
                    var material = m_Background.material;
                    material.SetFloat("_TransitionThreshold", m_TierBackgroundLevel[tier]);
                }
            }
        }

        private void OnMapGenerated(GenericEventArgs args, Action listenerExecutedCallback)
        {
            try
            {
                m_SpawnedViews = new List<UIEncounterGroupView>();
                Entity.GameManager.KillChildren(Entity);
                transform.localPosition = m_StartPosition;

                var progressionData = args.Args[0] as ProgressionData;

                for (int i = 0; i < progressionData.GeneratedEncounters.Count; i++)
                {
                    var newGroup = Entity.GameManager.Spawn<UIEncounterGroupView, UIEntity>(m_UIEncounterGroupPrefab);
                    newGroup.ReferencedGroup = progressionData.GeneratedEncounters[i];
                    newGroup.transform.SetParent(transform, false);
                    newGroup.EncounterClick += OnGroupClicked;
                    newGroup.EncounterGroupIndex = i;
                    m_SpawnedViews.Add(newGroup);
                }

                LayoutRebuilder.MarkLayoutForRebuild(transform as RectTransform);
                
                m_TotalTimeText.text = Entity.GameManager.GetManager<ProgressionManager>().MaxTime.ToString();

                m_ActualTimeText.text = "0";
                
                var progressionManager = Entity.GameManager.GetManager<ProgressionManager>();
                var tier = progressionManager.ActualTier;

                if (tier >= 0 && tier < m_TierBackgroundLevel.Length)
                {
                    var material = m_Background.material;
                    DOTween.To(() => material.GetFloat("_TransitionThreshold"), x => material.SetFloat("_TransitionThreshold", x), m_TierBackgroundLevel[tier], .25f);
                }
            }
            catch (Exception ex)
            {
                Debug.LogError($"Error while building the map UI!\n{ex.Message}");
            }

            listenerExecutedCallback?.Invoke();
        }

        private void OnGroupClicked(int groupIndex, int encounterIndex)
        {
            Entity.GameManager.GetManager<ProgressionManager>().PlayGroup(groupIndex, encounterIndex);
        }

        private void OnProgressionPositionChanged(GenericEventArgs args, Action listenerExecutedCallback)
        {
            
            transform.DOLocalMove(new Vector3(transform.localPosition.x,m_ScrollAmount * (int)args.Args[0]), m_ScrollDuration).onComplete += () => listenerExecutedCallback?.Invoke();
        }

        private void OnEncounterGroupAdded(GenericEventArgs args, Action listenerExecutedCallback)
        {
            var position = (int) args.Args[0];
            var group = (EncounterGroup) args.Args[1];
            var newGroup = Entity.GameManager.Spawn<UIEncounterGroupView, UIEntity>(m_UIEncounterGroupPrefab);
            newGroup.ReferencedGroup = group;
            newGroup.transform.SetParent(transform, false);
            newGroup.EncounterClick += OnGroupClicked;
            newGroup.transform.SetSiblingIndex(position);
            newGroup.EncounterGroupIndex = position;
            m_SpawnedViews.Insert(position, newGroup);

            for (int i = 0; i < m_SpawnedViews.Count; i++)
            {
                m_SpawnedViews[i].EncounterGroupIndex = i;
            }
            
            listenerExecutedCallback?.Invoke();
        }

        private void OnEncounterGroupRemove(GenericEventArgs args, Action listenerExecutedCallback)
        {
            var position = (int) args.Args[0];
            
            Entity.GameManager.Kill(m_SpawnedViews[position].Entity);
            m_SpawnedViews.RemoveAt(position);
            
            for (int i = 0; i < m_SpawnedViews.Count; i++)
            {
                m_SpawnedViews[i].EncounterGroupIndex = i;
            }
            
            listenerExecutedCallback?.Invoke();
        }

        private void OnCostChanged(GenericEventArgs args, Action listenerExecutedCallback)
        {
            var newCost = (int) args.Args[0];

            var value = float.Parse(m_ActualTimeText.text);
            
            m_TotalTimeText.text = Entity.GameManager.GetManager<ProgressionManager>().MaxTime.ToString();

            DOTween.To(() => value, x =>
            {
                value = x;
                m_ActualTimeText.text = Mathf.FloorToInt(value).ToString();
            }, newCost, .5f).onComplete += () =>  listenerExecutedCallback?.Invoke();
        }

        private void OnTierChanged(GenericEventArgs args, Action listenerExecutedCallback)
        {
            var newTier = (int) args.Args[0];

            if (newTier >= 0 && newTier < m_TierBackgroundLevel.Length)
            {
                var material = m_Background.material;
                DOTween.To(() => material.GetFloat("_TransitionThreshold"), x => material.SetFloat("_TransitionThreshold", x), m_TierBackgroundLevel[newTier], .5f).onComplete += () =>
                {
                    listenerExecutedCallback?.Invoke();
                };
            }
            else
            {
                listenerExecutedCallback?.Invoke();
            }
        }
    }
}