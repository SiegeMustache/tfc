﻿using System;
using DG.Tweening;
using Kairos.Progression;
using TMPro;
using TorkFramework;
using TorkFramework.Events;
using TorkFramework.Runtime.UI;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Progressions
{
    public class UIProgressionSlider : EntityBehaviour<UIEntity>
    {
        [SerializeField] private Slider m_ProgressionSlider;
        [SerializeField] private TextMeshProUGUI m_ProgressionText;
        [SerializeField] private float m_ScrollTime;

        private void OnEnable()
        {
            var progressionManager = Entity.GameManager.GetManager<ProgressionManager>();
            if (progressionManager != null)
            {
                m_ProgressionSlider.value = progressionManager.ActualTime;
                m_ProgressionText.text = progressionManager.ActualTime.ToString();
            }

            var eventManager = Entity.GameManager.GetManager<EventManager>();
            if (eventManager != null)
                eventManager.AddListener(EventId.ProgressionCostChanged, OnProgressionCostChanged);
        }

        private void OnDisable()
        {
            Entity.GameManager.GetManager<EventManager>().RemoveListener(EventId.ProgressionCostChanged, OnProgressionCostChanged);
        }

        private void OnProgressionCostChanged(GenericEventArgs args, Action listenerExecutedCallback)
        {
            DOTween.To(() => m_ProgressionSlider.value, x =>
            {
                m_ProgressionSlider.value = x;
                m_ProgressionText.text = Mathf.RoundToInt(x).ToString();
            }, (int) args.Args[0], m_ScrollTime).onComplete += () => listenerExecutedCallback?.Invoke();
        }
    }
}