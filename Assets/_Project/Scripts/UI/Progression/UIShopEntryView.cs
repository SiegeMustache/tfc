﻿using System;
using Kairos.Progression.Rewards;
using Kairos.Sentinels;
using Kairos.UI.Cards;
using TMPro;
using TorkFramework;
using TorkFramework.Runtime.UI;
using UnityEngine;

public class UIShopEntryView : EntityBehaviour<UIEntity>
{
    public event Action<Reward> Selected;

    [SerializeField] private TextMeshProUGUI m_ShopCostText;
    [SerializeField] private TextMeshProUGUI m_PopupText;
    [SerializeField] private UICardView m_PreviewCard;
    [SerializeField] private UISentinelRewardView m_PreviewSentinel;
    [SerializeField] private GameObject m_PotionPreview;

    private Reward m_TargetReward;

    public Reward TargetReward
    {
        get => m_TargetReward;
        set
        {
            m_TargetReward = value;
            SetupView((dynamic)m_TargetReward);
        }
    }


    public void OnClick()
    {
        Selected?.Invoke(TargetReward);
    }

    private void SetupView(Reward reward)
    {
        Debug.LogError("Reward View Not Managed");
    }
    
    private void SetupView(CardReward reward)
    {
        m_PopupText.text = "Take Card";
        m_PreviewCard.gameObject.SetActive(true);
        m_PreviewSentinel.gameObject.SetActive(false);
        m_PotionPreview.gameObject.SetActive(false);
        m_PreviewCard.ReferencedCard = reward.TargetCard.Card;
        m_ShopCostText.text = reward.ShopCost.ToString();
    }

    private void SetupView(SentinelReward reward)
    {
        m_PopupText.text = "Take Blessing";
        m_PreviewCard.gameObject.SetActive(false);
        m_PreviewSentinel.gameObject.SetActive(true);
        m_PotionPreview.gameObject.SetActive(false);
        m_ShopCostText.text = reward.ShopCost.ToString();
        SentinelObject tempObject = reward.TargetSentinel.Resource as SentinelObject;
        SentinelInfo tempData = tempObject.Sentinel;
        m_PreviewSentinel.SetupSentinelPreview(tempData);

        if (m_PreviewSentinel.IsSentinelAlreadyOwned(tempData))
		{
            m_PopupText.transform.parent.gameObject.SetActive(false);
		}
        else
		{
            m_PopupText.transform.parent.gameObject.SetActive(true);
		}
    }
    
    private void SetupView(PotionReward reward)
    {
        m_PopupText.text = "Restore";
        m_PreviewCard.gameObject.SetActive(false);
        m_PreviewSentinel.gameObject.SetActive(false);
        m_PotionPreview.gameObject.SetActive(true);
        m_ShopCostText.text = reward.ShopCost.ToString();
    }
}
