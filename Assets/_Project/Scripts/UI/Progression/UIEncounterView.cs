﻿using System;
using Kairos.Indicators;
using Kairos.Progression.Encounters;
using TMPro;
using TorkFramework;
using TorkFramework.Runtime.UI;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Kairos.UI.Progression
{
    public class UIEncounterView : EntityBehaviour<UIEntity>, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField] private Image m_EncounterImage;
        [SerializeField] private TextMeshProUGUI m_EncounterCost;
        [SerializeField] private CursorTrigger m_CursorTrigger;

        public UnityEvent OnSelect;
        public UnityEvent OnDeselect;
        
        private Encounter m_ReferencedEncounter;

        public Action EncounterClicked;

        private bool m_Selected = false;

        public Encounter ReferencedEncounter
        {
            get => m_ReferencedEncounter;
            set
            {
                m_ReferencedEncounter = value;
                if (m_ReferencedEncounter != null)
                {
                    if (m_ReferencedEncounter.PreviewSprite != null && m_ReferencedEncounter.PreviewSprite.Resource != null)
                    {
                        m_EncounterImage.sprite = m_ReferencedEncounter.PreviewSprite.Resource as Sprite;
                    }

                    m_EncounterCost.text = m_ReferencedEncounter.Cost.ToString();
                }
            }
        }

        public UIEncounterGroupView Group;

        private void Awake()
        {
            m_CursorTrigger = GetComponent<CursorTrigger>();
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if(Group.Selected)
                EncounterClicked?.Invoke();
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (Group.Selected)
            {
                m_CursorTrigger.OnSelect();
                m_Selected = true;
                OnSelect?.Invoke();
            }
        }

        private void Update()
        {
            if (m_Selected)
            {
                if (!Group.Selected)
                {
                    m_CursorTrigger.OnDeselect();
                    m_Selected = false;
                    OnDeselect?.Invoke();
                }
            }
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            m_CursorTrigger.OnDeselect();
            m_Selected = false;
            OnDeselect?.Invoke();
        }
    }
}