﻿using Kairos.Cards;
using Kairos.Player;
using Kairos.UI.Cards;
using TorkFramework;
using TorkFramework.Runtime.UI;
using UnityEngine;
using UnityEngine.UI;

namespace Kairos.UI.Status
{
    public class UIHeroCardList : EntityBehaviour<UIEntity>
    {
        [SerializeField] private UICardView m_CardViewPrefab;
        
        private void OnEnable()
        {
            UpdateCardList();
        }

        private void Start()
        {
            UpdateCardList();
        }

        private void UpdateCardList()
        {
            if (Entity != null && Entity.GameManager != null)
            {
                Entity.GameManager.KillChildren(Entity);
                
                if (Entity.GameManager.HasManager<PlayerManager>() && Entity.GameManager.GetManager<PlayerManager>().Data != null)
                {
                    foreach (var card in Entity.GameManager.GetManager<PlayerManager>().Data.Cards)
                    {
                        var newCard = Entity.GameManager.Spawn<UICardView, UIEntity>(m_CardViewPrefab);
                        newCard.transform.SetParent(transform, false);
                        newCard.ReferencedCard = CardDatabase.LoadedCards[card];
                    }
                    
                    LayoutRebuilder.MarkLayoutForRebuild(transform as RectTransform);
                }
            }
        }
    }
}