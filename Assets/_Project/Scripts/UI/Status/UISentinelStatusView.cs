﻿using System;
using Kairos.Sentinels;
using Kairos.UI.Feedbacks;
using TorkFramework;
using TorkFramework.Runtime.UI;
using UnityEngine;
using UnityEngine.UI;

namespace Kairos.UI.Status
{
    [RequireComponent(typeof(UITooltipComponent))]
    public class UISentinelStatusView : EntityBehaviour<UIEntity>
    {
        [SerializeField] private Image m_PreviewImage;
        
        private SentinelInfo m_TargetSentinel;
        private UITooltipComponent m_Tooltip;
        
        public SentinelInfo TargetSentinel
        {
            get
            {
                return m_TargetSentinel;
            }

            set
            {
                m_TargetSentinel = value;
                if (m_TargetSentinel != null)
                {
                    m_PreviewImage.sprite = m_TargetSentinel.SentinelIcon.Resource as Sprite;
                    
                    string title = "<b>" + m_TargetSentinel.SentinelName + "</b>\n";
                    string description = m_TargetSentinel.SentinelDescription;
                    string total = title + description;

                    m_Tooltip.ShowText = total;
                }
            }
        }

        private void Awake()
        {
            m_Tooltip = GetComponent<UITooltipComponent>();
        }
    }
}