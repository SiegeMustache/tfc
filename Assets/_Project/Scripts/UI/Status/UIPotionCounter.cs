﻿using Kairos.Heroes;
using Kairos.Player;
using TorkFramework;
using TorkFramework.Runtime.UI;
using UnityEngine;
using UnityEngine.UI;

namespace Kairos.UI.Status
{
    public class UIPotionCounter : EntityBehaviour<UIEntity>
    {
        [SerializeField] private UIEntity m_PotionSlotEmptyPrefab;
        [SerializeField] private UIEntity m_PotionSlotFullPrefab;

        private int m_ActualTotal = 0;
        private int m_ActualCount = 0;

        private void OnEnable()
        {
            UpdatePotionCounter();

        }

        private void Start()
        {
            UpdatePotionCounter();
        }

        private void Update()
        {
            if (Entity.GameManager.HasManager<PlayerManager>() && Entity.GameManager.GetManager<PlayerManager>().Data != null)
            {
                if (Entity.GameManager.GetManager<PlayerManager>().Data.PotionCount != m_ActualCount || HeroDatabase.LoadedHeroes[Entity.GameManager.GetManager<PlayerManager>().Data.HeroId].PotionCount != m_ActualTotal)
                {
                    UpdatePotionCounter();
                }
            }
        }

        private void UpdatePotionCounter()
        {
            
            Entity.GameManager.KillChildren(Entity);
            if (Entity != null && Entity.GameManager != null)
            {
                if (Entity.GameManager.HasManager<PlayerManager>() && Entity.GameManager.GetManager<PlayerManager>().Data != null)
                {
                    m_ActualCount = Entity.GameManager.GetManager<PlayerManager>().Data.PotionCount;
                    m_ActualTotal = HeroDatabase.LoadedHeroes[Entity.GameManager.GetManager<PlayerManager>().Data.HeroId].PotionCount;
                    for (int i = 0; i < m_ActualTotal; i++)
                    {
                        if (i < m_ActualCount)
                        {
                            var newToken = Entity.GameManager.Spawn(m_PotionSlotFullPrefab);
                            newToken.transform.SetParent(transform, false);
                        }
                        else
                        {
                            var newToken = Entity.GameManager.Spawn(m_PotionSlotEmptyPrefab);
                            newToken.transform.SetParent(transform, false);
                        }
                    }
                    
                    LayoutRebuilder.MarkLayoutForRebuild(transform as RectTransform);
                }
            }
        }
    }
}