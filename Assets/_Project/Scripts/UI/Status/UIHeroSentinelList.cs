﻿using DG.Tweening;
using Kairos.Player;
using TorkFramework;
using TorkFramework.Runtime.UI;
using UnityEngine;
using UnityEngine.UI;

namespace Kairos.UI.Status
{
    public class UIHeroSentinelList : EntityBehaviour<UIEntity>
    {
        [SerializeField] private UISentinelStatusView m_SentinelPrefab;
        [SerializeField] private int m_MaxVisualization = 3;
        [SerializeField] private float m_Offset = 132;
        [SerializeField] private GameObject m_IncreaseButton;
        [SerializeField] private GameObject m_DecreaseButton;
        
        private int m_Index = 0;
        
        private void OnEnable()
        {
            UpdateSentinelList();
        }

        private void Start()
        {
            UpdateSentinelList();
        }

        private void UpdateSentinelList()
        {
            Entity.GameManager.KillChildren(Entity);
            (transform as RectTransform).anchoredPosition = Vector2.zero;
            m_Index = 0;
            m_DecreaseButton.SetActive(false);
            m_IncreaseButton.SetActive(false);
            
            if (Entity != null && Entity.GameManager != null)
            {
                if (Entity.GameManager.HasManager<PlayerManager>() && Entity.GameManager.GetManager<PlayerManager>().Data != null)
                {
                    if (m_Index + m_MaxVisualization < Entity.GameManager.GetManager<PlayerManager>().Data.Sentinels.Count)
                    {
                        m_IncreaseButton.SetActive(true);
                    }
                    
                    foreach (var sentinel in Entity.GameManager.GetManager<PlayerManager>().Data.Sentinels)
                    {
                        var newSentinel = Entity.GameManager.Spawn<UISentinelStatusView, UIEntity>(m_SentinelPrefab);
                        newSentinel.transform.SetParent(transform, false);
                        newSentinel.TargetSentinel = sentinel;
                    }
                    
                    LayoutRebuilder.MarkLayoutForRebuild(transform as RectTransform);
                }
            }
        }

        public void Increase()
        {
            if (Entity.GameManager.HasManager<PlayerManager>() && Entity.GameManager.GetManager<PlayerManager>().Data != null)
            {
                if (m_Index + m_MaxVisualization < Entity.GameManager.GetManager<PlayerManager>().Data.Sentinels.Count)
                {
                    m_Index++;
                    if (!(m_Index + m_MaxVisualization < Entity.GameManager.GetManager<PlayerManager>().Data.Sentinels.Count))
                    {
                        m_IncreaseButton.SetActive(false);
                    }
                    m_DecreaseButton.SetActive(true);

                    var rectTransform = transform as RectTransform;
                    DOTween.To(() => rectTransform.anchoredPosition, x => rectTransform.anchoredPosition = x, new Vector2(-m_Offset * m_Index, 0f), .25f);

                }
            }
        }

        public void Decrease()
        {
            if (Entity.GameManager.HasManager<PlayerManager>() && Entity.GameManager.GetManager<PlayerManager>().Data != null)
            {
                if (m_Index > 0)
                {
                    m_Index--;
                    if (m_Index == 0)
                    {
                        m_DecreaseButton.SetActive(false);
                    }
                    m_IncreaseButton.SetActive(true);
                    
                    var rectTransform = transform as RectTransform;
                    DOTween.To(() => rectTransform.anchoredPosition, x => rectTransform.anchoredPosition = x, new Vector2(-m_Offset * m_Index, 0f), .25f);
                }
            }
        }
    }
}