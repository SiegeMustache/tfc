﻿using DG.Tweening;
using TorkFramework;
using TorkFramework.Runtime.UI;
using UnityEngine;

namespace Kaiors.UI
{
    public class UIStatusViewModel : EntityBehaviour<UIEntity>
    {
        [SerializeField] private Vector2 m_HideLocation;
        [SerializeField] private Vector2 m_ShowLocation;
        [SerializeField] private float m_ShowTime;
        [SerializeField] private float m_HideTime;
        [SerializeField] private GameObject m_ShowButton;
        [SerializeField] private GameObject m_HideButton;

        private bool m_Showed = false;

        private void OnEnable()
        {
            m_HideButton.SetActive(false);
            m_ShowButton.SetActive(true);
            (transform as RectTransform).anchoredPosition = m_HideLocation;
            m_Showed = false;
        }

        private void OnDisable()
        {
            (transform as RectTransform).anchoredPosition = m_HideLocation;
            m_HideButton.SetActive(false);
            m_ShowButton.SetActive(true);
            m_Showed = false;
        }

        public void Show()
        {
            if (!m_Showed)
            {
                m_Showed = true;
                m_ShowButton.SetActive(false);
                m_HideButton.SetActive(true);
                var rectTransform = transform as RectTransform;
                DOTween.To(() => rectTransform.anchoredPosition, value => rectTransform.anchoredPosition = value, m_ShowLocation, m_ShowTime); 
            }
        }

        public void Hide()
        {
            if (m_Showed)
            {
                m_Showed = false;
                m_ShowButton.SetActive(true);
                m_HideButton.SetActive(false);
                var rectTransform = transform as RectTransform;
                DOTween.To(() => rectTransform.anchoredPosition, value => rectTransform.anchoredPosition = value, m_HideLocation, m_HideTime); 
            }
        }
    }
}