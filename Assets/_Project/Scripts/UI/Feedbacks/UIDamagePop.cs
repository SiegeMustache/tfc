﻿
using DG.Tweening;
using TMPro;
using TorkFramework;
using TorkFramework.Runtime.UI;
using UnityEngine;

namespace Kairos.UI.Feedbacks
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class UIDamagePop : EntityBehaviour<UIEntity>
    {
        [Header("Move Settings")] [SerializeField]
        private Vector3 m_StartLocalPosition;

        [SerializeField] private Vector3 m_EndLocalPosition;
        [SerializeField] private float m_MoveTime;

        [Header("Scale Settings")] [SerializeField]
        private float m_StartScale;

        [SerializeField] private float m_EndScale;

        private TextMeshProUGUI m_Text;

        private void Awake()
        {
            m_Text = GetComponent<TextMeshProUGUI>();
            gameObject.SetActive(false);
        }

        public void Show(int amount)
        {
            m_Text.text = amount.ToString();
            transform.localPosition = m_StartLocalPosition;
            transform.localScale = Vector3.one * m_StartScale;

            var sequence = DOTween.Sequence();
            sequence.Append(transform.DOLocalMove(m_EndLocalPosition, m_MoveTime));
            sequence.Insert(0, transform.DOScale(Vector3.one * m_EndScale, m_MoveTime));

            gameObject.SetActive(true);
            sequence.Play().onComplete += () => { Entity.GameManager.Kill(Entity); };
        }
    }
}