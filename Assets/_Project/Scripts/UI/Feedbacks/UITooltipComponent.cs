﻿using System.Collections;
using System.Collections.Generic;
using TorkFramework;
using TorkFramework.Runtime.UI;
using UnityEngine;

namespace Kairos.UI.Feedbacks
{
    public class UITooltipComponent : EntityBehaviour<UIEntity>
    {
        [SerializeField, Multiline] private string m_ShowText;
        public string ShowText
        {
            get
            {
                return m_ShowText;
            }
            set
            {
                m_ShowText = value;
            }
        }
        

        public void Show()
        {
            Entity.UIManager.GetQuerableEntity<UITooltipController>().ShowTooltip(m_ShowText);
        }

        public void Hide()
        {
            Entity.UIManager.GetQuerableEntity<UITooltipController>().HideTooltip();
        }
    }
}