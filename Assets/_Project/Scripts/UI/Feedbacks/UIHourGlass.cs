﻿using System;
using DG.Tweening;
using Kairos.Cards;
using TMPro;
using TorkFramework;
using TorkFramework.Events;
using TorkFramework.Runtime.UI;
using UnityEngine;

namespace Kairos.UI.Feedbacks
{
    public class UIHourGlass : EntityBehaviour<UIEntity>
    {
        [SerializeField] private float m_RotationTime = 1f;
        [SerializeField] private TextMeshProUGUI m_UpText;
        [SerializeField] private TextMeshProUGUI m_DownText;
        private void OnEnable()
        {
            if (Entity.GameManager.HasManager<EventManager>())
            {
                Entity.GameManager.GetManager<EventManager>().AddListener(EventId.TurnStarted, OnTurnStarted);
                Entity.GameManager.GetManager<EventManager>().AddListener(EventId.PlayerCardPlayed, OnPlayerCardPlayed);
                Entity.GameManager.GetManager<EventManager>().AddListener(EventId.PlayerCardUndo, OnPlayerCardUndo);
                Entity.GameManager.GetManager<EventManager>().AddListener(EventId.NewTimeframe, OnNewTimeframe);
            }
        }

        private void OnDisable()
        {
            Entity.GameManager.GetManager<EventManager>().RemoveListener(EventId.TurnStarted, OnTurnStarted);
            Entity.GameManager.GetManager<EventManager>().RemoveListener(EventId.PlayerCardPlayed, OnPlayerCardPlayed);
            Entity.GameManager.GetManager<EventManager>().RemoveListener(EventId.PlayerCardUndo, OnPlayerCardUndo);
            Entity.GameManager.GetManager<EventManager>().RemoveListener(EventId.NewTimeframe, OnNewTimeframe);
        }

        private void OnTurnStarted(GenericEventArgs args, Action listenerExecutedCallback)
        {
            transform.DOLocalRotate(new Vector3(0, 0, -180f), m_RotationTime).onComplete += () =>
            {
                transform.localRotation = Quaternion.identity;
                m_UpText.text = Globals.TIMELINE_SIZE.ToString();
                m_DownText.text = "0";
                listenerExecutedCallback?.Invoke();
            };
        }

        private void OnPlayerCardPlayed(GenericEventArgs args, Action listenerExecutedCallback)
        {
            var card = args.Args[0] as CardInfo;

            var upNumber = int.Parse(m_UpText.text);
            var downNumber = int.Parse(m_DownText.text);

            upNumber -= card.CardSpeed;
            downNumber += card.CardSpeed;
            
            m_UpText.text = upNumber.ToString();
            m_DownText.text = downNumber.ToString();
            listenerExecutedCallback?.Invoke();
        }
        
        private void OnPlayerCardUndo(GenericEventArgs args, Action listenerExecutedCallback)
        {
            var card = args.Args[0] as CardInfo;

            var upNumber = int.Parse(m_UpText.text);
            var downNumber = int.Parse(m_DownText.text);

            upNumber += card.CardSpeed;
            downNumber -= card.CardSpeed;
            
            m_UpText.text = upNumber.ToString();
            m_DownText.text = downNumber.ToString();
            listenerExecutedCallback?.Invoke();
        }

        private void OnNewTimeframe(GenericEventArgs args, Action listenerExecutedCallback)
        {
            var targetTime = (int) args.Args[0];
            
           var downNumber = int.Parse(m_DownText.text);

            if (downNumber < targetTime)
            {
                downNumber = targetTime;
                var upNumber = Globals.TIMELINE_SIZE - downNumber;
                
                m_UpText.text = upNumber.ToString();
                m_DownText.text = downNumber.ToString();
            }
            listenerExecutedCallback?.Invoke();
        }
    }
}