﻿using System.Collections.Generic;
using TorkFramework;
using TorkFramework.Runtime.UI;
using UnityEngine;

namespace Kairos.UI.Feedbacks
{
    public class UIDamagePopQueue : EntityBehaviour<UIEntity>
    {
        [SerializeField] private UIDamagePop m_PopPrefab;
        [SerializeField] private float m_PopDelay = .25f;

        private Queue<int> m_DamagesToShow;

        private float m_Timer = 0f;
        
        private void Start()
        {
            m_DamagesToShow = new Queue<int>();
        }

        private void Update()
        {
            if (m_Timer <= 0f)
            {
                if (m_DamagesToShow.Count > 0)
                {
                    var newPop = Entity.GameManager.Spawn<UIDamagePop, UIEntity>(m_PopPrefab);
                    newPop.transform.SetParent(transform, false);
                    newPop.Show(m_DamagesToShow.Dequeue());
                    m_Timer = m_PopDelay;
                }
            }
            else
            {
                m_Timer -= Time.deltaTime;
            }
        }

        public void PushDamage(int amount)
        {
            m_DamagesToShow.Enqueue(amount);
        }
    }
}