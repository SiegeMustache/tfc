﻿using System;
using Kairos.Cards;
using Kairos.Heroes;
using Kairos.UI.Cards;
using Kairos.Units;
using TorkFramework;
using TorkFramework.Runtime.UI;
using UnityEngine;
using UnityEngine.UI;

namespace Kairos.UI.Rewards
{
    public class UIHeroRewardPopup : EntityBehaviour<UIEntity>
    {
        [SerializeField] private Image m_HeroIllustration;
        private Action m_ResultCallback = null;
        private void Start()
        {
            Entity.UIManager.AddQuerableEntity(this);
        }

        private void OnDestroy()
        {
            Entity.UIManager.ClearQuerableEntity(GetType());
        }
        
        public void ShowHeroReward(string hero, Action resultCallback = null)
        {
            m_ResultCallback = resultCallback;
            m_HeroIllustration.sprite = UnitDatabase.LoadedUnits[HeroDatabase.LoadedHeroes[hero].HeroUnit].UnitRender.Resource as Sprite;
            Entity.GameManager.ChangeState(GameStates.HeroReward);
        }

        public void OnConfirm()
        {
            Entity.GameManager.PopState(() =>
            {
                m_ResultCallback?.Invoke();
                m_ResultCallback = null;
            });
        }
    }
}