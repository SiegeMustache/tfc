﻿using System;
using Kairos.Cards;
using Kairos.UI.Cards;
using TorkFramework;
using TorkFramework.Runtime.UI;
using UnityEngine;

namespace Kairos.UI.Rewards
{
    public class UICardRewardPopup : EntityBehaviour<UIEntity>
    {
        [SerializeField] private UICardView m_TargetCard;
        private Action<bool> m_ResultCallback = null;
        private void Start()
        {
            Entity.UIManager.AddQuerableEntity(this);
        }

        private void OnDestroy()
        {
            Entity.UIManager.ClearQuerableEntity(GetType());
        }
        
        public void ShowCardReward(CardInfo targetCard, Action<bool> resultCallback = null)
        {
            m_ResultCallback = resultCallback;
            m_TargetCard.ReferencedCard = targetCard;
            Entity.GameManager.ChangeState(GameStates.CardReward);
        }

        public void OnConfirm()
        {
            Entity.GameManager.PopState(() =>
            {
                m_ResultCallback?.Invoke(true);
                m_ResultCallback = null;
            });
        }

        public void OnCancel()
        {
            Entity.GameManager.PopState(() =>
            {
                m_ResultCallback?.Invoke(false);
                m_ResultCallback = null;
            });
        }
    }
}