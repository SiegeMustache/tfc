﻿using System;
using System.Collections.Generic;
using DG.Tweening;
using Kairos.Timeline;
using TorkFramework;
using TorkFramework.Runtime.UI;
using UnityEngine;
using UnityEngine.UI;

namespace Kairos.UI.Timeline
{
    public class UITimelineView : EntityBehaviour<UIEntity>
    {
        [SerializeField] private Slider m_TimelineSlider;
        [SerializeField] private UITimeframeView m_TimeframePrefab;
        [SerializeField] private Transform[] m_TimeframeLocations;

        private List<UITimeframeView> m_TimeframeViews;
        private bool m_HandleState = true;

        public TimelineController Timeline;

        private void Awake()
        {
            m_TimeframeViews = new List<UITimeframeView>();
            for (int i = 0; i < Globals.TIMELINE_SIZE; i++)
            {
                m_TimeframeViews.Add(Entity.GameManager.Spawn<UITimeframeView, UIEntity>(m_TimeframePrefab));
                m_TimeframeViews[i].transform.SetParent(m_TimeframeLocations[i], false);
                m_TimeframeViews[i].transform.localPosition = Vector3.zero;
            }

            m_TimelineSlider.handleRect.gameObject.SetActive(false);
            m_HandleState = false;
        }

        private void OnEnable()
        {
            for (int i = 0; i < Globals.TIMELINE_SIZE; i++)
            {
                m_TimeframeViews[i].Timeline = Timeline;
            }

        }

        private void Start()
        {
            Entity.UIManager.AddQuerableEntity(this);
        }

        private void OnDisable()
        {
            Clear();
        }

        private void OnDestroy()
        {
            Entity.UIManager.ClearQuerableEntity(typeof(UITimelineView));
        }

        public void AddActionView(TimelineController.TimelineAction action)
        {
            m_TimeframeViews[action.Timeframe - 1].AddAction(action);
        }

        public void AddSentinelActionView(TimelineController.SentinelAction action)
        {
            m_TimeframeViews[action.Timeframe - 1].AddSentinelAction(action);
        }

        public void RemoveActionView(TimelineController.TimelineAction action)
        {
            m_TimeframeViews[action.Timeframe - 1].RemoveAction(action);
        }

        public void RemoveSentinelAction(TimelineController.SentinelAction action)
        {
            m_TimeframeViews[action.Timeframe - 1].RemoveSentinelAction(action);
        }

        public void MoveHandle(int timeframe, Action handleMovedCallback)
        {
            if (!m_HandleState)
            {
                m_TimelineSlider.handleRect.gameObject.SetActive(true);
                m_HandleState = true;
            }

            timeframe--;

            m_TimeframeViews[timeframe].EnableParticle(0);
            if (timeframe >0)
            {
                m_TimeframeViews[timeframe - 1].DisableParticle(0);
                m_TimeframeViews[timeframe - 1].EnableParticle(1);
            }
            if (timeframe > 1)
            {
                m_TimeframeViews[timeframe - 2].DisableParticle(1);
                m_TimeframeViews[timeframe - 2].EnableParticle(2);
            }
            if (timeframe > 2)
            {
                m_TimeframeViews[timeframe - 3].DisableParticle(2);
                m_TimeframeViews[timeframe - 3].EnableParticle(3);
            }
            if (timeframe > 3)
            {
                m_TimeframeViews[timeframe - 3].DisableParticle(3);
            }

            Entity.GameManager.GetManager<TimeManager>().WaitTime(.5f, handleMovedCallback);

            //DOTween.To(() => m_TimelineSlider.value, x => m_TimelineSlider.value = x, timeframe, 1f).onComplete += () => handleMovedCallback?.Invoke();
        }

        public void HideHandle()
        {
            if (m_HandleState)
            {
                m_TimelineSlider.value = 1;
                m_TimelineSlider.handleRect.gameObject.SetActive(false);
                m_HandleState = false;
                foreach (var timeframeView in m_TimeframeViews)
                {
                    timeframeView.ClearParticles();
                }
            }
        }

        public void Clear()
        {
            foreach (var timeframeView in m_TimeframeViews)
            {
                timeframeView.Clear();
            }

            HideHandle();
        }

        public void ClearSentinelActions()
        {
            foreach(var timeframeView in m_TimeframeViews)
            {
                timeframeView.ClearSentinelActions();
            }
        }
    }
}