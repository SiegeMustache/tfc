﻿using System;
using DG.Tweening;
using Kairos.Timeline;
using Kairos.UI.Cards;
using Kairos.Units;
using TorkFramework;
using TorkFramework.Runtime.UI;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Kairos.UI.Timeline
{
    [RequireComponent(typeof(Image))]
    public class UITimelineActionView : EntityBehaviour<UIEntity>, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField] private UICardView m_ActionPreview;
        [SerializeField] private Image m_UnitIcon;
        [SerializeField] private Image m_ActionIcon;
        
        private TimelineController.TimelineAction m_ReferencedAction;
        public TimelineController Timeline;

        public TimelineController.TimelineAction ReferencedAction
        {
            get => m_ReferencedAction;
            set
            {
                m_ReferencedAction = value;
                if (m_ReferencedAction.Card != null)
                {
                    m_ActionIcon.sprite = m_ReferencedAction.Card.CardIcon.Resource as Sprite;
                    m_UnitIcon.sprite = m_ReferencedAction.Source.GetUnitIcon();
                    m_ActionPreview.ReferencedCard = m_ReferencedAction.Card;
                }
                else
                {
                    m_ActionIcon.sprite = null;
                    m_ActionPreview.ReferencedCard = null;
                    m_ActionPreview.TargetUnitSnapshot = null;
                }
            }
        }

        private void Awake()
        {
            m_ActionPreview.GetComponent<CanvasGroup>().alpha = 0;
        }

        private void Update()
        {
            if (m_ReferencedAction.Source.Team == Team.Player)
            {
                m_ActionPreview.TargetUnitSnapshot = Timeline.Snapshots[m_ReferencedAction.Timeframe - 1].HeroSnapshot;
            }
            else
            {
                foreach (var enemySnapshot in Timeline.Snapshots[m_ReferencedAction.Timeframe - 1].EnemiesSnapshots)
                {
                    if (enemySnapshot.SourceUnit == m_ReferencedAction.Source)
                        m_ActionPreview.TargetUnitSnapshot = enemySnapshot;
                }
            }
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (ReferencedAction.Source.Team == Team.Player)
            {
                foreach (var targetGroup in ReferencedAction.Targets)
                {
                    foreach (var target in targetGroup)
                    {
                        target.GetComponent<ISelectable>()?.OnSelect(Team.Player);
                    }
                }
            }
            else
            {
                ReferencedAction.Source.GetComponent<ISelectable>()?.OnSelect(Team.Enemy);
            }

            DOTween.To(() => m_ActionPreview.GetComponent<CanvasGroup>().alpha, x => m_ActionPreview.GetComponent<CanvasGroup>().alpha = x, 1, .25f);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (ReferencedAction.Source.Team == Team.Player)
            {
                foreach (var targetGroup in ReferencedAction.Targets)
                {
                    foreach (var target in targetGroup)
                    {
                        target.GetComponent<ISelectable>()?.OnDeselect();
                    }
                }
            }
            else
            {
                ReferencedAction.Source.GetComponent<ISelectable>()?.OnDeselect();
            }

            DOTween.To(() => m_ActionPreview.GetComponent<CanvasGroup>().alpha, x => m_ActionPreview.GetComponent<CanvasGroup>().alpha = x, 0, .25f);
        }
    }
}