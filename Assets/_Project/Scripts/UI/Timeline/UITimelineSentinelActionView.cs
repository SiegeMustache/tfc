﻿using DG.Tweening;
using Kairos.Sentinels;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using TorkFramework;
using TorkFramework.ResourceManagement;
using TorkFramework.Runtime.UI;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Kairos.UI.Timeline
{
    [RequireComponent(typeof(Image))]
    public class UITimelineSentinelActionView : EntityBehaviour<UIEntity>, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField] private GameObject m_PreviewPanel;
        [SerializeField] private Image m_SentinelIcon;
        [SerializeField] private TextMeshProUGUI m_SentinelName;
        [SerializeField] private TextMeshProUGUI m_SentinelDescription;

        private SentinelController m_ReferencedSentinel;

        public SentinelController ReferencedSentinel
        {
            get => m_ReferencedSentinel;
            set
            {
                m_ReferencedSentinel = value;
                if(m_ReferencedSentinel.SentinelData != null)
                {
                    m_SentinelIcon.sprite = m_ReferencedSentinel.SentinelData.SentinelIcon.Resource as Sprite;
                    m_SentinelName.text = m_ReferencedSentinel.SentinelData.SentinelName;
                    m_SentinelDescription.text = m_ReferencedSentinel.SentinelData.SentinelDescription;
                }
                else
                {
                    m_SentinelIcon.sprite = null;
                    m_SentinelDescription.text = "MISSING DATA";
                    m_SentinelName.text = "MISSING DATA";
                }
            }
        }

        private void Awake()
        {
            m_PreviewPanel.GetComponent<CanvasGroup>().alpha = 0;
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            DOTween.To(() => m_PreviewPanel.GetComponent<CanvasGroup>().alpha, x => m_PreviewPanel.GetComponent<CanvasGroup>().alpha = x, 1, .25f);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            DOTween.To(() => m_PreviewPanel.GetComponent<CanvasGroup>().alpha, x => m_PreviewPanel.GetComponent<CanvasGroup>().alpha = x, 0, .25f);
        }
    }
}

