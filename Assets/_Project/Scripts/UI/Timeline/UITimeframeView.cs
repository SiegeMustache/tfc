﻿using System;
using System.Collections.Generic;
using Kairos.Timeline;
using Kairos.Units;
using TorkFramework;
using TorkFramework.Runtime.UI;
using UnityEngine;
using UnityEngine.UI;

namespace Kairos.UI.Timeline
{
    public class UITimeframeView : EntityBehaviour<UIEntity>
    {
        [SerializeField] private UITimelineActionView[] m_HeroTokens;
        [SerializeField] private UITimelineSentinelActionView[] m_SentinelTokens;
        [SerializeField] private UITimelineActionView[] m_EnemyTokens;
        [SerializeField] private Transform[] m_Particles;

        public TimelineController Timeline;

        private Dictionary<TimelineController.TimelineAction, UITimelineActionView> m_ActionTokens;
        private Dictionary<TimelineController.SentinelAction, UITimelineSentinelActionView> m_SentinelActionTokens;
        private void Awake()
        {
            m_ActionTokens = new Dictionary<TimelineController.TimelineAction, UITimelineActionView>();
            m_SentinelActionTokens = new Dictionary<TimelineController.SentinelAction, UITimelineSentinelActionView>();
            ClearParticles();
            Clear();
        }

        private void OnEnable()
        {
            foreach (var heroToken in m_HeroTokens)
            {
                heroToken.Timeline = Timeline;
            }

            foreach (var enemyToken in m_EnemyTokens)
            {
                enemyToken.Timeline = Timeline;
            }
        }

        public void Clear()
        {
            foreach (var token in m_HeroTokens)
            {
                //token.gameObject.SetActive(false);
                token.transform.parent.gameObject.SetActive(false);
            }

            ClearSentinelActions();
            
            foreach (var token in m_EnemyTokens)
            {
                //token.gameObject.SetActive(false);
                token.transform.parent.gameObject.SetActive(false);
            }
            
            m_ActionTokens.Clear();
        }

        public void ClearSentinelActions()
        {
            foreach (var token in m_SentinelTokens)
            {
                token.gameObject.SetActive(false);
            }
        }

        public void AddAction(TimelineController.TimelineAction action)
        {
            if (!m_ActionTokens.ContainsKey(action))
            {
                switch (action.Source.Type)
                {
                    case UnitType.Hero:
                    {
                        // for (int i = 0; i < m_HeroTokens.Length; i++)
                        // {
                        //     if (!m_HeroTokens[i].gameObject.activeInHierarchy)
                        //     {
                        //         m_ActionTokens.Add(action, m_HeroTokens[i]);
                        //         m_HeroTokens[i].ReferencedAction = action;
                        //         m_HeroTokens[i].gameObject.SetActive(true);
                        //         return;
                        //     }
                        // }
                        
                        for (int i = 0; i < m_HeroTokens.Length; i++)
                        {
                            if (!m_HeroTokens[i].transform.parent.gameObject.activeInHierarchy)
                            {
                                m_ActionTokens.Add(action, m_HeroTokens[i]);
                                m_HeroTokens[i].ReferencedAction = action;
                                m_HeroTokens[i].gameObject.transform.parent.gameObject.SetActive(true);
                                return;
                            }
                        }
                        break;
                    }
                    case UnitType.Enemy:
                    {
                        for (int i = 0; i < m_EnemyTokens.Length; i++)
                        {
                            // if (!m_EnemyTokens[i].gameObject.activeInHierarchy)
                            // {
                            //     m_ActionTokens.Add(action, m_EnemyTokens[i]);
                            //     m_EnemyTokens[i].ReferencedAction = action;
                            //     m_EnemyTokens[i].gameObject.SetActive(true);
                            //     return;
                            // }
                            
                            if (!m_EnemyTokens[i].gameObject.transform.parent.gameObject.activeInHierarchy)
                            {
                                m_ActionTokens.Add(action, m_EnemyTokens[i]);
                                m_EnemyTokens[i].ReferencedAction = action;
                                m_EnemyTokens[i].transform.parent.gameObject.SetActive(true);
                                return;
                            }
                        }
                        break;
                    }
                }
            }
        }

        public void AddSentinelAction(TimelineController.SentinelAction action)
        {
            for (int i = 0; i < m_SentinelTokens.Length; i++)
            {
                if (!m_SentinelTokens[i].gameObject.activeInHierarchy)
                {
                    m_SentinelActionTokens.Add(action, m_SentinelTokens[i]);
                    m_SentinelTokens[i].ReferencedSentinel = action.Source;
                    m_SentinelTokens[i].gameObject.SetActive(true);
                    m_SentinelTokens[i].gameObject.GetComponent<Image>().sprite = action.Source.SentinelData.SentinelIcon.Resource as Sprite;
                    return;
                }
            }
        }

        public void RemoveAction(TimelineController.TimelineAction action)
        {
            if (m_ActionTokens.ContainsKey(action))
            {
                m_ActionTokens[action].transform.parent.gameObject.SetActive(false);
                m_ActionTokens.Remove(action);
            }
        }

        public void RemoveSentinelAction(TimelineController.SentinelAction action)
        {
            if(m_SentinelActionTokens.ContainsKey(action))
            {
                m_SentinelActionTokens[action].gameObject.GetComponent<Image>().sprite = null;
                m_SentinelActionTokens[action].gameObject.SetActive(false);
                m_SentinelActionTokens.Remove(action);
                for(int i = m_SentinelTokens.Length - 1; i >= 0; i--)
                {
                    if(m_SentinelTokens[i].gameObject.activeInHierarchy)
                    {
                        if(m_SentinelTokens[i].ReferencedSentinel == action.Source)
                        {
                            m_SentinelTokens[i].gameObject.GetComponent<Image>().sprite = null;
                            m_SentinelTokens[i].gameObject.SetActive(false);
                            break;
                        }
                    }
                }
            }
        }

        public void EnableParticle(int index)
        {
            m_Particles[index].gameObject.SetActive(true);
        }

        public void DisableParticle(int index)
        {
            m_Particles[index].gameObject.SetActive(false);
        }

        public void ClearParticles()
        {
            foreach (var particle in m_Particles)
            {
                particle.gameObject.SetActive(false);
            }
        }
    }
}