﻿using System;
using System.Collections;
using System.Collections.Generic;
using TorkFramework;
using TorkFramework.Events;
using TorkFramework.Runtime.UI;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Kairos.UI.Timeline
{
    public class UITimeframeHighlighter : EntityBehaviour<UIEntity>
    {
        public enum TimeframeStatus
        {
            Disabled,
            Highlit,
            Blinking
        }

        private TimeframeStatus m_CurrentStatus;
        private Image m_Image;
        private float m_IterativeAlphaValue = 0;
        private float m_BlinkingFrequence;
        public Color32 m_TimeframeColor;
        private UITimelineHighlighter m_TimelineHightlighter;
        [SerializeField] private int m_TimeframeIndex;

        public TimeframeStatus CurrentStatus => m_CurrentStatus;

        private void Awake()
        {
            m_Image = GetComponent<Image>();
        }

        private void Start()
        {
            m_TimelineHightlighter = GetComponentInParent<UITimelineHighlighter>();
            m_BlinkingFrequence = m_TimelineHightlighter.TimeframeHighlightBlinkSpeed;
        }

        // Update is called once per frame
        void Update()
        {
            switch(m_CurrentStatus)
            {
                case TimeframeStatus.Disabled:
                    {
                        break;
                    }
                case TimeframeStatus.Blinking:
                    {
                        m_IterativeAlphaValue += Time.unscaledDeltaTime * m_BlinkingFrequence;
                        HighlightTimeframe(m_TimeframeColor, (float)Math.Abs(Math.Sin(m_IterativeAlphaValue)));
                        break;
                    }
                case TimeframeStatus.Highlit:
                    {
                        HighlightTimeframe(m_TimeframeColor, 1f);
                        break;
                    }
            }
        }

        public void SetTimeframeHightlighter(TimeframeStatus status)
        {
            m_CurrentStatus = status;
            
            if(status == TimeframeStatus.Disabled)
            {
                var tempAlpha = m_Image.color;
                tempAlpha.a = 0f;
                m_Image.color = tempAlpha;
            }
        }

        private void HighlightTimeframe(Color color, float alphaValue)
        {
            m_Image.color = color;
            var tempAlpha = m_Image.color;
            tempAlpha.a = alphaValue;
            m_Image.color = tempAlpha;
        }

        
        private void OnMouseEnter()
        {
            if (m_CurrentStatus != TimeframeStatus.Disabled)
            {
                m_TimelineHightlighter.SetCurrentTimeframe(m_TimeframeIndex);
            }
        }

        private void OnMouseExit()
        {
            if(m_CurrentStatus != TimeframeStatus.Disabled)
            {
                m_TimelineHightlighter.SetCurrentTimeframe(0);
            }
        }

        private void OnMouseUpAsButton()
        {
            if(m_CurrentStatus != TimeframeStatus.Disabled)
            {
                m_TimelineHightlighter.AddAction(m_TimeframeIndex, () => { });
            }
        }
    }
}

