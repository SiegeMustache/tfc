﻿using Kairos.Sentinels;
using Kairos.Timeline;
using Kairos.UI.Player;
using System;
using System.Collections;
using System.Collections.Generic;
using TorkFramework;
using TorkFramework.Events;
using TorkFramework.Runtime.UI;
using UnityEngine;

namespace Kairos.UI.Timeline
{
    public class UITimelineHighlighter : EntityBehaviour<UIEntity>
    {
        [SerializeField] private UITimeframeHighlighter[] m_TimeframeHighlighters;
        public enum SentinelSelectionStatus
        {
            Disabled,
            Selected
        }
        private SentinelSelectionStatus m_CurrentStatus;
        public SentinelSelectionStatus CurrentStatus => m_CurrentStatus;

        private int m_CurrentTimeframeIndex = 0;
        private int m_NextTimeframes = 0;
        private int m_PreviousTimeframes = 0;

        public float TimeframeHighlightBlinkSpeed = 1f;
        private SentinelController m_CurrentSentinel;
        private List<int> m_ExcludedTimeframes = new List<int>();

        private void Start()
        {
            Entity.GameManager.GetManager<EventManager>().AddListener(EventId.RoundStarted, OnRoundStart);
        }

        private void OnDisable()
        {
            Entity.GameManager.GetManager<EventManager>().RemoveListener(EventId.RoundStarted, OnRoundStart);
            Clear();
        }

        private void OnDestroy()
        {
            Entity.GameManager.GetManager<EventManager>().RemoveListener(EventId.RoundStarted, OnRoundStart);
            Clear();
        }

        private void Update()
        {
            switch(m_CurrentStatus)
            {
                case SentinelSelectionStatus.Disabled:
                    {
                        m_CurrentTimeframeIndex = 0;
                        break;
                    }
                case SentinelSelectionStatus.Selected:
                    {
                        HighlightTimeframes();
                        break;
                    }
            }
        }

        private void OnRoundStart(GenericEventArgs args, Action eventCallback)
        {
            Entity.UIManager.QuerableEntities.Add(typeof(UITimelineHighlighter), this);
            m_CurrentStatus = SentinelSelectionStatus.Disabled;
            eventCallback?.Invoke();
        }

        private void HighlightTimeframes()
        {
            if(m_CurrentTimeframeIndex == 0)
            {
                for(int i = 0; i < m_TimeframeHighlighters.Length; i++)
                {
                    if(!m_ExcludedTimeframes.Contains(i))
					{
                        m_TimeframeHighlighters[i].SetTimeframeHightlighter(UITimeframeHighlighter.TimeframeStatus.Blinking);
                    }
                }
            }
            else
            {
                List<int> iterativeListOfIndexes = new List<int>();
                iterativeListOfIndexes.Add(m_CurrentTimeframeIndex - 1);
                for(int i = 0; i < m_NextTimeframes + 1; i++)
                {
                    int nextIndex = (m_CurrentTimeframeIndex - 1) + i;
                    if (nextIndex < Globals.TIMELINE_SIZE)
                    {
                        iterativeListOfIndexes.Add(nextIndex);
                    }
                }
                for(int j = 0; j < m_PreviousTimeframes + 1; j++)
                {
                    int previousIndex = (m_CurrentTimeframeIndex - 1) - j;
                    if(previousIndex >= 0)
                    {
                        iterativeListOfIndexes.Add(previousIndex);
                    }
                }
                

                for(int k = 0; k < m_TimeframeHighlighters.Length; k++)
                {
                    if(iterativeListOfIndexes.Contains(k) && !m_ExcludedTimeframes.Contains(k))
                    {
                        m_TimeframeHighlighters[k].SetTimeframeHightlighter(UITimeframeHighlighter.TimeframeStatus.Highlit);
                    }
                    else
                    {
                        m_TimeframeHighlighters[k].SetTimeframeHightlighter(UITimeframeHighlighter.TimeframeStatus.Disabled);
                    }
                }
            }
        }

        public void OnSentinelSelected(SentinelController sentinel)
        {
            m_NextTimeframes = sentinel.CurrentEffect.NextTargetTimeframes;
            m_PreviousTimeframes = sentinel.CurrentEffect.PreviousTargetTimeframes;
            m_CurrentSentinel = sentinel;
            m_CurrentStatus = SentinelSelectionStatus.Selected;
            (Entity.GameManager.GetManager<UIManager>().QuerableEntities[typeof(UIPlayerController)] as UIPlayerController).ControllerState = UIPlayerController.UIPlayerControllerState.PlacingSentinel;
            TimelineController timeline = sentinel.TimelineController;

            foreach(TimelineController.SentinelAction sentinelAction in timeline.SentinelActions)
			{
                if(sentinelAction.Source == sentinel)
				{
                    m_ExcludedTimeframes.Add(sentinelAction.Timeframe - 1);
				}
			}
        }

        public void Clear()
        {
            m_CurrentTimeframeIndex = 0;
            m_NextTimeframes = 0;
            m_PreviousTimeframes = 0;
            m_CurrentSentinel = null;
            m_ExcludedTimeframes.Clear();
            foreach(UITimeframeHighlighter highlighter in m_TimeframeHighlighters)
            {
                highlighter.SetTimeframeHightlighter(UITimeframeHighlighter.TimeframeStatus.Disabled);
            }
            m_CurrentStatus = SentinelSelectionStatus.Disabled;
            try
            {
                (Entity.GameManager.GetManager<UIManager>().QuerableEntities[typeof(UIPlayerController)] as UIPlayerController).ControllerState = UIPlayerController.UIPlayerControllerState.Idle;
            }
            catch (Exception ex)
            {
                
            }
        }

        public void SetCurrentTimeframe(int targeTimeframe)
        {
            m_CurrentTimeframeIndex = targeTimeframe;
        }

        public void AddAction(int targetTimeframe, Action executionCallback)
        {
            Entity.GameManager.GetManager<EventManager>().TriggerEvent(EventId.SentinelPlaced, this, executionCallback, m_CurrentSentinel, targetTimeframe);
            m_CurrentSentinel.m_CurrentState = SentinelController.SentinelState.EffectPlaced;
            Clear();
        }
    }
}

