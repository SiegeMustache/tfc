﻿using Kairos.Timeline;
using Kairos.Units;
using TorkFramework;
using TorkFramework.Runtime.UI;
using UnityEngine;
using UnityEngine.UI;

namespace Kairos.UI.Units
{
    public class UIBoardView : EntityBehaviour<UIEntity>
    {
        [SerializeField] private UIUnitView m_UnitViewPrefab;
        [SerializeField] private UIUnitView m_HeroViewPrefab;

        [SerializeField] private UIEntity m_HeroLocation;
        [SerializeField] private UIEntity m_EnemiesGroupLocation;
        
        private void Start()
        {
            Entity.UIManager.QuerableEntities.Add(typeof(UIBoardView), this);
        }

        private void OnDisable()
        {
            Clear();
        }

        private void Clear()
        {
            Entity.GameManager.KillChildren(m_HeroLocation);
            Entity.GameManager.KillChildren(m_EnemiesGroupLocation);
        }

        public void AddHeroPanel(UnitController hero)
        {
            var newPanel = Entity.GameManager.Spawn<UIUnitView, UIEntity>(m_HeroViewPrefab);
            newPanel.transform.SetParent(m_HeroLocation.transform, false);
            newPanel.Timeline = FindObjectOfType<TimelineController>();
            newPanel.ReferencedUnit = hero;
        }

        public void AddEnemyPanel(UnitController enemy)
        {
            var newPanel = Entity.GameManager.Spawn<UIUnitView, UIEntity>(m_UnitViewPrefab);
            newPanel.transform.SetParent(m_EnemiesGroupLocation.transform, false);
            
            newPanel.Timeline = FindObjectOfType<TimelineController>();
            newPanel.ReferencedUnit = enemy;
            LayoutRebuilder.MarkLayoutForRebuild(m_EnemiesGroupLocation.transform as RectTransform);
        }

    }

}