﻿using Kairos.Statuses;
using Kairos.UI.Feedbacks;
using TMPro;
using TorkFramework;
using TorkFramework.Runtime.UI;
using UnityEngine;
using UnityEngine.UI;

namespace Kairos.UI.Units
{
    public class UIStatusView : EntityBehaviour<UIEntity>
    {
        [SerializeField] private Image m_StatusIcon;
        [SerializeField] private TextMeshProUGUI m_StatusCountText;
        [SerializeField] private UITooltipComponent m_Tooltip;

        public void SetStatusView(StatusInfo status, int value)
        {
            m_StatusIcon.sprite = status.StatusSprite.Resource as Sprite;
            m_StatusCountText.text = value.ToString();
            m_Tooltip.ShowText = status.StatusDescription;
        }

    }
}