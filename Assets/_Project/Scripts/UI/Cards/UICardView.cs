﻿using Kairos.Cards;
using Kairos.Keywords;
using Kairos.Timeline;
using Kairos.UI.Keywords;
using Kairos.Units;
using TMPro;
using TorkFramework;
using TorkFramework.Runtime.UI;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Kairos.UI.Cards
{
    public class UICardView : EntityBehaviour<UIEntity>, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField] protected int m_TooltipOrder = 1;
        [SerializeField] protected TextMeshProUGUI m_CardNameText;
        [SerializeField] protected TextMeshProUGUI m_CardDescriptionText;
        [SerializeField] protected TextMeshProUGUI m_CardSpeedText;
        [SerializeField] protected Image m_CardImage;
        [SerializeField] protected Image m_CardBackgroundImage;
        [SerializeField] protected float m_ShowHintTime = .5f;
        [SerializeField] protected Image m_IllustrationMask;
        
        [Header("Masks")]
        [SerializeField] protected Sprite m_NormalMask;
        [SerializeField] protected Sprite m_EnemyMask;

        [Header("Rarity Backgrounds")] 
        [SerializeField] protected Sprite BaseBackground;
        [SerializeField] protected Sprite CommonBackground;
        [SerializeField] protected Sprite RareBackground;
        [SerializeField] protected Sprite LeggendaryBackground;
        [SerializeField] protected Sprite EnemyBackground;

        protected CardInfo m_ReferencedCard;

        protected float m_SelectedTimer = 0f;
        public bool m_Selected { get; protected set; }
        protected float m_LastTimeCheck = 0f;

        protected Vector3 m_LastMouseInput;

        public bool Playable = true;

        public CardInfo ReferencedCard
        {
            get => m_ReferencedCard;
            set
            {
                m_ReferencedCard = value;
                if (m_ReferencedCard != null)
                {
                    m_CardNameText.text = m_ReferencedCard.CardName;
                    m_CardImage.sprite = m_ReferencedCard.CardImage.Resource as Sprite;
                    int speedChanged;
                    m_CardSpeedText.text = CardHelper.GetEffectiveCardSpeed(ReferencedCard, TargetUnitSnapshot, out speedChanged).ToString();
                    if (speedChanged > 0)
                        m_CardSpeedText.color = Color.green;
                    else if (speedChanged < 0)
                        m_CardSpeedText.color = Color.red;
                    else
                        m_CardSpeedText.color = Color.white;
                    m_CardDescriptionText.text = KeywordDatabase.GetFormattedText(CardHelper.GetEffectiveCardDescription(ReferencedCard, TargetUnitSnapshot));

                    switch (m_ReferencedCard.Rarity)
                    {
                        case CardInfo.CardRarity.Base:
                        {
                            m_IllustrationMask.sprite = m_NormalMask;
                            m_CardSpeedText.gameObject.SetActive(true);
                            m_CardBackgroundImage.sprite = BaseBackground;
                            break;
                        }
                        case CardInfo.CardRarity.Common:
                        {
                            m_IllustrationMask.sprite = m_NormalMask;
                            m_CardSpeedText.gameObject.SetActive(true);
                            m_CardBackgroundImage.sprite = CommonBackground;
                            break;
                        }
                        case CardInfo.CardRarity.Rare:
                        {
                            m_IllustrationMask.sprite = m_NormalMask;
                            m_CardSpeedText.gameObject.SetActive(true);
                            m_CardBackgroundImage.sprite = RareBackground;
                            break;
                        }
                        case CardInfo.CardRarity.Legendary:
                        {
                            m_IllustrationMask.sprite = m_NormalMask;
                            m_CardSpeedText.gameObject.SetActive(true);
                            m_CardBackgroundImage.sprite = LeggendaryBackground;
                            break;
                        }
                        case CardInfo.CardRarity.Enemy:
                        {
                            m_IllustrationMask.sprite = m_EnemyMask;
                            m_CardSpeedText.gameObject.SetActive(false);
                            m_CardBackgroundImage.sprite = EnemyBackground;
                            break;
                        }
                    }
                }
            }
        }

        //public UnitController TargetUnit { get; set; }
        public TimelineController.TimelineUnitSnapshot? TargetUnitSnapshot { get; set; } = null;

        protected virtual void Update()
        {
            if (ReferencedCard != null)
            {
                int speedChanged;
                m_CardSpeedText.text = CardHelper.GetEffectiveCardSpeed(ReferencedCard, TargetUnitSnapshot, out speedChanged).ToString();
                if (speedChanged > 0)
                    m_CardSpeedText.color = Color.green;
                else if (speedChanged < 0)
                    m_CardSpeedText.color = Color.red;
                else
                    m_CardSpeedText.color = Color.white;
                m_CardDescriptionText.text = KeywordDatabase.GetFormattedText(CardHelper.GetEffectiveCardDescription(ReferencedCard, TargetUnitSnapshot));

                if (m_Selected)
                {
                    m_SelectedTimer += Time.deltaTime;
                    if (m_SelectedTimer >= m_ShowHintTime && m_LastTimeCheck < m_ShowHintTime)
                    {
                        Entity.UIManager.GetQuerableEntity<UIKeywordExplanationListPanel>().ShowCardExplanation(this, m_TooltipOrder);
                    }

                    m_LastTimeCheck = m_SelectedTimer;
                }
            }
        }

        public void OnHoverEnter()
        {
            m_SelectedTimer = 0f;
            m_LastTimeCheck = 0f;
            m_Selected = true;
        }

        public void OnHoverExit()
        {
            m_Selected = false;
            if (m_LastTimeCheck >= m_ShowHintTime)
                Entity.UIManager.GetQuerableEntity<UIKeywordExplanationListPanel>().HideCardExplanation();
        }

        public virtual void OnPointerEnter(PointerEventData eventData)
        {
            OnHoverEnter();
        }

        public virtual void OnPointerExit(PointerEventData eventData)
        {
            OnHoverExit();
        }
    }
}