﻿using System;
using DG.Tweening;
using TorkFramework.Events;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Kairos.UI.Cards
{
    public class UIHandCardView : UICardView, IPointerDownHandler, IPointerUpHandler
    {
        [SerializeField] private float m_HoverScaleSize;
        [SerializeField] private Canvas m_SortCanvas;
        [SerializeField] private CanvasGroup m_FadeGroup;
        [SerializeField] private int m_DefaultSort;
        [SerializeField] private int m_SelectedSort;
        [SerializeField] private float m_ActivationY;
        [SerializeField] private Vector2 m_DragOffset;

        public bool Selected { get; private set; }

        private Camera m_UICamera;

        public Camera UiCamera
        {
            get
            {
                if (m_UICamera == null)
                    m_UICamera = GameObject.FindWithTag("UICamera").GetComponent<Camera>();
                return m_UICamera;
            }
        }

        public bool HasOffset { get; private set; } = false;

        public override void OnPointerEnter(PointerEventData eventData)
        {
            base.OnPointerEnter(eventData);
            transform.DOScale(m_HoverScaleSize, .25f);
            m_SortCanvas.sortingOrder = m_SelectedSort;
            HasOffset = true;
        }

        public override void OnPointerExit(PointerEventData eventData)
        {
            base.OnPointerExit(eventData);
            transform.DOScale(1, .25f);
            m_SortCanvas.sortingOrder = m_DefaultSort;
            HasOffset = false;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            Selected = true;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            Selected = false;
            if ((transform as RectTransform).localPosition.y > m_ActivationY)
            {
                DOTween.To(() => m_FadeGroup.alpha, x => m_FadeGroup.alpha = x, 0, .25f).onComplete += () =>
                {
                    Entity.GameManager.GetManager<EventManager>().TriggerEvent(EventId.PlayCardRequest, this, () => { DOTween.To(() => m_FadeGroup.alpha, x => m_FadeGroup.alpha = x, 1, .25f); }, ReferencedCard);
                };
            }
        }

        protected override void Update()
        {
            try
            {
                base.Update();
            }
            catch (Exception ex)
            {
                Debug.Log(ex.Message);
            }

            if (Selected)
            {
                var screenPoint = Input.mousePosition;
                screenPoint.z = 10.0f;
                transform.localPosition = Input.mousePosition - Vector3.right * UiCamera.scaledPixelWidth / 2f;
            }
        }

        private void OnDisable()
        {
            Entity.GameManager.KillChildren(Entity);
        }
    }
}