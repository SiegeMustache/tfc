﻿using System;
using TMPro;
using TorkFramework;
using TorkFramework.Runtime.UI;
using UnityEngine;

namespace Kairos.UI.Popups
{
    public class SingleQuestionPopupController : EntityBehaviour<UIPage>
    {
        [SerializeField] private TextMeshProUGUI m_ShowText;
        [SerializeField] private TextMeshProUGUI m_ConfirmText;
        [SerializeField] private TextMeshProUGUI m_CancelText;

        private Action<bool> m_ResultCallback = null;

        private void Start()
        {
            Entity.UIManager.AddQuerableEntity(this);
        }

        private void OnDestroy()
        {
            Entity.UIManager.ClearQuerableEntity(GetType());
        }

        public void ShowConfirmBox(string text, string confirmText = "Confirm", string cancelText = "Cancel", Action<bool> resultCallback = null)
        {
            m_ShowText.text = text;
            m_ConfirmText.text = confirmText;
            m_CancelText.text = cancelText;
            m_ResultCallback = resultCallback;
            Entity.GameManager.ChangeState(GameStates.ConfirmPopup);
        }

        public void OnConfirm()
        {
            Entity.GameManager.PopState(() =>
            {
                m_ResultCallback?.Invoke(true);
                m_ResultCallback = null;
            });
        }

        public void OnCancel()
        {
            Entity.GameManager.PopState(() =>
            {
                m_ResultCallback?.Invoke(false);
                m_ResultCallback = null;
            });
        }
    }
}