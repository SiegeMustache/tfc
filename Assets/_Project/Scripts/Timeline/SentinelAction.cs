﻿using System;
using Kairos.Sentinels;

namespace Kairos.Timeline
{
    public partial class TimelineController
    {
        public struct SentinelAction : IComparable
        {
            public readonly SentinelController Source;
            public readonly int Timeframe;
            public int SentinelOrder;

            public SentinelAction(SentinelController source, int targetTimeframe, int sentinelOrder)
            {
                Source = source;
                Timeframe = targetTimeframe;
                SentinelOrder = sentinelOrder;
            }

            public int CompareTo(object obj)
            {
                var otherAction = (SentinelAction) obj;

                if (otherAction.Timeframe == Timeframe)
                {
                    return otherAction.SentinelOrder.CompareTo(SentinelOrder);
                }
                else
                {
                    return otherAction.Timeframe.CompareTo(Timeframe);
                }
            }
        }
    }
}