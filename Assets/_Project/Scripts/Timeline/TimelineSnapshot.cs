﻿using System;

namespace Kairos.Timeline
{
    public partial class TimelineController
    {
        [Serializable]
        public struct TimelineSnapshot
        {
            public TimelineUnitSnapshot HeroSnapshot;
            public TimelineUnitSnapshot[] EnemiesSnapshots;
            public int TimeframeAssigned;

            public TimelineSnapshot(TimelineUnitSnapshot heroSnapshot, TimelineUnitSnapshot[] enemiesSnapshot, int timeframe)
            {
                HeroSnapshot = heroSnapshot;
                EnemiesSnapshots = enemiesSnapshot;
                TimeframeAssigned = timeframe;
            }

            public TimelineSnapshot(TimelineSnapshot other)
            {
                HeroSnapshot = new TimelineUnitSnapshot(other.HeroSnapshot);
                EnemiesSnapshots = new TimelineUnitSnapshot[other.EnemiesSnapshots.Length];
                for (int i = 0; i < EnemiesSnapshots.Length; i++)
                {
                    EnemiesSnapshots[i] = new TimelineUnitSnapshot(other.EnemiesSnapshots[i]);
                }
                TimeframeAssigned = other.TimeframeAssigned;
            }

            public void UpdateFromOther(TimelineSnapshot otherSnapshot)
			{
                HeroSnapshot.UpdateFromOther(otherSnapshot.HeroSnapshot);
                for (int i = 0; i < EnemiesSnapshots.Length; i++)
                {
                    EnemiesSnapshots[i].UpdateFromOther(otherSnapshot.EnemiesSnapshots[i]);
                }
            }
        }
    }
}