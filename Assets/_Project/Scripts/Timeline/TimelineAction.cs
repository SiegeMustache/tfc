﻿using System;
using System.Collections.Generic;
using Kairos.Cards;
using Kairos.Units;

namespace Kairos.Timeline
{
    public partial class TimelineController
    {
        public class TimelineAction : IComparable
        {
            public readonly CardInfo Card;
            public readonly UnitController Source;
            public readonly List<UnitController[]> Targets;
            public readonly int Timeframe;
            public bool HasBeenShifted;
            public bool Executed;

            public TimelineAction()
            {
            }

            public TimelineAction(CardInfo card, UnitController source, List<UnitController[]> targets, int timeframe)
            {
                Card = card;
                Source = source;
                Targets = targets;
                Timeframe = timeframe;
                HasBeenShifted = false;
                Executed = false;
            }

            public int CompareTo(object obj)
            {
                var otherAction = (TimelineAction) obj;

                if (otherAction.Timeframe == Timeframe)
                {
                    if (otherAction.Card.CardPriority.CompareTo(Card.CardPriority) == 0)
                    {
                        if (otherAction.Card.Equals(Card) && otherAction.Source == Source && otherAction.Timeframe == Timeframe)
                            return 0;
                        return 1;
                    }
                    else
                    {
                        return otherAction.Card.CardPriority.CompareTo(Card.CardPriority);
                    }
                }
                else
                {
                    return otherAction.Timeframe.CompareTo(Timeframe);
                }
            }
        }
    }
}