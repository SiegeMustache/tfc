﻿using System;
using System.Collections.Generic;
using Kairos.Cards;
using Kairos.Controllers;
using Kairos.Sentinels;
using Kairos.UI.Player;
using Kairos.UI.Timeline;
using Kairos.Units;
using TorkFramework;
using TorkFramework.Events;
using TorkFramework.Runtime.UI;
using UnityEngine;

namespace Kairos.Timeline
{
    public partial class TimelineController : EntityBehaviour<GameEntity>
    {
        private List<TimelineAction> m_Actions;
        private List<SentinelAction> m_SentinelActions;

        private int m_ActualTimeframe = 0;
        private int m_ActualSentinel = 0;
        [HideInInspector] public bool m_SentinelActionsExecuted = false;
        private UITimelineView m_View;
        private BoardController m_Board;
        private SelectionController m_Selection;

        private bool m_Running = false;

        public int ActualTimeframe => m_ActualTimeframe;
        public List<SentinelAction> SentinelActions => m_SentinelActions;

        public TimelineSnapshot[] Snapshots;

        public void Load(BoardController board, SelectionController selection)
        {
            m_Actions = new List<TimelineAction>();
            m_ActualTimeframe = 0;
            m_View = Entity.GameManager.GetManager<UIManager>().QuerableEntities[typeof(UITimelineView)] as UITimelineView;
            m_View.Timeline = this;
            Entity.GameManager.GetManager<UIManager>().GetQuerableEntity<UIPlayerController>().HandView.Timeline = this;
            m_Board = board;
            m_Selection = selection;

            m_SentinelActions = new List<SentinelAction>();
        }

        public void Unload()
        {
            Entity.GameManager.GetManager<UIManager>().GetQuerableEntity<UIPlayerController>().HandView.Timeline = null;
        }

        public void AddAction(TimelineAction action)
        {
            if (!m_Actions.Contains(action))
            {
                m_Actions.Add(action);
                m_Actions.Sort((x, y) => y.CompareTo(x));
                UpdateSnapshots();
                try
                {
                    m_View.AddActionView(action);
                }
                catch (Exception ex)
                {
                    Debug.LogError("Can't show action. " + ex.Message);
                }
            }
        }

        public void AddSentinelAction(SentinelAction action)
        {
            Debug.Log("ACTION ADDED");
            action.SentinelOrder = m_ActualSentinel;
            if (!m_SentinelActions.Contains(action))
            {
                m_SentinelActions.Add(action);
            }

            m_ActualSentinel += 1;
            m_SentinelActions.Sort();
            try
            {
                m_View.AddSentinelActionView(action);
            }
            catch (Exception ex)
            {
                Debug.LogError("Can't show action. " + ex.Message);
            }
            UpdateSnapshots();
        }

        public void AddSentinelAction(SentinelController source, int targetTimeframe, Action sentinelActionAddedCallback)
        {
            Debug.Log("ACTION ADDED");
            List<SentinelAction> actionList = new List<SentinelAction>();

            if (source.CurrentEffect.PreviousTargetTimeframes > 0)
            {
                for (int i = 1; i < source.CurrentEffect.PreviousTargetTimeframes + 1; i++)
                {
                    if (targetTimeframe - i > 0)
                    {
                        SentinelAction action = new SentinelAction(source, targetTimeframe - i, m_ActualSentinel);
                        actionList.Add(action);
                    }
                }
            }


            if (targetTimeframe > 0 && targetTimeframe <= Globals.TIMELINE_SIZE)
            {
                SentinelAction action = new SentinelAction(source, targetTimeframe, m_ActualSentinel);
                actionList.Add(action);
            }

            if (source.CurrentEffect.NextTargetTimeframes > 0)
            {
                for (int i = 1; i < source.CurrentEffect.NextTargetTimeframes + 1; i++)
                {
                    if (targetTimeframe + i <= Globals.TIMELINE_SIZE)
                    {
                        SentinelAction action = new SentinelAction(source, targetTimeframe + i, m_ActualSentinel);
                        actionList.Add(action);
                    }
                }
            }

            foreach (SentinelAction action in actionList)
            {
                m_SentinelActions.Add(action);
                m_View.AddSentinelActionView(action);                
            }

            m_SentinelActions.Sort();
            m_ActualSentinel += 1;
            source.CurrentActivations -= 1;
            UpdateSnapshots();
            sentinelActionAddedCallback?.Invoke();
        }

        public void RemoveAction(TimelineAction action)
        {
            if (m_Actions.Contains(action))
            {
                m_Actions.Remove(action);
                m_View.RemoveActionView(action);
                UpdateSnapshots();
            }
        }

        public void RemoveUnitActions(UnitController sourceUnit)
        {
            for (int i = m_Actions.Count - 1; i >= 0; i--)
            {
                if (m_Actions[i].Source == sourceUnit && !m_Actions[i].Executed)
                {
                    RemoveAction(m_Actions[i]);
                }
            }
            UpdateSnapshots();
        }

        public void UndoSentinelAction(SentinelAction action, Action sentinelActionRemovedCallback)
        {
            Debug.Log("REMOVED");
            List<SentinelAction> iterativeList = new List<SentinelAction>();
            SentinelController source = action.Source;

            foreach (SentinelAction sentinelAction in m_SentinelActions)
            {
                if (sentinelAction.Source == action.Source && sentinelAction.SentinelOrder == m_ActualSentinel)
                {
                    iterativeList.Add(sentinelAction);
                }
            }

            m_ActualSentinel -= 1;
            m_View.RemoveSentinelAction(action);

            foreach (SentinelAction iterativeAction in iterativeList)
            {
                if (m_SentinelActions.Contains(iterativeAction))
                {
                    m_SentinelActions.Remove(iterativeAction);
                }
            }

            m_SentinelActions.Sort();
            UpdateSnapshots();
            sentinelActionRemovedCallback?.Invoke();
        }

        public void UndoSentinelAction(SentinelController source, Action sentinelActionUndidCallback)
        {

            List<SentinelAction> iterativeList = new List<SentinelAction>();

            foreach (SentinelAction sentinelAction in m_SentinelActions)
            {
                if (sentinelAction.Source == source && sentinelAction.SentinelOrder == m_ActualSentinel -1)
                {
                    iterativeList.Add(sentinelAction);
                }
            }

            foreach (SentinelAction iterativeAction in iterativeList)
            {
                if (m_SentinelActions.Contains(iterativeAction))
                {
                    Debug.Log("UNDO");
                    m_View.RemoveSentinelAction(iterativeAction);
                    m_SentinelActions.Remove(iterativeAction);
                }
            }

            m_ActualSentinel -= 1;
            m_SentinelActions.Sort();
            UpdateSnapshots();
            sentinelActionUndidCallback?.Invoke();
        }

        public void ClearAllSentinelActions()
        {
            m_SentinelActions.Clear();
            UpdateSnapshots();
        }

        public void ExecuteTimeline(Action timelineExecutedCallback)
        {
            if (m_Running)
            {
                timelineExecutedCallback?.Invoke();
                return;
            }

            m_Running = true;
            ExecuteTimeframes(Globals.TIMELINE_SIZE, () =>
            {
                m_Running = false;
                UpdateSnapshots();
                timelineExecutedCallback?.Invoke();
            });
        }

        private void ExecuteTimeframes(int count, Action timeframeExecutedCallback)
        {
            if (m_ActualTimeframe >= Globals.TIMELINE_SIZE)
            {
                timeframeExecutedCallback?.Invoke();
                m_SentinelActionsExecuted = false;
                Entity.GameManager.GetManager<EventManager>().TriggerEvent(EventId.TimelineEndReached, this);
            }
            else if (count <= 0)
            {
                timeframeExecutedCallback?.Invoke();
            }
            else
            {
                m_ActualTimeframe++;

                Entity.GameManager.GetManager<EventManager>().TriggerEvent(EventId.NewTimeframe, this,
                    () => { m_View.MoveHandle(m_ActualTimeframe, () => { ExecutePriorityRecursive(CardInfo.Priority.HeroPassivePriority, () => { ExecuteTimeframes(count - 1, timeframeExecutedCallback); }); }); },
                    m_ActualTimeframe);
            }
        }

        public void ExecuteSentinelActionsInTimeframe(int timeframe, Action timeframeExecuted)
        {
            if (m_SentinelActionsExecuted == false)
            {
                var iterativeList = new List<SentinelAction>();
                foreach (var action in m_SentinelActions)
                {
                    if (action.Timeframe == timeframe)
                    {
                        iterativeList.Add(action);
                    }
                }

                if (iterativeList.Count <= 0)
                {
                    timeframeExecuted?.Invoke();
                }
                else
                {
                    iterativeList.Sort();
                    foreach (var action in iterativeList)
                    {
                        action.Source.CurrentEffect.ExecuteEffect(timeframe, m_Board, this, () => { timeframeExecuted?.Invoke(); });
                    }
                }
            }
            else
            {
                timeframeExecuted?.Invoke();
            }
        }

        private void ExecutePriorityRecursive(CardInfo.Priority actualPriority, Action prioritiesExecutedCallback)
        {
            if (actualPriority == CardInfo.Priority.DO_NOT_USE)
            {
                prioritiesExecutedCallback?.Invoke();
            }
            else
            {
                var actionsToExecute = new List<int>();
                for (var i = 0; i < m_Actions.Count; i++)
                {
                    var action = m_Actions[i];
                    if (action.Timeframe == m_ActualTimeframe && action.Card.CardPriority == actualPriority)
                        actionsToExecute.Add(i);
                }

                int counter = actionsToExecute.Count;
                if (counter == 0)
                {
                    ExecutePriorityRecursive(actualPriority + 1, prioritiesExecutedCallback);
                }
                else
                {
                    foreach (var actionIndex in actionsToExecute)
                    {
                        if (actionIndex >= 0 && actionIndex < m_Actions.Count && !m_Actions[actionIndex].Executed)
                        {
                            m_Actions[actionIndex].Executed = true;
                            m_Actions[actionIndex].Card.Play(m_Actions[actionIndex], m_Board, this, () =>
                            {
                                counter--;
                                if (counter == 0)
                                    ExecutePriorityRecursive(actualPriority + 1, prioritiesExecutedCallback);
                            });
                        }
                        else
                        {
                            counter--;
                            if (counter == 0)
                                ExecutePriorityRecursive(actualPriority + 1, prioritiesExecutedCallback);
                        }
                    }
                }
            }
        }

        public void ShiftForwardAction(int timeframeShiftAmount, int targetTimeframe, Team targetTeam, Action actionShiftedCallback)
        {
            List<TimelineAction> iterativeActionList = new List<TimelineAction>();

            foreach (TimelineAction action in m_Actions)
            {
                if (action.Timeframe == targetTimeframe && targetTimeframe < Globals.TIMELINE_SIZE && targetTimeframe > 0)
                {
                    if (action.Source.Team == targetTeam && action.HasBeenShifted == false)
                    {
                        iterativeActionList.Add(action);
                    }
                }
            }

            foreach (TimelineAction action in iterativeActionList)
            {
                RemoveAction(action);
            }

            foreach (TimelineAction action in iterativeActionList)
            {
                if (action.Timeframe + timeframeShiftAmount > Globals.TIMELINE_SIZE)
                {
                    TimelineAction newTimelineAction = new TimelineAction(action.Card, action.Source, action.Targets, Globals.TIMELINE_SIZE);
                    newTimelineAction.HasBeenShifted = true;
                    AddAction(newTimelineAction);
                }
                else
                {
                    TimelineAction newTimelineAction = new TimelineAction(action.Card, action.Source, action.Targets, action.Timeframe + timeframeShiftAmount);
                    newTimelineAction.HasBeenShifted = true;
                    AddAction(newTimelineAction);
                }
            }
        }

        public void ShiftBackwardAction(int timeframeShiftAmount, int targetTimeframe, Team targetTeam, Action actionShiftCallback)
        {
            List<TimelineAction> iterativeActionList = new List<TimelineAction>();

            foreach (TimelineAction action in m_Actions)
            {
                if (action.Timeframe == targetTimeframe && targetTimeframe > 1 && targetTimeframe <= Globals.TIMELINE_SIZE)
                {
                    if (action.Source.Team == targetTeam && action.HasBeenShifted == false)
                    {
                        iterativeActionList.Add(action);
                    }
                }
            }

            foreach (TimelineAction action in iterativeActionList)
            {
                RemoveAction(action);
            }

            foreach (TimelineAction action in iterativeActionList)
            {
                if (action.Timeframe - timeframeShiftAmount < 1)
                {
                    TimelineAction newTimelineAction = new TimelineAction(action.Card, action.Source, action.Targets, 1);
                    newTimelineAction.HasBeenShifted = true;
                    AddAction(newTimelineAction);
                }
                else
                {
                    TimelineAction newTimelineAction = new TimelineAction(action.Card, action.Source, action.Targets, action.Timeframe - timeframeShiftAmount);
                    newTimelineAction.HasBeenShifted = true;
                    AddAction(newTimelineAction);
                }
            }
        }

        public void Clear()
        {
            m_Actions.Clear();
            m_ActualTimeframe = 0;
            m_View.Clear();
            ClearAllSentinelActions();
            UpdateSnapshots();
        }

        public void AssignCard(bool checkSentences, CardInfo targetCard, UnitController targetUnit, Action<bool, TimelineAction> cardAssignedCallback)
        {
            var playerNextTimeframe = GetFirstPlayableTimeframe(targetUnit);

            if (playerNextTimeframe > Globals.TIMELINE_SIZE)
            {
                cardAssignedCallback?.Invoke(false, new TimelineAction());
                return;
            }

            var effectiveSpeed = Mathf.Clamp(targetCard.CardSpeed + targetUnit.SpeedModifer, 1, Globals.TIMELINE_SIZE);

            var targetTimeframe = playerNextTimeframe + effectiveSpeed;

            if (targetTimeframe > Globals.TIMELINE_SIZE)
            {
                cardAssignedCallback?.Invoke(false, new TimelineAction());
                return;
            }

            targetCard.GetTargets(checkSentences, targetUnit, m_Selection, m_Board, this, targetTimeframe, (result, targets) =>
            {
                m_Selection.Flush();
                if (!result)
                {
                    cardAssignedCallback?.Invoke(false, new TimelineAction());
                }
                else
                {
                    var newAction = new TimelineAction(targetCard, targetUnit, targets, targetTimeframe);
                    AddAction(newAction);
                    cardAssignedCallback?.Invoke(true, newAction);
                }
            });
        }

        public int GetFirstPlayableTimeframe(UnitController targetUnit)
        {
            if (targetUnit.DisableCardPlay)
                return int.MaxValue;

            var maxTimeframe = m_ActualTimeframe;

            foreach (var timelineAction in m_Actions)
            {
                if (timelineAction.Source == targetUnit)
                {
                    if (timelineAction.Timeframe > maxTimeframe)
                        maxTimeframe = timelineAction.Timeframe;
                }
            }

            return maxTimeframe;
        }

        public List<UnitController> GetUnitsAtTimeframe(int timeframe)
        {
            List<UnitController> result = new List<UnitController>();

            foreach (var action in m_Actions)
            {
                if (action.Timeframe == timeframe)
                {
                    if (!result.Contains(action.Source))
                        result.Add(action.Source);
                }
            }

            return result;
        }

        public bool IsFirstPlayedAction(TimelineAction iAction)
        {
            foreach (var action in m_Actions)
            {
                if (action.Source == iAction.Source)
                {
                    if (action.CompareTo(iAction) == 0)
                        return true;
                    return false;
                }
            }

            return false;
        }

        public void UpdateSnapshots()
        {
            Snapshots = new TimelineSnapshot[Globals.TIMELINE_SIZE + 1];
            
            var heroSnapshot = new TimelineUnitSnapshot(m_Board.HeroUnit);
            
            var enemySnapshots = new TimelineUnitSnapshot[m_Board.EnemyUnits.Length];
            for (int i = 0; i < enemySnapshots.Length; i++)
            {
                enemySnapshots[i] = new TimelineUnitSnapshot(m_Board.EnemyUnits[i]);
            }
            
            var previousSnapshot = new TimelineSnapshot(heroSnapshot, enemySnapshots, 1);

            for (int i = 0; i < Globals.TIMELINE_SIZE; i++)
            {
                Snapshots[i] = previousSnapshot;
                previousSnapshot = new TimelineSnapshot(previousSnapshot);
                previousSnapshot.TimeframeAssigned = i + 1;
            }
            
            Dictionary<int, List<SentinelAction>> sentinelsToExecute = new Dictionary<int, List<SentinelAction>>();

            for(int i = 0; i < Globals.TIMELINE_SIZE +1; i++)
                sentinelsToExecute.Add(i, new List<SentinelAction>());
            
            foreach (var sentinelAction in m_SentinelActions)
            {
                if(sentinelAction.Source.CurrentEffect.IsPreviousExecution())
                    sentinelsToExecute[0].Add(sentinelAction);
                else
                {
                    sentinelsToExecute[sentinelAction.Timeframe - 1].Add(sentinelAction);
                }
            }

            foreach (var immediateSentinel in sentinelsToExecute[0])
            {
                immediateSentinel.Source.CurrentEffect.ExecuteOnSnapshot(ref Snapshots[0], this, immediateSentinel.Timeframe);
            }

            foreach (var timeframeSentinel in sentinelsToExecute[1])
            {
                timeframeSentinel.Source.CurrentEffect.ExecuteOnSnapshot(ref Snapshots[0], this, 1);
            }
            ExecuteTimeframeOnSnapshot(1, ref Snapshots[0]);
            
            for (int i = 1; i < Globals.TIMELINE_SIZE; i++)
            {
                Snapshots[i].UpdateFromOther(Snapshots[i-1]);
                foreach (var timeframeSentinel in sentinelsToExecute[i])
                {
                    timeframeSentinel.Source.CurrentEffect.ExecuteOnSnapshot(ref Snapshots[i], this, i+1);
                }
                ExecuteTimeframeOnSnapshot(i+1, ref Snapshots[i]);
            }
            
            previousSnapshot = new TimelineSnapshot(Snapshots[Globals.TIMELINE_SIZE-1]);
            ExecuteEndTimelineEffectsOnSnapshot(ref previousSnapshot);
            Snapshots[Globals.TIMELINE_SIZE] = previousSnapshot;
        }

        private void ExecuteTimeframeOnSnapshot(int timeframe, ref TimelineSnapshot snapshot)
        {
            for (var priority = CardInfo.Priority.HeroPriority; priority != CardInfo.Priority.DO_NOT_USE; priority++)
            {
                var actionsToExecute = new List<int>();
                for (int i = 0; i < m_Actions.Count; i++)
                {
                    var action = m_Actions[i];
                        if(action.Timeframe == timeframe && action.Card.CardPriority == priority)
                            actionsToExecute.Add(i);
                }

                foreach (var actionIndex in actionsToExecute)
                {
                    if(m_Actions[actionIndex].Source.Team == Team.Player && snapshot.HeroSnapshot.Life > 0)
                        m_Actions[actionIndex].Card.PlayOnSnapshot(ref snapshot, m_Actions[actionIndex], this, m_Board);
                    else
                    {
                        foreach (var enemySnapshot in snapshot.EnemiesSnapshots)
                        {
                            if(enemySnapshot.SourceUnit == m_Actions[actionIndex].Source && enemySnapshot.Life > 0)
                                m_Actions[actionIndex].Card.PlayOnSnapshot(ref snapshot, m_Actions[actionIndex], this, m_Board);
                        }
                    }
                }
            }
        }

        private void ExecuteEndTimelineEffectsOnSnapshot(ref TimelineSnapshot snapshot)
        {
            snapshot.HeroSnapshot.Life -= snapshot.HeroSnapshot.Miasma;
            for (int i = 0; i < snapshot.EnemiesSnapshots.Length; i++)
                snapshot.EnemiesSnapshots[i].Life -= snapshot.EnemiesSnapshots[i].Miasma;

        }
    }
}