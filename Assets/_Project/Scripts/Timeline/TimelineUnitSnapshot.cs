﻿using System;
using Kairos.Units;
using TorkFramework.Events;
using UnityEngine;

namespace Kairos.Timeline
{
    public partial class TimelineController
    {
        [Serializable]
        public struct TimelineUnitSnapshot
        {
            public readonly UnitController SourceUnit;
            public int MaxLife;
            public int Life;
            public int StrengthModifier;
            public int ArmorModifier;
            public int SpeedModifier;
            public int HealingModifier;
            public int Miasma;
            public int TurnTakedDamage;
            public int SentinelDamageBoost;

            public TimelineUnitSnapshot(UnitController unit)
            {
                MaxLife = unit.ReferencedUnit.TargetUnit.MaxLifePoints;
                Life = unit.ReferencedUnit.ActualLifePoints;
                Miasma = unit.Poison;
                ArmorModifier = unit.ArmorModifier;
                SpeedModifier = unit.SpeedModifer;
                HealingModifier = unit.HealingModifier;
                StrengthModifier = unit.StrengthModifier;
                TurnTakedDamage = unit.TurnTakedDamage;
                SourceUnit = unit;
                SentinelDamageBoost = 0;
            }

            public TimelineUnitSnapshot(TimelineUnitSnapshot other)
            {
                MaxLife = other.MaxLife;
                Life = other.Life;
                StrengthModifier = other.StrengthModifier;
                ArmorModifier = other.ArmorModifier;
                SpeedModifier = other.SpeedModifier;
                HealingModifier = other.HealingModifier;
                Miasma = other.Miasma;
                TurnTakedDamage = other.TurnTakedDamage;
                SourceUnit = other.SourceUnit;
                SentinelDamageBoost = 0;
            }

            public void UpdateFromOther(TimelineUnitSnapshot otherSnapshotHeroSnapshot)
            {
                MaxLife = otherSnapshotHeroSnapshot.MaxLife;
                Life = otherSnapshotHeroSnapshot.Life;
                StrengthModifier = otherSnapshotHeroSnapshot.StrengthModifier;
                ArmorModifier = otherSnapshotHeroSnapshot.ArmorModifier;
                SpeedModifier = otherSnapshotHeroSnapshot.SpeedModifier;
                HealingModifier = otherSnapshotHeroSnapshot.HealingModifier;
                Miasma = otherSnapshotHeroSnapshot.Miasma;
                TurnTakedDamage = otherSnapshotHeroSnapshot.TurnTakedDamage;
            }

            public void DealDamage(TimelineUnitSnapshot source, int amount)
            {
                if (Life <= 0)
                    return;

                var effectiveDamage = amount + source.StrengthModifier + source.SentinelDamageBoost - ArmorModifier;
                effectiveDamage = effectiveDamage < 0 ? 0 : effectiveDamage;
                Life -= effectiveDamage;
                TurnTakedDamage += effectiveDamage;
            }

            public void DealDamageNoSource(int amount)
            {
                if (Life <= 0)
                    return;

                var effectiveDamage = amount - ArmorModifier;
                effectiveDamage = effectiveDamage < 0 ? 0 : effectiveDamage;
                Life -= effectiveDamage;
                TurnTakedDamage += effectiveDamage;
            }

            public void Heal(int amount)
            {
                if (Life <= 0)
                    return;

                var effectiveHeal = amount + HealingModifier;
                effectiveHeal = effectiveHeal < 0 ? 0 : effectiveHeal;
                Life = Mathf.Clamp(Life + effectiveHeal, 0, MaxLife);
            }

            public void AddPoison(int amount)
            {
                if (Life <= 0)
                    return;

                Miasma += amount;
            }

            public void AddStrengthModifier(int amount, int turns)
            {
                if (Life <= 0)
                    return;

                StrengthModifier += amount;
            }

            public void AddArmorModifier(int amount, int turns)
            {
                if (Life <= 0)
                    return;

                ArmorModifier += amount;
            }

            public void AddSpeedModifier(int amount, int turns)
            {
                if (Life <= 0)
                    return;

                SpeedModifier += amount;
            }

            public void AddHealingModifier(int amount, int turns)
            {
                if (Life <= 0)
                    return;

                HealingModifier += amount;
            }
        }
    }
}