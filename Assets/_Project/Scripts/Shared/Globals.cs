﻿using UnityEngine;

namespace Kairos
{
    public static class Globals
    {
        public static string _CARDS_FOLDER => Application.streamingAssetsPath + @"/Cards/";
        public static string _UNITS_FOLDER => Application.streamingAssetsPath + @"/Units/";
        public static string _HEROES_FOLDER => Application.streamingAssetsPath + @"/Heroes/";
        public static string _SENTINELS_FOLDER => Application.streamingAssetsPath + @"/Sentinels/";
        public static string _SENTINELS_EFFECTS_FOLDER => Application.streamingAssetsPath + @"/EffectsSentinel/";
        public static string _ENEMIES_FOLDER => Application.streamingAssetsPath + @"/Enemies/";
        public static string _ENEMY_POOLS_FOLDER => Application.streamingAssetsPath + @"/EnemyPools/";
        public static string _HERO_PASSIVES_FOLDER => Application.streamingAssetsPath + @"/HeroPassives/";
        public static string _KEYWORDS_PATH => Application.streamingAssetsPath + @"/Databases/Keywords.db";
        public static string _STATUS_DATABASE_FILE => Application.streamingAssetsPath + @"/Databases/Statuses.db";

        public static string _DATA_SAVE_PATH = Application.persistentDataPath + @"/Data/save.gd";
        
        public static string _GAME_VERSION = Application.persistentDataPath + "0-1.ver";
        
        public const string GAME_SCENE = "SCN_Game";
        
        public const int TIMELINE_SIZE = 6;
        public const int MAX_SENTINELS = 6;
        public const int START_TURN_HAND_SIZE = 6;
        public const int ENCOUNTER_GROUP_COUNT = 3;
    }
}