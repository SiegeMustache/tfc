﻿using System.Collections.Generic;
using TorkFramework;
using UnityEngine;

namespace Kairos.Indicators
{
    public class QuadraticIndicator : EntityBehaviour<GameEntity>
    {
        private const float PREVIEW_RESOLUTION = 0.005f;

        [SerializeField] private GameEntity m_PointerPrefab;
        [SerializeField] private GameEntity m_RepetitionPrefab;

        [SerializeField] private float m_MeshScales = 1;
        [SerializeField] private float m_ChildOffset = .5f;

        [SerializeField] private Vector3 m_ControlPosition;
        [SerializeField] private Vector3 m_EndPosition;

        public Vector3 ControlPosition
        {
            get => m_ControlPosition;
            set { m_ControlPosition = value; }
        }

        public Vector3 EndPosition
        {
            get => m_EndPosition;
            set { m_EndPosition = value; }
        }

        public void DrawIndicator()
        {
            Entity.GameManager.KillChildren(Entity);

            List<Vector3> positions = new List<Vector3>();
            positions.Add(transform.position);
            var actualDistance = 0f;

            List<Vector3> directions = new List<Vector3>();
            directions.Add((EvaluateQuadratic(transform.position, EndPosition, ControlPosition, PREVIEW_RESOLUTION) - transform.position).normalized);

            for (float i = PREVIEW_RESOLUTION; i < 1; i += PREVIEW_RESOLUTION)
            {
                var newPoint = EvaluateQuadratic(transform.position, EndPosition, ControlPosition, i);
                actualDistance += Vector3.Distance(positions[positions.Count - 1], newPoint);
                if (actualDistance >= m_ChildOffset)
                {
                    actualDistance = 0;
                    positions.Add(newPoint);
                    directions.Add((EvaluateQuadratic(transform.position, EndPosition, ControlPosition, i - PREVIEW_RESOLUTION) -
                                    EvaluateQuadratic(transform.position, EndPosition, ControlPosition, i + PREVIEW_RESOLUTION)).normalized);
                }
            }

            for (int i = 0; i < positions.Count - 1; i++)
            {
                var newObject = Entity.GameManager.Spawn(m_RepetitionPrefab);
                newObject.transform.SetParent(transform, false);
                newObject.transform.position = positions[i];
                newObject.transform.forward = directions[i];
                newObject.transform.localScale = m_MeshScales * Vector3.one;
            }

            var newPointer = Entity.GameManager.Spawn(m_PointerPrefab);
            newPointer.transform.SetParent(transform, false);
            newPointer.transform.position = positions[positions.Count - 1];
            newPointer.transform.forward = (positions[positions.Count - 1] - positions[positions.Count - 2]).normalized;
            newPointer.transform.localScale = m_MeshScales * Vector3.one;
        }

        private void OnEnable()
        {
            Entity.GameManager.KillChildren(Entity);
        }

        private void OnDisable()
        {
            Entity.GameManager.KillChildren(Entity);
        }

        private void OnDrawGizmosSelected()
        {
            for (float i = PREVIEW_RESOLUTION; i < 1; i += PREVIEW_RESOLUTION)
            {
                Gizmos.DrawLine(EvaluateQuadratic(transform.position, EndPosition, ControlPosition, i - PREVIEW_RESOLUTION),
                    EvaluateQuadratic(transform.position, EndPosition, ControlPosition, i));
            }
        }

        private static Vector3 EvaluateQuadratic(Vector3 start, Vector3 end, Vector3 control, float t)
        {
            var p0 = Vector3.Lerp(start, control, t);
            var p1 = Vector3.Lerp(control, end, t);
            return Vector3.Lerp(p0, p1, t);
        }
    }
}