﻿using System;
using Kairos.Enemies.EnemyPools;
using Kairos.Heroes;
using Kairos.Managers;
using Kairos.Player;
using Kairos.Progression.Rewards;
using TorkFramework;
using TorkFramework.ResourceManagement;
using TorkFramework.Serialization;

using Random = UnityEngine.Random;

namespace Kairos.Progression.Tree
{
    public class EnemyFightTreeEffect : TreeEffect
    {
        [XmlSerialized] public EnemyPoolReference TargetEnemyPool;
        [XmlSerialized] public ResourceReference RewardPool;
        [XmlSerialized(false)] public Reward SelectedReward = null;

        public EnemyFightTreeEffect() : base()
        {
            TargetEnemyPool = new EnemyPoolReference();
            RewardPool = new ResourceReference();
            SelectedReward = null;
        }
        
        public EnemyFightTreeEffect(EnemyFightTreeEffect source) :base(source)
        {
            TargetEnemyPool = new EnemyPoolReference {PoolId = source.TargetEnemyPool.PoolId};
            RewardPool = new ResourceReference {Path = source.RewardPool.Path};
            SelectedReward = null;
        }
        
        public override void Generate(GameManager gameManager)
        {
            var rewardPool = RewardPool.Resource as RewardPool;
            //SelectedReward = rewardPool.PossibleRewards[Random.Range(0, rewardPool.PossibleRewards.Length)].Reward;
            SelectedReward = rewardPool.GetRandomReward(HeroDatabase.LoadedHeroes[gameManager.GetManager<PlayerManager>().Data.HeroId]);
        }

        public override void Execute(GameManager gameManager, Action<bool> effectExecutedCallback)
        {
            gameManager.GetManager<RoundManager>().StartNewRound(TargetEnemyPool, result=>
            {
                if (result)
                {
                    SelectedReward.CollectReward(gameManager, gameManager.GetManager<PlayerManager>(), () =>
                    {
                        effectExecutedCallback?.Invoke(true);
                    });
                }
                else
                {
                    effectExecutedCallback?.Invoke(false);
                }
            });
        }
    }
}