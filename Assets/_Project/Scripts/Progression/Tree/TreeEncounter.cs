﻿using System;
using Kairos.Progression.Encounters;
using Kairos.UI.Popups;
using TorkFramework;
using TorkFramework.Runtime.UI;
using TorkFramework.Serialization;

namespace Kairos.Progression.Tree
{
    public class TreeEncounter : Encounter
    {
        [XmlSerialized] public string Question;
        [XmlSerialized] public TreeOption[] Options;
        
        public TreeEncounter() : base()
        {
            Question = "";
            Options = new TreeOption[0];
        }

        public TreeEncounter(TreeEncounter source) : base(source)
        {
            Question = source.Question;
            Options = new TreeOption[source.Options.Length];

            for (int i = 0; i < Options.Length; i++)
            {
                Options[i] = new TreeOption(source.Options[i]);
            }
        }
        
        public override void GenerateEncounter(GameManager gameManager)
        {
            foreach (var option in Options)
            {
                option.Generate(gameManager);
            }
        }

        public override void PlayEncounter(GameManager gameManager, Action<bool> encounterExecutedCallback)
        {
            var optionTexts = new string[Options.Length];
            for (int i = 0; i < Options.Length; i++)
            {
                optionTexts[i] = Options[i].Text;
            }
            
            (gameManager.GetManager<UIManager>().QuerableEntities[typeof(MultipleQuestionPopupController)] as MultipleQuestionPopupController)
                .ShowMultipleQuestionBox(Question, index => OnAnswerSelectedCallback(gameManager, index, encounterExecutedCallback), optionTexts);
        }

        private void OnAnswerSelectedCallback(GameManager manager, int index, Action<bool> encounterExecutedCallback)
        {
            Options[index].ApplyOption(manager, encounterExecutedCallback);
        }
    }
}