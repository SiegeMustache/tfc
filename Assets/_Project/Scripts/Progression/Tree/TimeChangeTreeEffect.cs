﻿using System;
using TorkFramework;
using TorkFramework.Serialization;

namespace Kairos.Progression.Tree
{
    public class TimeChangeTreeEffect : TreeEffect
    {
        [XmlSerialized] public int TimeChange;

        public TimeChangeTreeEffect() : base()
        {
        }

        public TimeChangeTreeEffect(TimeChangeTreeEffect source) : base(source)
        {
            TimeChange = source.TimeChange;
        }

        public override void Generate(GameManager gameManager)
        {
        }

        public override void Execute(GameManager gameManager, Action<bool> effectExecutedCallback)
        {
            gameManager.GetManager<ProgressionManager>().ActualTime += TimeChange;
            effectExecutedCallback?.Invoke(true);
        }
    }
}