﻿using System;
using TorkFramework;
using TorkFramework.Serialization;

namespace Kairos.Progression.Tree
{
    public class TreeOption
    {
        [XmlSerialized] public string Text;
        [XmlSerialized] public TreeEffect[] Effects;

        public TreeOption()
        {
            Text = "";
            Effects = new TreeEffect[0];
        }

        public TreeOption(TreeOption source)
        {
            if (source != null)
            {
                Text = source.Text;
                Effects = new TreeEffect[source.Effects.Length];

                for (int i = 0; i < Effects.Length; i++)
                {
                    if (source.Effects[i] != null)
                        Effects[i] = Activator.CreateInstance(source.Effects[i].GetType(), source.Effects[i]) as TreeEffect;
                }
            }
            else
            {
                Text = "";
                Effects = new TreeEffect[0];
            }
        }

        public void Generate(GameManager gameManager)
        {
            for (int i = 0; i < Effects.Length; i++)
            {
                Effects[i].Generate(gameManager);
            }
        }

        public void ApplyOption(GameManager gameManager, Action<bool> optionExecutedCallback)
        {
            if (Effects != null && Effects.Length > 0)
                Effects[0].Execute(gameManager, result => OnEffectExecuted(result, 0, gameManager, optionExecutedCallback));
            else
                optionExecutedCallback?.Invoke(true);
        }

        private void OnEffectExecuted(bool result, int actualIndex, GameManager gameManager, Action<bool> optionExecutedCallback)
        {
            if (!result)
            {
                optionExecutedCallback?.Invoke(false);
                return;
            }

            actualIndex++;
            if (actualIndex >= Effects.Length)
            {
                optionExecutedCallback?.Invoke(true);
                return;
            }

            Effects[actualIndex].Execute(gameManager, res => OnEffectExecuted(res, actualIndex, gameManager, optionExecutedCallback));
        }
    }
}