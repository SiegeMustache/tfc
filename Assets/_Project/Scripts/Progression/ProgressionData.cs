﻿using System;
using System.Collections.Generic;
using Kairos.Progression.Encounters;
using Kairos.Progression.Events;
using TorkFramework.Serialization;
using UnityEngine;

namespace Kairos.Progression
{
    [Serializable]
    public class ProgressionData
    {
        [XmlSerialized] public int ActualTier;
        [XmlSerialized] public int ActualCost;
        [XmlSerialized] public int ActualEncounter;
        [XmlSerialized] public List<EncounterGroup> GeneratedEncounters;
        [XmlSerialized] public TierChangeEvent[] GeneratedTierChangedEvents;
        
        public ProgressionData()
        {
            ActualTier = 0;
            ActualCost = 0;
            ActualEncounter = 0;
            GeneratedEncounters = new List<EncounterGroup>();
            GeneratedTierChangedEvents = new TierChangeEvent[0];
        }
    }
}