﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using Kairos.Heroes;
using Kairos.Player;
using Kairos.Progression.Encounters;
using Kairos.Progression.Events;
using Kairos.Progression.Rewards;
using TorkFramework;
using TorkFramework.Events;
using TorkFramework.Save;
using TorkFramework.Serialization;
using TorkFramework.Utility;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Kairos.Progression
{
    public class ProgressionManager : Manager
    {
        [SerializeField] private EncounterGroupObject[] m_EnemyGroupPool;
        [SerializeField] private EncounterGroupObject[] m_MixedGroupPool;
        [SerializeField] private ScriptableObject[] m_TierEvents;

        [SerializeField] private int m_MaxTier;
        [SerializeField] private int m_MaxTierEncounters;
        [SerializeField] private int m_EnemyEncountersPerTier;
        [SerializeField] private RewardPool m_EndGameRewardPool;

        private ProgressionData m_ProgressionData;
        private bool m_Running = false;

        public bool CanContinue => m_ProgressionData != null;

        public int MaxTime
        {
            get
            {
                foreach (var changeTierEvent in m_ProgressionData.GeneratedTierChangedEvents)
                {
                    if (changeTierEvent.RequiredTier == m_ProgressionData.ActualTier)
                    {
                        return changeTierEvent.RequiredCost;
                    }
                }

                return 0;
            }
        }

        public int ActualTime
        {
            get => m_ProgressionData.ActualCost;
            set => m_ProgressionData.ActualCost = value;
        }

        public int ActualGroup => m_ProgressionData.ActualEncounter;
        public int ActualTier => m_ProgressionData?.ActualTier ?? 0;

        public void GenerateMap(Action mapGeneratedCallback)
        {
            StartCoroutine(GenerateMapCoroutine(() =>
            {
                StopAllCoroutines();
                mapGeneratedCallback?.Invoke();
            }));
        }

        private IEnumerator GenerateMapCoroutine(Action mapGeneratedCallback)
        {
            var actualTier = 1;

            m_ProgressionData = new ProgressionData();

            while (actualTier <= m_MaxTier)
            {
                try
                {
                    var tierPool = new List<EncounterGroup>();
                    while (tierPool.Count < m_EnemyEncountersPerTier)
                    {
                        try
                        {
                            var testGroup = m_EnemyGroupPool[Random.Range(0, m_EnemyGroupPool.Length)].Group;
                            if (testGroup.Tier == actualTier)
                                tierPool.Add(new EncounterGroup(testGroup));
                        }
                        catch (Exception ex)
                        {
                            Debug.LogError(ex.Message);
                        }
                    }


                    tierPool = tierPool.Shuffle();
                    m_ProgressionData.GeneratedEncounters.Add(tierPool[0]);
                    tierPool.RemoveAt(0);
                    bool placedEnemy = true;

                    Stack<int> nonEnemyPositions = new Stack<int>();

                    for (int i = 1; i < m_MaxTierEncounters; i++)
                    {
                        try
                        {
                            if (placedEnemy)
                            {
                                if (Random.Range(0, 2) > 0)
                                {
                                    bool found = false;
                                    while (!found)
                                    {
                                        try
                                        {
                                            var testGroup = m_MixedGroupPool[Random.Range(0, m_MixedGroupPool.Length)].Group;
                                            if (testGroup.Tier == actualTier)
                                            {
                                                m_ProgressionData.GeneratedEncounters.Add(new EncounterGroup(testGroup));
                                                placedEnemy = false;
                                                found = true;
                                                nonEnemyPositions.Push((actualTier - 1) * m_MaxTierEncounters + i);
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            Debug.LogError(ex.Message);
                                        }
                                    }
                                }
                                else
                                {
                                    try
                                    {
                                        if (tierPool.Count > 0)
                                        {
                                            m_ProgressionData.GeneratedEncounters.Add(tierPool[0]);
                                            tierPool.RemoveAt(0);
                                            placedEnemy = true;
                                        }
                                        else
                                        {
                                            bool found = false;
                                            while (!found)
                                            {
                                                try
                                                {
                                                    var testGroup = m_EnemyGroupPool[Random.Range(0, m_EnemyGroupPool.Length)].Group;
                                                    if (testGroup.Tier == actualTier)
                                                    {
                                                        m_ProgressionData.GeneratedEncounters.Add(new EncounterGroup(testGroup));
                                                        placedEnemy = false;
                                                        found = true;
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    Debug.LogError(ex.Message);
                                                }
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Debug.LogError(ex.Message);
                                    }
                                }
                            }
                            else
                            {
                                try
                                {
                                    if (tierPool.Count > 0)
                                    {
                                        m_ProgressionData.GeneratedEncounters.Add(tierPool[0]);
                                        tierPool.RemoveAt(0);
                                        placedEnemy = true;
                                    }
                                    else
                                    {
                                        bool found = false;
                                        while (!found)
                                        {
                                            try
                                            {
                                                var testGroup = m_EnemyGroupPool[Random.Range(0, m_EnemyGroupPool.Length)].Group;
                                                if (testGroup.Tier == actualTier)
                                                {
                                                    m_ProgressionData.GeneratedEncounters.Add(new EncounterGroup(testGroup));
                                                    placedEnemy = false;
                                                    found = true;
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                Debug.LogError(ex.Message);
                                            }
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Debug.LogError(ex.Message);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Debug.LogError(ex.Message);
                        }
                    }

                    while (tierPool.Count > 0)
                    {
                        try
                        {
                            var position = nonEnemyPositions.Pop();
                            m_ProgressionData.GeneratedEncounters.RemoveAt(position);
                            m_ProgressionData.GeneratedEncounters.Insert(position, tierPool[0]);
                            tierPool.RemoveAt(0);
                        }
                        catch (Exception ex)
                        {
                            Debug.LogError(ex.Message);
                        }
                    }

                    actualTier++;
                }
                catch (Exception ex)
                {
                    Debug.LogError(ex.Message);
                }


                yield return new WaitForEndOfFrame();
            }

            try
            {
                m_ProgressionData.GeneratedTierChangedEvents = new TierChangeEvent[m_TierEvents.Length];
                for (int i = 0; i < m_TierEvents.Length; i++)
                {
                    m_ProgressionData.GeneratedTierChangedEvents[i] = ((IEventObject) m_TierEvents[i]).GetEvent();
                }

                foreach (var generatedEncounter in m_ProgressionData.GeneratedEncounters)
                {
                    generatedEncounter.GenerateEncounterGroup(GameManager);
                }

                foreach (var generatedTiers in m_ProgressionData.GeneratedTierChangedEvents)
                {
                    generatedTiers.GenerateEncounter(GameManager);
                }

                m_ProgressionData.ActualTier = 1;
                m_ProgressionData.ActualCost = 0;
                m_ProgressionData.ActualEncounter = 0;

                GameManager.GetManager<EventManager>().TriggerEvent(EventId.MapGenerated, this, () => { mapGeneratedCallback?.Invoke(); }, m_ProgressionData);
            }
            catch (Exception ex)
            {
                Debug.LogError(ex.Message);
            }

            yield return new WaitForEndOfFrame();
        }

        public void PlayGroup(int groupIndex, int counterIndex)
        {
            if (!m_Running && groupIndex == m_ProgressionData.ActualEncounter)
            {
                m_Running = true;
                m_ProgressionData.ActualCost += m_ProgressionData.GeneratedEncounters[groupIndex].Encounters[counterIndex].Cost;
                GameManager.GetManager<EventManager>().TriggerEvent(EventId.ProgressionCostChanged, this, () =>
                {
                    m_ProgressionData.GeneratedEncounters[groupIndex].Encounters[counterIndex].PlayEncounter(GameManager, result =>
                    {
                        if (!result)
                        {
                            m_Running = false;
                            GameManager.GetManager<SaveManager>().LoadedData.Remove("ProgressionData");
                            m_ProgressionData = null;
                            GameManager.ChangeState(GameStates.Loading, () => { GameManager.GetManager<SaveManager>().SaveGameData(() => { GameManager.ChangeState(GameStates.Lose); }); });
                        }
                        else
                        {
                            if (m_ProgressionData.GeneratedEncounters[groupIndex].IsLastGroup)
                            {
                                m_Running = false;
                                GameManager.GetManager<SaveManager>().LoadedData.Remove("ProgressionData");
                                m_ProgressionData = null;
                                GameManager.ChangeState(GameStates.Win, () =>
                                {
                                    //show reward
                                    var reward = m_EndGameRewardPool.GetRandomReward(HeroDatabase.LoadedHeroes[GameManager.GetManager<PlayerManager>().Data.HeroId]);
                                    reward.CollectReward(GameManager, GameManager.GetManager<PlayerManager>(), () => { GameManager.GetManager<SaveManager>().SaveGameData(() => { }); });
                                });
                            }
                            else
                            {
                                GameManager.ChangeState(GameStates.LevelSelection, () =>
                                {
                                    bool tierChanged = false;
                                    foreach (var tierChangedEvent in m_ProgressionData.GeneratedTierChangedEvents)
                                    {
                                        if (tierChangedEvent.RequiredTier == m_ProgressionData.ActualTier && tierChangedEvent.RequiredCost <= m_ProgressionData.ActualCost)
                                        {
                                            tierChanged = true;
                                            var newGroup = new EncounterGroup {Tier = tierChangedEvent.RequiredTier, Encounters = new Encounter[] {tierChangedEvent}};
                                            if (tierChangedEvent.IsLastEvent)
                                                newGroup.IsLastGroup = true;

                                            m_ProgressionData.GeneratedEncounters.Insert(m_ProgressionData.ActualEncounter + 1, newGroup);
                                            GameManager.GetManager<EventManager>().TriggerEvent(EventId.EncounterGroupAdded, this, () =>
                                            {
                                                m_ProgressionData.ActualTier++;
                                                GameManager.GetManager<EventManager>().TriggerEvent(EventId.TierChanged, this, () =>
                                                {
                                                    Stack<int> positionsToRemove = new Stack<int>();

                                                    for (int i = m_ProgressionData.ActualEncounter + 2; i < m_ProgressionData.GeneratedEncounters.Count; i++)
                                                    {
                                                        if (m_ProgressionData.GeneratedEncounters[i].Tier < m_ProgressionData.ActualTier)
                                                            positionsToRemove.Push(i);
                                                    }

                                                    while (positionsToRemove.Count > 0)
                                                    {
                                                        var targetPosition = positionsToRemove.Pop();
                                                        GameManager.GetManager<EventManager>().TriggerEvent(EventId.EncounterGroupRemove, this, () => { }, targetPosition,
                                                            m_ProgressionData.GeneratedEncounters[targetPosition]);
                                                        m_ProgressionData.GeneratedEncounters.RemoveAt(targetPosition);
                                                    }

                                                    m_ProgressionData.ActualEncounter++;
                                                    GameManager.GetManager<EventManager>().TriggerEvent(EventId.ProgressionPositionChanged, this, () => { }, m_ProgressionData.ActualEncounter);
                                                    m_Running = false;
                                                    GameManager.GetManager<SaveManager>().SaveGameData(() => { });
                                                }, m_ProgressionData.ActualTier);
                                            }, m_ProgressionData.ActualEncounter + 1, newGroup);
                                            break;
                                        }
                                    }

                                    if (!tierChanged)
                                    {
                                        m_ProgressionData.ActualEncounter++;
                                        GameManager.GetManager<EventManager>().TriggerEvent(EventId.ProgressionPositionChanged, this, () => { }, m_ProgressionData.ActualEncounter);
                                        m_Running = false;
                                        GameManager.GetManager<SaveManager>().SaveGameData(() => { });
                                    }
                                });
                            }
                        }
                    });
                }, m_ProgressionData.ActualCost);
            }
        }

        private void Start()
        {
            GameManager.GetManager<EventManager>().AddListener(EventId.BeforeSave, OnBeforeGameSave);
            GameManager.GetManager<EventManager>().AddListener(EventId.AfterLoad, OnAfterGameLoad);
            LoadData();
        }

        private void OnDestroy()
        {
            GameManager.GetManager<EventManager>().RemoveListener(EventId.BeforeSave, OnBeforeGameSave);
            GameManager.GetManager<EventManager>().RemoveListener(EventId.AfterLoad, OnAfterGameLoad);
        }

        private void OnBeforeGameSave(GenericEventArgs args, Action listenerExecutedCallback)
        {
            SaveData();
        }

        private void OnAfterGameLoad(GenericEventArgs args, Action listenerExecutedCallback)
        {
            LoadData();
        }

        private void SaveData()
        {
            if (m_ProgressionData != null)
            {
                var serializer = new Serializer(new XmlWriterSettings());
                if (!GameManager.GetManager<SaveManager>().LoadedData.ContainsKey("ProgressionData"))
                {
                    GameManager.GetManager<SaveManager>().LoadedData.Add("ProgressionData", serializer.SerializeToString(m_ProgressionData));
                }
                else
                {
                    GameManager.GetManager<SaveManager>().LoadedData["ProgressionData"] = serializer.SerializeToString(m_ProgressionData);
                }
            }
        }

        private void LoadData()
        {
            var deserializer = new Deserializer();
            if (GameManager.GetManager<SaveManager>().LoadedData.ContainsKey("ProgressionData"))
            {
                m_ProgressionData = deserializer.DeserializeFromString<ProgressionData>(GameManager.GetManager<SaveManager>().LoadedData["ProgressionData"] as string);
            }
        }

        public void Continue(Action mapGenerated)
        {
            GameManager.GetManager<EventManager>().TriggerEvent(EventId.MapGenerated, this, () =>
            {
                mapGenerated?.Invoke();
                GameManager.GetManager<EventManager>().TriggerEvent(EventId.ProgressionPositionChanged, this, () => { OnPositionUpdated(0); }, 0);
            }, m_ProgressionData);
        }

        private void OnPositionUpdated(int actualPosition)
        {
            if (actualPosition < m_ProgressionData.ActualEncounter)
            {
                actualPosition++;
                GameManager.GetManager<EventManager>().TriggerEvent(EventId.ProgressionPositionChanged, this, () => { OnPositionUpdated(actualPosition); }, actualPosition);
            }
        }
    }
}