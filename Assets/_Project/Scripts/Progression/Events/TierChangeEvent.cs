﻿using Kairos.Progression.Encounters;
using TorkFramework.Serialization;

namespace Kairos.Progression.Events
{
    public abstract class TierChangeEvent : Encounter
    {
        [XmlSerialized] public bool IsLastEvent = false;
        [XmlSerialized] public int RequiredTier;
        [XmlSerialized] public int RequiredCost;

        public TierChangeEvent()
        {
            RequiredTier = 0;
            RequiredCost = 0;
            IsLastEvent = false;
        }
    }
}