﻿namespace Kairos.Progression.Events
{
    public interface IEventObject
    {
        TierChangeEvent GetEvent();
    }
}