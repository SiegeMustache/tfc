﻿using System;
using Kairos.Enemies.EnemyPools;
using Kairos.Managers;
using Kairos.Player;
using Kairos.Progression.Rewards;
using TorkFramework;
using TorkFramework.ResourceManagement;
using TorkFramework.Serialization;

namespace Kairos.Progression.Events
{
    public class BossFightEvent : TierChangeEvent
    {
        [XmlSerialized] public EnemyPoolReference FightEnemies;
        [XmlSerialized] public ResourceReference FightReward;

        public override void GenerateEncounter(GameManager gameManager)
        {
        }

        public override void PlayEncounter(GameManager gameManager, Action<bool> encounterExecutedCallback)
        {
            gameManager.GetManager<RoundManager>().StartNewRound(FightEnemies, result =>
            {
                if (result)
                {
                    if (FightReward == null || FightReward.Resource == null)
                    {
                        encounterExecutedCallback?.Invoke(true);
                    }
                    else
                    {
                        ((FightReward.Resource as RewardObject).Reward).CollectReward(gameManager, gameManager.GetManager<PlayerManager>(), () => { encounterExecutedCallback?.Invoke(true); });
                    }
                }
                else
                {
                    encounterExecutedCallback?.Invoke(false);
                }
            });
        }
    }
}