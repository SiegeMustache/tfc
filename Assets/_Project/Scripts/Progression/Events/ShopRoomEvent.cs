﻿using System;
using Kairos.Heroes;
using Kairos.Player;
using UnityEngine;
using Kairos.Progression.Rewards;
using Kairos.UI.Progression;
using TorkFramework;
using TorkFramework.ResourceManagement;
using TorkFramework.Runtime.UI;
using TorkFramework.Serialization;
using Random = UnityEngine.Random;

namespace Kairos.Progression.Events
{
    public class ShopRoomEvent : TierChangeEvent
    {
        [XmlSerialized] public ResourceReference[] RewardPools;
        [XmlSerialized(false)] public int[] GeneratedRewards;

        public ShopRoomEvent()
        {
            RewardPools = new ResourceReference[0];
        }

        public override void GenerateEncounter(GameManager gameManager)
        {
            try
            {
                if (RewardPools != null)
                {
                    GeneratedRewards = new int[RewardPools.Length];
                    for (int i = 0; i < RewardPools.Length; i++)
                    {
                        var rewardPool = RewardPools[i].Resource as RewardPool;
                        int possibleReward = -1;
                        bool found = false;
                        while (!found)
                        {
                            //GeneratedRewards[i] = Random.Range(0, rewardPool.PossibleRewards.Length);
                            possibleReward = Random.Range(0, rewardPool.GetValuableRewards(HeroDatabase.LoadedHeroes[gameManager.GetManager<PlayerManager>().Data.HeroId]).Length);

                            found = true;

                            var selectedObjectId = rewardPool.GetValuableRewards(HeroDatabase.LoadedHeroes[gameManager.GetManager<PlayerManager>().Data.HeroId])[possibleReward].ObjectID;
                            
                            for (int j = i - 1; j >= 0; j--)
                            {
                                var otherObjectId = (RewardPools[j].Resource as RewardPool).GetValuableRewards(HeroDatabase.LoadedHeroes[gameManager.GetManager<PlayerManager>().Data.HeroId])[GeneratedRewards[j]].ObjectID;
                                
                                if (selectedObjectId == otherObjectId)
                                {
                                    found = false;
                                    break;
                                }
                            }
                        }

                        GeneratedRewards[i] = possibleReward;
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.LogError($"Error while generating the shop room. {ex.Message}");
            }
        }

        public override void PlayEncounter(GameManager gameManager, Action<bool> encounterExecutedCallback)
        {
            var rewards = new Reward[RewardPools.Length];
            for (int i = 0; i < RewardPools.Length; i++)
            {
                rewards[i] = (RewardPools[i].Resource as RewardPool).GetValuableRewards(HeroDatabase.LoadedHeroes[gameManager.GetManager<PlayerManager>().Data.HeroId])[GeneratedRewards[i]].Reward;
            }

            (gameManager.GetManager<UIManager>().QuerableEntities[typeof(UIShopPopup)] as UIShopPopup).ShowShopPopup((reward) =>
            {
                if (reward == null)
                {
                    encounterExecutedCallback?.Invoke(true);
                }
                else
                {
                    gameManager.GetManager<ProgressionManager>().ActualTime += reward.ShopCost;
                    reward.CollectRewardFromShop(gameManager, gameManager.GetManager<PlayerManager>(), () => { encounterExecutedCallback?.Invoke(true); });
                }
            }, rewards);
        }
    }
}