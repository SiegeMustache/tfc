﻿using TorkFramework.Editor.Utility;
using UnityEditor;
using UnityEngine;

namespace Kairos.Progression.Events.Editor
{
    [CustomEditor(typeof(BossFightEventObject))]
    public class BossFightEventObjectEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            var targetObject = (BossFightEventObject) target;

            targetObject.Event = FieldUtils.AdaptiveField("", targetObject.Event) as BossFightEvent;
            
            GUI.color = Color.green;
            if (GUILayout.Button("Save"))
            {
                targetObject.Save();
                EditorUtility.SetDirty(targetObject);
                AssetDatabase.SaveAssets();
            }
        }
    }
}