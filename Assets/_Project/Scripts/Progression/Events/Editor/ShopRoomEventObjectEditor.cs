﻿using System;
using TorkFramework.Editor.Utility;
using TorkFramework.ResourceManagement;
using UnityEditor;
using UnityEngine;

namespace Kairos.Progression.Events.Editor
{
    [CustomEditor(typeof(ShopRoomEventObject))]
    public class ShopRoomEventObjectEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            var targetObject = (ShopRoomEventObject) target;

            targetObject.Event = FieldUtils.AdaptiveField("", targetObject.Event) as ShopRoomEvent;

            EditorGUI.indentLevel++;

            EditorGUILayout.LabelField("Reward Pools");
            EditorGUI.indentLevel++;

            var newSize = (int) FieldUtils.AdaptiveField("Size", targetObject.Event.RewardPools.Length);

            if (newSize < 0)
                newSize = 0;
            if (newSize != targetObject.Event.RewardPools.Length)
                Array.Resize(ref targetObject.Event.RewardPools, newSize);

            for (int i = 0; i < targetObject.Event.RewardPools.Length; i++)
            {
                targetObject.Event.RewardPools[i] = FieldUtils.AdaptiveField($"Item {i}", targetObject.Event.RewardPools[i]) as ResourceReference;
            }

            EditorGUI.indentLevel--;

            GUI.color = Color.green;
            if (GUILayout.Button("Save"))
            {
                targetObject.Save();
                EditorUtility.SetDirty(targetObject);
                AssetDatabase.SaveAssets();
            }

            EditorGUI.indentLevel--;
        }
    }
}