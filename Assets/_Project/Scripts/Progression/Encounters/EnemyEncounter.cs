﻿using System;
using Kairos.Enemies.EnemyPools;
using Kairos.Heroes;
using Kairos.Managers;
using Kairos.Player;
using Kairos.Progression.Rewards;
using Kairos.UI.Progression;
using TorkFramework;
using TorkFramework.ResourceManagement;
using TorkFramework.Runtime.UI;
using TorkFramework.Serialization;
using Random = UnityEngine.Random;

namespace Kairos.Progression.Encounters
{
    public class EnemyEncounter : Encounter
    {
        [XmlSerialized] public EnemyPoolReference TargetEnemyPool;
        [XmlSerialized] public ResourceReference RewardPool;
        [XmlSerialized] public int RewardCount;
        
        //[XmlSerialized(false)] public Reward SelectedReward = null;
        [XmlSerialized(false)] public Reward[] SelectedRewards = null;

        public EnemyEncounter() : base()
        {
            TargetEnemyPool = new EnemyPoolReference();
            RewardPool = new ResourceReference();
            RewardCount = 3;
            //SelectedReward = null;
            SelectedRewards = new Reward[0];
        }

        public EnemyEncounter(EnemyEncounter source) :base(source)
        {
            TargetEnemyPool = new EnemyPoolReference {PoolId = source.TargetEnemyPool.PoolId};
            RewardPool = new ResourceReference {Path = source.RewardPool.Path};
            RewardCount = source.RewardCount;
            //SelectedReward = null;
            
            SelectedRewards = new Reward[0];
        }

        public override void GenerateEncounter(GameManager gameManager)
        {
            var rewardPool = RewardPool.Resource as RewardPool;
            //SelectedReward = rewardPool.GetRandomReward(HeroDatabase.LoadedHeroes[gameManager.GetManager<PlayerManager>().Data.HeroId]);
            SelectedRewards = new Reward[RewardCount];
            for (int i = 0; i < RewardCount; i++)
            {
                SelectedRewards[i] = rewardPool.GetRandomReward(HeroDatabase.LoadedHeroes[gameManager.GetManager<PlayerManager>().Data.HeroId]);
            }
        }

        public override void PlayEncounter(GameManager gameManager, Action<bool> encounterExecutedCallback)
        {
            gameManager.GetManager<RoundManager>().StartNewRound(TargetEnemyPool, result=>
            {
                if (result)
                {
                    // SelectedReward.CollectReward(gameManager, gameManager.GetManager<PlayerManager>(), () =>
                    // {
                    //     encounterExecutedCallback?.Invoke(true);
                    // });

                    gameManager.ChangeState(GameStates.LevelSelection, () =>
                    {
                        gameManager.GetManager<UIManager>().GetQuerableEntity<UIShopPopup>().ShowShopPopup(resultReward =>
                        {
                            if (resultReward == null)
                            {
                                encounterExecutedCallback?.Invoke(true);
                            }
                            else
                            {
                                resultReward.CollectRewardFromShop(gameManager, gameManager.GetManager<PlayerManager>(),
                                    () => { encounterExecutedCallback?.Invoke(true); });
                            }
                        }, SelectedRewards);
                    });
                }
                else
                {
                    encounterExecutedCallback?.Invoke(false);
                }
            });
        }
    }
}