﻿using System;
using TorkFramework.Editor.Utility;
using TorkFramework.Utility;
using UnityEditor;
using UnityEngine;

namespace Kairos.Progression.Encounters.Editor
{
    [CustomEditor(typeof(EncounterGroupObject))]
    public class EncounterGroupObjectEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            var targetObject = (EncounterGroupObject) target;

            targetObject.Group.Tier = (int) FieldUtils.AdaptiveField("Tier", targetObject.Group.Tier);
            
            var newSize = (int) FieldUtils.AdaptiveField("Size", targetObject.Group.Encounters.Length);
            newSize = Mathf.Clamp(newSize, 0, Globals.ENCOUNTER_GROUP_COUNT);

            if (newSize != targetObject.Group.Encounters.Length)
            {
                Array.Resize(ref targetObject.Group.Encounters, newSize);
            }

            for (int i = 0; i < targetObject.Group.Encounters.Length; i++)
            {
                EditorGUILayout.BeginHorizontal();
                if (targetObject.Group.Encounters[i] == null)
                    EditorGUILayout.LabelField("None");
                else
                    EditorGUILayout.LabelField(targetObject.Group.Encounters[i].GetType().Name);
                if (GUILayout.Button("Change"))
                {
                    var targetIndex = i;
                    GenericMenu menu = new GenericMenu();
                    foreach (var encounterType in TypeUtils.GetDerivedTypes(typeof(Encounter)))
                    {
                        menu.AddItem(new GUIContent(encounterType.Name), false, () => { SetEncounter(targetIndex, encounterType);});
                    }
                    menu.ShowAsContext();
                }
                
                
                EditorGUILayout.EndHorizontal();

                if (targetObject.Group.Encounters[i] != null)
                {
                    targetObject.Group.Encounters[i] = FieldUtils.AdaptiveField("", (dynamic)  targetObject.Group.Encounters[i]) as Encounter;
                }
            }

            GUI.color = Color.green;
            if (GUILayout.Button("Save"))
            {
                targetObject.Save();
                EditorUtility.SetDirty(targetObject);
                AssetDatabase.SaveAssets();
            }
        }

        private void SetEncounter(int index, Type targetType)
        {
            var targetObject = (EncounterGroupObject) target;

            targetObject.Group.Encounters[index] = Activator.CreateInstance(targetType) as Encounter;
                
            targetObject.Save();
            EditorUtility.SetDirty(targetObject);
            AssetDatabase.SaveAssets();
        }
    }
}