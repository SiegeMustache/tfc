﻿using System;
using TorkFramework;
using TorkFramework.Serialization;

namespace Kairos.Progression.Encounters
{
    public class EncounterGroup
    {
        [XmlSerialized] public int Tier;
        [XmlSerialized] public Encounter[] Encounters;
        [XmlSerialized(false)] public bool IsLastGroup;

        public EncounterGroup()
        {
            Tier = 1;
            Encounters = new Encounter[0];
            IsLastGroup = false;
        }

        public EncounterGroup(EncounterGroup source)
        {
            Tier = source.Tier;
            IsLastGroup = source.IsLastGroup;
            Encounters = new Encounter[source.Encounters.Length];
            for (int i = 0; i < Encounters.Length; i++)
            {
                Encounters[i] = Activator.CreateInstance(source.Encounters[i].GetType(), source.Encounters[i]) as Encounter;
            }
        }

        public void GenerateEncounterGroup(GameManager gameManager)
        {
            if (Encounters != null)
            {
                foreach (Encounter encounter in Encounters)
                {
                    encounter?.GenerateEncounter(gameManager);
                }
            }
        }
    }
}