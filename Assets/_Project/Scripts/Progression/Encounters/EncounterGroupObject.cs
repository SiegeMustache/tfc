﻿using System.Xml;
using TorkFramework.Serialization;
using UnityEngine;

namespace Kairos.Progression.Encounters
{
    [CreateAssetMenu(menuName = "Encounter Group")]
    public class EncounterGroupObject : ScriptableObject, ISerializationCallbackReceiver
    {
        [SerializeField] private string m_SerializedGroup;

        private EncounterGroup m_Group;

        public EncounterGroup Group
        {
            get
            {
                if(m_Group == null)
                    Load();
                return m_Group;
            }
        }
        
        public void Load()
        {
            if (string.IsNullOrEmpty(m_SerializedGroup))
            {
                m_Group = new EncounterGroup();
            }
            else
            {
                var deserializer = new Deserializer();
                m_Group = deserializer.DeserializeFromString<EncounterGroup>(m_SerializedGroup);
            }
        }

        public void Save()
        {
            if(m_Group == null)
                Load();
            
            var serializer = new Serializer(new XmlWriterSettings());
            m_SerializedGroup = serializer.SerializeToString(m_Group);
        }

        public void OnBeforeSerialize()
        {
            Save();
        }

        public void OnAfterDeserialize()
        {
            Load();
        }
    }
}