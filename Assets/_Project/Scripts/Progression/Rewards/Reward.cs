﻿using System;
using Kairos.Heroes;
using Kairos.Player;
using TorkFramework;
using TorkFramework.Serialization;
using UnityEngine;

namespace Kairos.Progression.Rewards
{
    public abstract class Reward
    {
        [XmlSerialized] public HeroReference Hero = new HeroReference();
        [XmlSerialized] public int ShopCost;
        
        public abstract void CollectRewardFromShop(GameManager gameManager, PlayerManager player, Action rewardCollectedCallback);
        public abstract void CollectReward(GameManager gameManager, PlayerManager player, Action rewardCollectedCallback);
    }
}