﻿using System;
using Kairos.Player;
using Kairos.Sentinels;
using TorkFramework;
using TorkFramework.ResourceManagement;
using TorkFramework.Serialization;
using UnityEngine;

namespace Kairos.Progression.Rewards
{
    public class SentinelReward : Reward
    {
        [XmlSerialized] public ResourceReference TargetSentinel;

        public override void CollectRewardFromShop(GameManager gameManager, PlayerManager player, Action rewardCollectedCallback)
        {
            SentinelObject sentinelObject = TargetSentinel.Resource as SentinelObject;
            player.Data.AddSentinel(sentinelObject.Sentinel);
            rewardCollectedCallback?.Invoke();
        }

        public override void CollectReward(GameManager gameManager, PlayerManager player, Action rewardCollectedCallback)
        {
            SentinelObject sentinelObject = TargetSentinel.Resource as SentinelObject;
            player.Data.AddSentinel(sentinelObject.Sentinel);
            rewardCollectedCallback?.Invoke();
        }
    }
}