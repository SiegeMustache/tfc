﻿using System;
using TorkFramework.Editor.Utility;
using TorkFramework.Utility;
using UnityEditor;
using UnityEngine;

namespace Kairos.Progression.Rewards.Editor
{
    [CustomEditor(typeof(RewardObject))]
    public class RewardEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            var targetObject = (RewardObject) target;
            
            EditorGUILayout.BeginHorizontal();
            if (targetObject.Reward == null)
                EditorGUILayout.LabelField("None");
            else
                EditorGUILayout.LabelField(targetObject.Reward.GetType().Name);
            if (GUILayout.Button("Change"))
            {
                GenericMenu menu = new GenericMenu();
                foreach (var rewardType in TypeUtils.GetDerivedTypes(typeof(Reward)))
                {
                    menu.AddItem(new GUIContent(rewardType.Name), false, () => { SetReward(targetObject, rewardType);});
                }
                menu.ShowAsContext();
            }
                
                
            EditorGUILayout.EndHorizontal();

            if (targetObject.Reward != null)
            {
                targetObject.Reward = FieldUtils.AdaptiveField("", targetObject.Reward) as Reward;
            }
            
            
            GUI.color = Color.green;
            if (GUILayout.Button("Save"))
            {
                targetObject.Save();
                EditorUtility.SetDirty(targetObject);
                AssetDatabase.SaveAssets();
            }
        }

        private void SetReward(RewardObject rewardObj, Type targetType)
        {
            rewardObj.Reward = Activator.CreateInstance(targetType) as Reward;
        }
    }
}