﻿using System;
using Kairos.Heroes;
using Kairos.Player;
using Kairos.UI.Rewards;
using TorkFramework;
using TorkFramework.Runtime.UI;
using TorkFramework.Serialization;

namespace Kairos.Progression.Rewards
{
    public class HeroReward : Reward
    {
        [XmlSerialized] public HeroReference TargetHero;

        public HeroReward()
        {
            TargetHero = new HeroReference();
        }

        public override void CollectRewardFromShop(GameManager gameManager, PlayerManager player, Action rewardCollectedCallback)
        {
            player.UnlockHero(TargetHero.Id);
            gameManager.GetManager<UIManager>().GetQuerableEntity<UIHeroRewardPopup>().ShowHeroReward(TargetHero.Id, rewardCollectedCallback);
        }

        public override void CollectReward(GameManager gameManager, PlayerManager player, Action rewardCollectedCallback)
        {
            player.UnlockHero(TargetHero.Id);
            gameManager.GetManager<UIManager>().GetQuerableEntity<UIHeroRewardPopup>().ShowHeroReward(TargetHero.Id, rewardCollectedCallback);
        }
    }
}