﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using TorkFramework.Serialization;
using UnityEngine;

namespace Kairos.Progression.Rewards
{
    [CreateAssetMenu(fileName = "newReward", menuName = "Reward")]
    public class RewardObject : ScriptableObject
    {
        private static int idCount = 0;
        
        [SerializeField] private string m_SerializedReward;

        private int m_Id = -1;
        
        public int ObjectID
        {
            get
            {
                if (m_Id < 0)
                {
                    m_Id = idCount;
                    idCount++;
                }

                return m_Id;
            }
        }
        
        private Reward m_Reward;

        public Reward Reward
        {
            get
            {
                if(m_Reward == null)
                    Load();
                return m_Reward;
            }
            set
            {
                m_Reward = value;
            }
        }
        
        public void Load()
        {
            if (string.IsNullOrEmpty(m_SerializedReward))
            {
                m_Reward = null;
            }
            else
            {
                var deserializer = new Deserializer();
                m_Reward = deserializer.DeserializeFromString<Reward>(m_SerializedReward);
            }
        }

        public void Save()
        {
            if(m_Reward == null)
                Load();
            
            var serializer = new Serializer(new XmlWriterSettings());
            m_SerializedReward = serializer.SerializeToString(m_Reward);
        }

        public void OnBeforeSerialize()
        {
            Save();
        }

        public void OnAfterDeserialize()
        {
            Load();
        }
    }
}