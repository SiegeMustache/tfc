﻿using System;
using System.Collections.Generic;
using Kairos.Indicators;
using Kairos.Units;
using TorkFramework;
using TorkFramework.Events;
using UnityEngine;

namespace Kairos.Controllers
{
    public class SelectionController : EntityBehaviour<GameEntity>
    {
        [SerializeField] private QuadraticIndicator m_SelectionIndicatorPrefab;
        //[SerializeField] private QuadraticIndicator m_SelectionIndicator;

        private LayerMask m_SelectionLayerMask;
        private Action<GameObject> m_SelectionDoneCallback;
        private bool m_Selecting = false;
        private ISelectable m_SelectedObject = null;
        private readonly List<QuadraticIndicator> m_SpawnedIndicators = new List<QuadraticIndicator>();

        public void SelectObject(Vector3 sourcePosition, LayerMask selectionLayerMask, Action<GameObject> selectionDoneCallback)
        {
            if (!m_Selecting)
            {
                //m_SelectionIndicator.gameObject.SetActive(true);
                //m_SelectionIndicator.transform.position = sourcePosition;
                m_SpawnedIndicators.Add(Instantiate(m_SelectionIndicatorPrefab));
                m_SpawnedIndicators[m_SpawnedIndicators.Count - 1].transform.position = sourcePosition;
                UpdateIndicator();

                m_SelectionDoneCallback = selectionDoneCallback;
                m_SelectionLayerMask = selectionLayerMask;
                m_Selecting = true;
            }
        }

        private void UpdateIndicator()
        {
            //var mousePosition = m_SelectionIndicator.EndPosition;
            var mousePosition = m_SpawnedIndicators[m_SpawnedIndicators.Count-1].EndPosition;
            var screenRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(screenRay, out var hit, m_SelectionLayerMask))
            {
                mousePosition = hit.point;
                if (Physics.Raycast(screenRay, out var selectionHit, m_SelectionLayerMask))
                {
                    var newObject = selectionHit.transform.GetComponent<ISelectable>();
                    if (newObject != null)
                    {
                        if (!(m_SelectedObject != null && m_SelectedObject == newObject))
                        {
                            if (m_SelectedObject != null)
                            {
                                m_SelectedObject.OnDeselect();
                                Entity.GameManager.GetManager<EventManager>().TriggerEvent(EventId.TargetUnselected, this);
                            }

                            m_SelectedObject = newObject;
                            if (m_SelectedObject != null)
                            {
                                m_SelectedObject.OnSelect(Team.Player);
                                Entity.GameManager.GetManager<EventManager>().TriggerEvent(EventId.TargetEnemySelected, this);
                            }
                        }
                    }
                    else
                    {
                        if (m_SelectedObject != null)
                        {
                            m_SelectedObject.OnDeselect();
                            Entity.GameManager.GetManager<EventManager>().TriggerEvent(EventId.TargetUnselected, this);
                            m_SelectedObject = null;
                        }
                    }
                }
            }

            //m_SelectionIndicator.EndPosition = mousePosition;
            m_SpawnedIndicators[m_SpawnedIndicators.Count-1].EndPosition = mousePosition;
            
            
            
            var midPosition = Vector3.Lerp(m_SpawnedIndicators[m_SpawnedIndicators.Count-1].transform.position, mousePosition, .5f);
            m_SpawnedIndicators[m_SpawnedIndicators.Count-1].ControlPosition = midPosition + Vector3.up * 5f;
            m_SpawnedIndicators[m_SpawnedIndicators.Count-1].DrawIndicator();

            //var midPosition = Vector3.Lerp(m_SelectionIndicator.transform.position, mousePosition, .5f);
            //m_SelectionIndicator.ControlPosition = midPosition + Vector3.up * 5f;
            //m_SelectionIndicator.DrawIndicator();
        }

        private void OnSubmitInput()
        {
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out var hit, float.MaxValue, m_SelectionLayerMask))
            {
                if (m_SelectedObject != null)
                {
                    m_SelectedObject.OnDeselect();
                    Entity.GameManager.GetManager<EventManager>().TriggerEvent(EventId.TargetUnselected, this);
                }
                m_Selecting = false;
                //m_SelectionIndicator.gameObject.SetActive(false);
                m_SelectionDoneCallback?.Invoke(hit.transform.gameObject);
            }
        }

        private void OnCancelInput()
        {
            if (m_SelectedObject != null)
            {
                m_SelectedObject.OnDeselect();
                Entity.GameManager.GetManager<EventManager>().TriggerEvent(EventId.TargetUnselected, this);
            }
            m_Selecting = false;
            //m_SelectionIndicator.gameObject.SetActive(false);
            m_SelectionDoneCallback?.Invoke(null);
        }

        public void Flush()
        {
            for (int i = m_SpawnedIndicators.Count - 1; i >= 0; i--)
            {
                DestroyImmediate(m_SpawnedIndicators[i]);
            }

            m_SpawnedIndicators.Clear();
        }

        private void Update()
        {
            if (m_Selecting)
            {
                UpdateIndicator();

                if (Input.GetKeyDown(KeyCode.Mouse0))
                    OnSubmitInput();
                else if (Input.GetKeyDown(KeyCode.Mouse1) || Input.GetKeyDown(KeyCode.Escape))
                    OnCancelInput();
            }
        }
    }
}