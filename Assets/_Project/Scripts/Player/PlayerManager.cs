﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using Kairos.Defaults;
using Kairos.Heroes;
using Kairos.Managers;
using TorkFramework;
using TorkFramework.Events;
using TorkFramework.Save;
using TorkFramework.Serialization;
using UnityEngine;

namespace Kairos.Player
{
    public class PlayerManager : Manager
    {
        [SerializeField] private DefaultData m_DefaultData;

        private PlayerProgressionData m_ProgressionData;
        private PlayerData m_Data;

        public PlayerData Data => m_Data;
        public PlayerProgressionData ProgressionData => m_ProgressionData;

        private void Start()
        {
            GameManager.GetManager<EventManager>().AddListener(EventId.BeforeSave, OnBeforeGameSave);
            GameManager.GetManager<EventManager>().AddListener(EventId.AfterLoad, OnAfterGameLoad);
            LoadData();
            if (m_ProgressionData == null)
            {
                m_ProgressionData = new PlayerProgressionData(m_DefaultData.UnlockedHeroes.ToList());
            }
        }

        private void OnDestroy()
        {
            GameManager.GetManager<EventManager>().RemoveListener(EventId.BeforeSave, OnBeforeGameSave);
            GameManager.GetManager<EventManager>().RemoveListener(EventId.AfterLoad, OnAfterGameLoad);
        }

        public void StartNewGame(PlayerData newPlayerData)
        {
            m_Data = newPlayerData;

            GameManager.GetManager<LevelManager>().ShowLevelSelection();
        }

        public void ClearData()
        {
            m_Data = null;
        }

        private void OnBeforeGameSave(GenericEventArgs args, Action listenerExecutedCallback)
        {
            SaveData();
        }

        private void OnAfterGameLoad(GenericEventArgs args, Action listenerExecutedCallback)
        {
            LoadData();
        }

        private void SaveData()
        {
            var serializer = new Serializer(new XmlWriterSettings());
            if (!GameManager.GetManager<SaveManager>().LoadedData.ContainsKey("PlayerData"))
            {
                GameManager.GetManager<SaveManager>().LoadedData.Add("PlayerData", serializer.SerializeToString(m_Data));
            }
            else
            {
                GameManager.GetManager<SaveManager>().LoadedData["PlayerData"] = serializer.SerializeToString(m_Data);
            }

            if (!GameManager.GetManager<SaveManager>().LoadedData.ContainsKey("PlayerProgressionData"))
            {
                GameManager.GetManager<SaveManager>().LoadedData.Add("PlayerProgressionData", serializer.SerializeToString(m_ProgressionData));
            }
            else
            {
                GameManager.GetManager<SaveManager>().LoadedData["PlayerProgressionData"] = serializer.SerializeToString(m_ProgressionData);
            }
        }

        private void LoadData()
        {
            var deserializer = new Deserializer();
            if (GameManager.GetManager<SaveManager>().LoadedData.ContainsKey("PlayerData"))
            {
                m_Data = deserializer.DeserializeFromString<PlayerData>(GameManager.GetManager<SaveManager>().LoadedData["PlayerData"] as string);
            }

            if (GameManager.GetManager<SaveManager>().LoadedData.ContainsKey("PlayerProgressionData"))
            {
                m_ProgressionData = deserializer.DeserializeFromString<PlayerProgressionData>(GameManager.GetManager<SaveManager>().LoadedData["PlayerProgressionData"] as string);
            }
        }

        public void UsePotion()
        {
            if (m_Data.PotionCount > 0 && m_Data.Hero.ActualLifePoints < m_Data.Hero.TargetUnit.MaxLifePoints)
            {
                var roundManager = GameManager.GetManager<RoundManager>();
                if (roundManager.IsLoaded)
                {
                    if (roundManager.Player.ActualState == PlayerController.PlayerStates.Idle)
                    {
                        m_Data.PotionCount--;
                        roundManager.Board.HeroUnit.HealNoSource(HeroDatabase.LoadedHeroes[m_Data.HeroId].PotionHealingAmount, () => { });
                        roundManager.Player.Timeline.UpdateSnapshots();
                    }
                }
                else
                {
                    m_Data.PotionCount--;
                    m_Data.Hero.ActualLifePoints += HeroDatabase.LoadedHeroes[m_Data.HeroId].PotionHealingAmount;
                }
            }
        }

        public void UnlockHero(string hero)
        {
            m_ProgressionData.UnlockHero(hero);
            SaveData();
        }
    }
}