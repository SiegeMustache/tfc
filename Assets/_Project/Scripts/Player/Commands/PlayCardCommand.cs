﻿using System;
using Kairos.Cards;
using Kairos.Controllers;
using Kairos.Timeline;
using TorkFramework;
using TorkFramework.Events;
using UnityEngine.XR;

namespace Kairos.Player
{
    public class PlayCardCommand : PlayerCommand
    {
        private TimelineController.TimelineAction m_Action;
        private bool m_Executed = false;
        
        public PlayCardCommand(params object[] parameters) : base(parameters) { }
        
        public override void Execute(GameManager gameManager, PlayerController player, Action<bool> commandExecuteCallback)
        {
            var board = m_Parameters[0] as BoardController;
            var timeline = m_Parameters[1] as TimelineController;
            var card = m_Parameters[2] as CardInfo;
            if (card != null && player.Hand.Contains(card))
            {
                player.ActualState = PlayerController.PlayerStates.PlayingCard;
                timeline.AssignCard(true, card, board.HeroUnit, (playResult, action) =>
                {
                    if (playResult)
                    {
                        m_Action = action;
                        m_Executed = true;
                        gameManager.GetManager<EventManager>().TriggerEvent(EventId.PlayerCardPlayed, this, () =>
                        {
                            commandExecuteCallback?.Invoke(true);
                            player.DiscardCard(card, false, () => { player.ActualState = PlayerController.PlayerStates.Idle; });
                        }, card);
                    }
                    else
                    {
                        commandExecuteCallback?.Invoke(false);
                        player.ActualState = PlayerController.PlayerStates.Idle;
                    }
                });
            }
        }

        public override void Undo(GameManager gameManager, PlayerController player, Action<bool> commandUndoCallback)
        {
            if (!m_Executed)
            {
                commandUndoCallback?.Invoke(false);
                return;
            }
            
            var timeline = m_Parameters[1] as TimelineController;
            player.ResetCard(m_Action.Card, () =>
            {
                gameManager.GetManager<EventManager>().TriggerEvent(EventId.PlayerCardUndo, this, () =>
                {
                    timeline.RemoveAction(m_Action);
                    commandUndoCallback?.Invoke(true);
                }, m_Action.Card);
            });
        }
    }
}