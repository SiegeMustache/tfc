﻿using Kairos.Sentinels.Effects;
using Kairos.Timeline;
using System;
using System.Collections.Generic;
using TorkFramework;
using TorkFramework.Events;
using TorkFramework.Serialization;

namespace Kairos.Sentinels
{
    public class SentinelController : EntityBehaviour<GameEntity>
    {
        public enum SentinelState
        {
            Inactive,
            Active,
            EffectPlaced,
            Disabled
        }
        public SentinelState m_CurrentState;
        
        public int CurrentActivations;
        public int TargetTimeframe;
        public SentinelInfo SentinelData;
        public SentinelEffect CurrentEffect;
        public TimelineController TimelineController;

        private void OnTimelineExecuted(GenericEventArgs args, Action timelineExecutedCallback)
        {
            if(m_CurrentState == SentinelState.EffectPlaced)
            {
                CurrentActivations -= 1;
                if(CurrentActivations > 0)
                {
                    m_CurrentState = SentinelState.Active;
                }
                else
                {
                    m_CurrentState = SentinelState.Inactive;
                }
            }
            timelineExecutedCallback?.Invoke();
        }

        public void UndoSentinelActionFromTimeline(SentinelController source, Action actionUndidCallback)
        {
            if(m_CurrentState == SentinelState.EffectPlaced)
            {
                CurrentActivations += 1;
                TimelineController.UndoSentinelAction(source, actionUndidCallback);
            }
        }

        public void InitializeSentinel()
        {
            Entity.GameManager.GetManager<EventManager>().AddListener(EventId.SentinelTimelineEnded, OnTimelineExecuted);
            CurrentActivations = SentinelData.MaxActivations;
            if(SentinelData != null)
            {
                CurrentEffect = SentinelData.Effect.Resource as SentinelEffect;
            }

            if(CurrentActivations > 0)
            {
                m_CurrentState = SentinelState.Active;
            }
            else
            {
                m_CurrentState = SentinelState.Inactive;
            }
        }

        public void TerminateSentinel()
        {
            Entity.GameManager.GetManager<EventManager>().RemoveListener(EventId.SentinelTimelineEnded, OnTimelineExecuted);
            m_CurrentState = SentinelState.Disabled;
        }
    }
}

