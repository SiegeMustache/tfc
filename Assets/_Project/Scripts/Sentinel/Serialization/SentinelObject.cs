﻿using System.Xml;
using TorkFramework.Serialization;
using UnityEngine;

namespace Kairos.Sentinels
{
    [CreateAssetMenu(menuName = "Sentinel Info")]
    public class SentinelObject : ScriptableObject, ISerializationCallbackReceiver
    {
        [SerializeField] private string m_SerializedSentinel;

        private SentinelInfo m_Sentinel;

        public SentinelInfo Sentinel
        {
            get
            {
                if(m_Sentinel == null)
                {
                    Load();
                }
                return m_Sentinel;
            }
        }

        public void Load()
        {
            if(string.IsNullOrEmpty(m_SerializedSentinel))
            {
                m_Sentinel = new SentinelInfo();
            }
            else
            {
                var deserializer = new Deserializer();
                m_Sentinel = deserializer.DeserializeFromString<SentinelInfo>(m_SerializedSentinel);
            }
        }

        public void Save()
        {
            if(m_Sentinel == null)
            {
                Load();
            }

            var serializer = new Serializer(new XmlWriterSettings());
            m_SerializedSentinel = serializer.SerializeToString(m_Sentinel);
        }

        public void OnAfterDeserialize()
        {
            Load();
        }

        public void OnBeforeSerialize()
        {
            Save();
        }
    }
}
