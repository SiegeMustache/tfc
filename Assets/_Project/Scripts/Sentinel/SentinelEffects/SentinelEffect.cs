﻿using TorkFramework.ResourceManagement;
using TorkFramework.Serialization;
using TorkFramework.VFX.Animations;
using System;
using UnityEngine;
using Kairos.Timeline;
using Kairos.Units;
using Kairos.Controllers;
using static Kairos.Timeline.TimelineController;

namespace Kairos.Sentinels.Effects
{
    public abstract class SentinelEffect : ScriptableObject
    {
        [XmlSerialized] public ResourceReference VFXPrefab;
        [XmlSerialized] public VFXAnimation[] EffectVFX;
        [XmlSerialized] public int NextTargetTimeframes;
        [XmlSerialized] public int PreviousTargetTimeframes;
        [XmlSerialized] public Team TargetTeam;
        public abstract void ExecuteEffect(int targetTimeframes, BoardController board, TimelineController timelineController, Action effectExecutedCallback);

        public abstract void ExecuteOnSnapshot(ref TimelineSnapshot snapshot, TimelineController timeline, int timeframe);

        public abstract bool IsPreviousExecution();
    }
}

