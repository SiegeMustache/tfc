﻿using Kairos.Sentinels;
using System.Collections;
using System.Collections.Generic;
using TorkFramework.Serialization;
using System;
using Kairos.Timeline;
using UnityEngine;
using Kairos.Units;
using Kairos.Sentinels.Effects;
using Kairos.Controllers;

namespace Kairos.Sentinels.Effects
{
    [CreateAssetMenu(fileName = "SE_TimelineActionShift", menuName ="CUSTOM/SENTINEL_EFFECTS/Timeline_Action_Shift")]
    public class ShiftEnemyAction : SentinelEffect
    {
        [XmlSerialized] public int TimeframeShiftAmount;
        public enum ShiftMode
        {
            Forward,
            Backward
        }
        [XmlSerialized] public ShiftMode ActionShiftMode;

        public override void ExecuteEffect(int targetTimeframe, BoardController boardController, TimelineController timelineController, Action effectExecutedCallback)
        {
            if(ActionShiftMode == ShiftMode.Forward)
            {
                timelineController.ShiftForwardAction(TimeframeShiftAmount,targetTimeframe, TargetTeam, () =>
                {
                    effectExecutedCallback?.Invoke();
                });
            }
            else
            {
                timelineController.ShiftBackwardAction(TimeframeShiftAmount, targetTimeframe, TargetTeam, () =>
                {
                    effectExecutedCallback?.Invoke();
                });
            }
        }

        public override void ExecuteOnSnapshot(ref TimelineController.TimelineSnapshot snapshot, TimelineController timeline, int timeframe)
        {
            throw new NotImplementedException();
        }

        public override bool IsPreviousExecution()
        {
            throw new NotImplementedException();
        }
    }
}

