﻿using Kairos.Controllers;
using Kairos.Timeline;
using Kairos.Units;
using System;
using System.Collections;
using System.Collections.Generic;
using TorkFramework.Serialization;
using UnityEngine;

namespace Kairos.Sentinels.Effects
{
    [CreateAssetMenu(fileName = "SE_Timeframe_Intercept", menuName = "CUSTOM/SENTINEL_EFFECTS/Timeframe_Intercept")]
    public class SentinelDealDamageIntercept : SentinelEffect
    {
        [XmlSerialized] public int DamageAmount;
        public enum InterceptMode
        {
            Heal,
            Damage
        }
        [XmlSerialized] public InterceptMode InterceptActionType;

        public override void ExecuteEffect(int targetTimeframes, BoardController board, TimelineController timelineController, Action effectExecutedCallback)
        {
            List<UnitController> targetList = timelineController.GetUnitsAtTimeframe(targetTimeframes);
            foreach(UnitController unit in targetList)
            {
                if(unit.Team == TargetTeam)
                {
                    if(InterceptActionType == InterceptMode.Damage)
                    {
                        unit.DealDamageNoSource(DamageAmount, effectExecutedCallback);
                    }
                    else
                    {
                        unit.HealNoSource(DamageAmount, effectExecutedCallback);
                    }
                }
            }   
        }

		public override void ExecuteOnSnapshot(ref TimelineController.TimelineSnapshot snapshot, TimelineController timeline, int timeframe)
		{
            List<UnitController> targetList = timeline.GetUnitsAtTimeframe(timeframe);
            foreach (UnitController unit in targetList)
            {
                if (unit.Team == TargetTeam)
                {
                    if(unit.Team == Team.Player)
					{
                        if (InterceptActionType == InterceptMode.Damage)
                        {
                            snapshot.HeroSnapshot.DealDamageNoSource(DamageAmount);
                        }
                        else
                        {
                            snapshot.HeroSnapshot.Heal(DamageAmount);
                        }
                    }
                    else
					{
                        for(int i = 0; i < snapshot.EnemiesSnapshots.Length; i++)
						{
                            if (snapshot.EnemiesSnapshots[i].SourceUnit == unit)
                            {
                                if (InterceptActionType == InterceptMode.Damage)
                                {
                                    snapshot.EnemiesSnapshots[i].DealDamageNoSource(DamageAmount);
                                }
                                else
                                {
                                    snapshot.EnemiesSnapshots[i].Heal(DamageAmount);
                                }
                            }
                        }
					}
                }
            }
        }

		public override bool IsPreviousExecution()
		{
            return true;
		}
	}
}

