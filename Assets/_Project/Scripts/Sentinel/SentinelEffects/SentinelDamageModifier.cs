﻿using Kairos.Controllers;
using Kairos.Timeline;
using Kairos.Units;
using System;
using TorkFramework.Serialization;
using UnityEngine;

namespace Kairos.Sentinels.Effects
{
    [CreateAssetMenu(fileName = "SO_SE_Timeframe_DamageModifier", menuName = "CUSTOM/SENTINEL_EFFECTS/Timeframe_Damage_Modifier")]
    public class SentinelDamageModifier : SentinelEffect
    {
        [XmlSerialized] public int DamageModifier;

        public override void ExecuteEffect(int targetTimeframe, BoardController boardController, TimelineController timelineController, Action effectExecutedCallback)
        {
            Debug.Log("EFFECT EXECUTED");
            if(TargetTeam == Units.Team.Enemy)
            {
                if(boardController.EnemyUnits.Length > 0)
                {
                    foreach (UnitController unit in boardController.EnemyUnits)
                    {
                        if (targetTimeframe >= 1 && targetTimeframe <= Globals.TIMELINE_SIZE)
                        {

                            unit.AddSentinelBoostModifier(DamageModifier, targetTimeframe, effectExecutedCallback);
                        }
                    }
                }
                else
                {
                    effectExecutedCallback?.Invoke();
                }
            }
            else
            {
                if(boardController.HeroUnit != null)
                {
                    UnitController hero = boardController.HeroUnit;

                    if (targetTimeframe >= 1 && targetTimeframe <= Globals.TIMELINE_SIZE)
                    {
                        hero.AddSentinelBoostModifier(DamageModifier, targetTimeframe, effectExecutedCallback);
                    }
                }
                else
                {
                    effectExecutedCallback?.Invoke();
                }
            }
        }

		public override void ExecuteOnSnapshot(ref TimelineController.TimelineSnapshot snapshot, TimelineController timeline, int timeframe)
		{
            if (TargetTeam == Units.Team.Enemy)
            {
                if (snapshot.EnemiesSnapshots.Length > 0)
                {
                    for(int i = 0; i < snapshot.EnemiesSnapshots.Length; i++)
                    {
                        snapshot.EnemiesSnapshots[i].SentinelDamageBoost += DamageModifier;
                    }
                }
            }
            else
            {
                snapshot.HeroSnapshot.SentinelDamageBoost += DamageModifier;
            }
        }

		public override bool IsPreviousExecution()
		{
            return false;
		}
	}
}

