﻿using UnityEditor;
using UnityEngine;

namespace Kairos.Keywords.Editor
{
    public class KeywordEditorWindow : EditorWindow
    {
        private Vector2 m_ScrollPosition;
        private string m_NewKeyword;
        private int m_SelectedItem = -1;

        [MenuItem("Custom/Keywords")]
        public static void Init()
        {
            var window = EditorWindow.GetWindow<KeywordEditorWindow>();
            window.name = "Keywords";
            window.titleContent = new GUIContent("Keywords");
            window.Show();
        }
        
        private void OnGUI()
    {
        EditorGUILayout.BeginScrollView(m_ScrollPosition);
        EditorGUILayout.LabelField("Keywords", EditorStyles.boldLabel);
        m_SelectedItem = GUILayout.SelectionGrid(m_SelectedItem, KeywordDatabase.GetKeywordsList(), 1);

        EditorGUILayout.LabelField("General", EditorStyles.boldLabel);
        EditorGUILayout.BeginHorizontal();
        m_NewKeyword = EditorGUILayout.TextField(m_NewKeyword);
        if (GUILayout.Button("+"))
        {
            if (!string.IsNullOrEmpty(m_NewKeyword) && !string.IsNullOrWhiteSpace(m_NewKeyword) && !KeywordDatabase.LoadedKeywords.ContainsKey(m_NewKeyword))
            {
                KeywordDatabase.LoadedKeywords.Add(m_NewKeyword.ToUpper(), new KeywordInfo {KeywordText = m_NewKeyword});
                m_NewKeyword = "";
                Repaint();
            }
        }

        EditorGUILayout.EndHorizontal();
        if (GUILayout.Button("Save"))
        {
            KeywordDatabase.Save();
            KeywordDatabase.Load();
            Repaint();
        }

        if (m_SelectedItem >= 0)
        {
            EditorGUILayout.LabelField("Details", EditorStyles.boldLabel);
            var targetKeyword = KeywordDatabase.LoadedKeywords[KeywordDatabase.GetKeywordsList()[m_SelectedItem]];
            targetKeyword.KeywordDescription = EditorGUILayout.TextField("Description", targetKeyword.KeywordDescription);
            targetKeyword.KeywordStyleOpener = EditorGUILayout.TextField("Style Opener", targetKeyword.KeywordStyleOpener);
            targetKeyword.KeywordStyleCloser = EditorGUILayout.TextField("Style Closer", targetKeyword.KeywordStyleCloser);

            EditorGUILayout.LabelField("Preview", EditorStyles.boldLabel);
            var style = EditorStyles.textField;
            style.richText = true;
            style.wordWrap = true;
            EditorGUILayout.LabelField(targetKeyword.KeywordStyleOpener + targetKeyword.KeywordText + targetKeyword.KeywordStyleCloser, style);
            if (GUILayout.Button("Delete"))
            {
                KeywordDatabase.LoadedKeywords.Remove(KeywordDatabase.GetKeywordsList()[m_SelectedItem]);
                m_SelectedItem = -1;
                Repaint();
            }
        }

        EditorGUILayout.EndScrollView();
    }
    }
}