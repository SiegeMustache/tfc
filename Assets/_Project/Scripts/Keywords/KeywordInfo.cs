﻿using System;
using TorkFramework.Serialization;

namespace Kairos.Keywords
{
    [Serializable]
    public class KeywordInfo
    {
        [XmlSerialized] public string KeywordText;
        [XmlSerialized] public string KeywordStyleOpener;
        [XmlSerialized] public string KeywordStyleCloser;
        [XmlSerialized] public string KeywordDescription;
        [XmlSerialized] public int NextWordsToInclude = 0;

        public KeywordInfo()
        {
            KeywordText = "";
            KeywordStyleOpener = "";
            KeywordStyleCloser = "";
            KeywordDescription = "";
            NextWordsToInclude = 0;
        }
    }
}