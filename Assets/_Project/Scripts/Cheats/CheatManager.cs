﻿using System;
using Kairos.Cards;
using Kairos.Player;
using TorkFramework;
using UnityEngine;

namespace Kaiors.Cheats
{
    public class CheatManager : Manager
    {
        [SerializeField, Card] private string[] m_CheatCards;

        private float timer = 0f;
        
        void Update()
        {
#if CHEAT_ENABLED

            if (timer > 0f)
            {
                timer -= Time.deltaTime;
                return;
            }

            if (Input.GetKey(KeyCode.C) && Input.GetKey(KeyCode.H) && Input.GetKey(KeyCode.T) && Input.GetKey(KeyCode.LeftAlt) && Input.GetKey(KeyCode.LeftControl))
            {
                GiveCheat();
                timer = 5f;
            }
#endif
        }


        private void GiveCheat()
        {
            try
            {
                var player = FindObjectOfType<PlayerController>();
                foreach (var cheatCard in m_CheatCards)
                {
                    player.DeckPile.Insert(0, new CardInfo(CardDatabase.LoadedCards[cheatCard]));
                }
                
                player.DrawCards(m_CheatCards.Length, () => {Debug.Log("Cheat activated");});
            }
            catch (Exception ex)
            {
                Debug.Log($"Unable to apply cheat.\n{ex}");
            }
        }
    }
}