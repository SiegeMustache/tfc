﻿using System;
using System.IO;
using TorkFramework.Editor.Utility;
using TorkFramework.ResourceManagement;
using UnityEditor;
using UnityEngine;

namespace Kairos.Heroes.Editor
{
    public class HeroEditorWindow : EditorWindow
    {
        private string m_SelectedHeroPath;
        private Vector2 m_ScrollPosition;
        
        public HeroInfo SelectedHero;
        
        [MenuItem("Custom/Heroes/Reload")]
        public static void ReloadHeroesDatabase()
        {
            HeroDatabase.Reload();
        }
        
        [MenuItem("Custom/Heroes/New Hero")]
        public static void CreateNewHero()
        {
            var newHero = HeroHelper.GenerateNewHero();
            HeroHelper.SaveHero(newHero, Globals._HEROES_FOLDER + "/newHero.hero");
            HeroDatabase.Reload();
        }
        
        [MenuItem("Custom/Heroes/Editor")]
        public static void Init()
        {
            var window = EditorWindow.GetWindow<HeroEditorWindow>();
            window.name = "Hero Editor";
            window.titleContent = new GUIContent("Hero Editor");
            window.Show();
        }
        
        private void OnEnable()
        {
            OnSelectionChange();
        }
        
        private void OnSelectionChange()
        {
            if (Selection.objects.Length == 1 && Path.GetExtension(AssetDatabase.GetAssetPath(Selection.objects[0])).ToLower() == ".hero")
            {
                m_SelectedHeroPath = Application.dataPath.Substring(0, Application.dataPath.Length - 6) + AssetDatabase.GetAssetPath(Selection.objects[0]);
                SelectedHero = HeroHelper.LoadHero(m_SelectedHeroPath);
            }

            Repaint();
        }
        
        private void OnGUI()
    {
        if (SelectedHero == null)
            return;

        m_ScrollPosition = EditorGUILayout.BeginScrollView(m_ScrollPosition);

        EditorGUI.BeginDisabledGroup(true);
        EditorGUILayout.TextField("ID", SelectedHero.HeroId);
        EditorGUI.EndDisabledGroup();

        SelectedHero.Title = FieldUtils.AdaptiveField("Title", SelectedHero.Title) as string;
        SelectedHero.DeckName = FieldUtils.AdaptiveField("DeckName", SelectedHero.DeckName) as string;
        SelectedHero.DeckSprite = FieldUtils.AdaptiveField("Deck Sprite", SelectedHero.DeckSprite) as ResourceReference;
        SelectedHero.PotionCount = (int) FieldUtils.AdaptiveField("Start Potion Count", SelectedHero.PotionCount);
        SelectedHero.PotionHealingAmount = (int) FieldUtils.AdaptiveField("Potion Healing Amount", SelectedHero.PotionHealingAmount);
        
        var so = new SerializedObject(this);
        var soHero = so.FindProperty("SelectedHero");
        var soUnit = soHero.FindPropertyRelative("HeroUnit");
        EditorGUILayout.PropertyField(soUnit);
        EditorGUILayout.LabelField("Cards");
        EditorGUI.indentLevel++;
        var soCards = soHero.FindPropertyRelative("Cards");
        for (int i = 0; i < SelectedHero.Cards.Length; i++)
        {
            EditorGUILayout.PropertyField(soCards.GetArrayElementAtIndex(i));
        }

        so.ApplyModifiedProperties();

        var size = EditorGUILayout.IntField("Size", SelectedHero.Cards.Length);
        if (size != SelectedHero.Cards.Length)
            Array.Resize(ref SelectedHero.Cards, size);
        EditorGUI.indentLevel--;

        if (GUILayout.Button("Save"))
        {
            HeroHelper.SaveHero(SelectedHero, m_SelectedHeroPath);
            Repaint();
        }
        
        EditorGUILayout.EndScrollView();
    }
    }
}