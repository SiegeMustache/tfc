﻿using System;
using TorkFramework.Serialization;

namespace Kairos.Heroes
{
    [Serializable]
    public class HeroReference
    {
        [XmlSerialized] public string Id;
        public HeroInfo Hero => HeroDatabase.LoadedHeroes[Id];

        public HeroReference()
        {
            Id = "";
        }
    }
}