﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Kairos.Heroes
{
    public static class HeroDatabase
    {
        private static Dictionary<string, HeroInfo> m_LoadedHeroes;
        public static Dictionary<string, HeroInfo> LoadedHeroes
        {
            get
            {
                if(m_LoadedHeroes == null)
                    Reload();
                return m_LoadedHeroes;
            }
        }
        
        public static HeroInfo[] GetHeroesList()
        {
            HeroInfo[] result = new HeroInfo[LoadedHeroes.Count];

            int counter = 0;
            foreach (var loadedHero in LoadedHeroes)
            {
                result[counter] = loadedHero.Value;
                counter++;
            }
        
            return result;
        }
        
        public static string[] GetIdList()
        {
            string[] result = new string[LoadedHeroes.Count];

            int counter = 0;
            foreach (var loadedHero in LoadedHeroes)
            {
                result[counter] = loadedHero.Key;
                counter++;
            }
        
            return result;
        }
        
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterAssembliesLoaded)]
        public static void Reload()
        {
            m_LoadedHeroes = new Dictionary<string, HeroInfo>();

            var filesToLoad = Directory.GetFiles(Globals._HEROES_FOLDER);
            foreach (var file in filesToLoad)
            {
                if (Path.GetExtension(file).ToLower() == ".hero")
                {
                    var newHero = HeroHelper.LoadHero(file);
                    LoadedHeroes.Add(newHero.HeroId, newHero);
                }
            }
        }
    }
}