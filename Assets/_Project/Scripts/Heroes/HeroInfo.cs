﻿using System;
using Kairos.Cards;
using Kairos.Units;
using TorkFramework.ResourceManagement;
using TorkFramework.Serialization;

namespace Kairos.Heroes
{
    [Serializable]
    public class HeroInfo
    {
        [XmlSerialized] public string HeroId;
        [XmlSerialized, Unit] public string HeroUnit;
        [XmlSerialized, Card] public string[] Cards;
        [XmlSerialized] public string Title;
        [XmlSerialized] public string DeckName;
        [XmlSerialized] public ResourceReference DeckSprite;
        [XmlSerialized] public int PotionCount;
        [XmlSerialized] public int PotionHealingAmount;

        public HeroInfo()
        {
            Cards = new string[0];
        }

    }
}
