﻿using Kairos.Controllers;
using Kairos.Timeline;
using Kairos.Units;

namespace Kairos.Cards.EffectSelectors
{
    [CardGenerationPattern("intercept")]
    public class InterceptEffectSelector : CardEffectSelector
    {
        public override bool CheckEffect(TimelineController.TimelineAction action, UnitController source, UnitController[] targets, TimelineController timeline, BoardController board, out bool overrideEffects, out int repetitions)
        {
            repetitions = 1;
            foreach (var unit in timeline.GetUnitsAtTimeframe(action.Timeframe))
            {
                if (unit != source && source.Team != unit.Team)
                {
                    overrideEffects = true;
                    return true;
                }
            }

            overrideEffects = false;
            return false;
        }

        public override bool CheckEffectOnSnapshot(ref TimelineController.TimelineSnapshot snapshot, TimelineController.TimelineAction action, TimelineController timeline, out bool overrideEffects, out int repetitions)
        {
            repetitions = 1;
            
            foreach (var unit in timeline.GetUnitsAtTimeframe(action.Timeframe))
            {
                if (unit != action.Source && action.Source.Team != unit.Team)
                {
                    overrideEffects = true;
                    return true;
                }
            }

            overrideEffects = false;
            return false;
        }

        public override string GetSelectorValue(ref TimelineController.TimelineUnitSnapshot? snapsho)
        {
            return "";
        }
    }
}