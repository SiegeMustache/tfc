﻿using Kairos.Controllers;
using Kairos.Timeline;
using Kairos.Units;
using TorkFramework.Serialization;

namespace Kairos.Cards.EffectSelectors
{
    [CardGenerationPattern("precision <%i:TargetTimeframe>")]
    public class PrecisionEffectSelector : CardEffectSelector
    {
        [XmlSerialized] public int TargetTimeframe;
        public override bool CheckEffect(TimelineController.TimelineAction action, UnitController source, UnitController[] targets, TimelineController timeline, BoardController board, out bool overrideEffects, out int repetitions)
        {
            repetitions = 1;
            if (action.Timeframe == TargetTimeframe)
            {
                overrideEffects = true;
                return true;
            }

            overrideEffects = false;
            return false;
        }

        public override bool CheckEffectOnSnapshot(ref TimelineController.TimelineSnapshot snapshot, TimelineController.TimelineAction action, TimelineController timeline, out bool overrideEffects, out int repetitions)
        {
            repetitions = 1;
            if (action.Timeframe == TargetTimeframe)
            {
                overrideEffects = true;
                return true;
            }

            overrideEffects = false;
            return false;
        }

        public override string GetSelectorValue(ref TimelineController.TimelineUnitSnapshot? snapsho)
        {
            return TargetTimeframe.ToString();
        }
    }
}