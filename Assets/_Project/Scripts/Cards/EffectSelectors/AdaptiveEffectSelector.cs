﻿using Kairos.Controllers;
using Kairos.Timeline;
using Kairos.Units;

namespace Kairos.Cards.EffectSelectors
{
    [CardGenerationPattern("adaptive")]
    public class AdaptiveEffectSelector : CardEffectSelector
    {
        public override bool CheckEffect(TimelineController.TimelineAction action, UnitController source, UnitController[] targets, TimelineController timeline, BoardController board, out bool overrideEffects, out int repetitions)
        {
            overrideEffects = false;
            repetitions = action.Timeframe;
            return true;
        }

        public override bool CheckEffectOnSnapshot(ref TimelineController.TimelineSnapshot snapshot, TimelineController.TimelineAction action, TimelineController timeline, out bool overrideEffects, out int repetitions)
        {
            overrideEffects = false;
            repetitions = action.Timeframe;
            return true;
        }

        public override string GetSelectorValue(ref TimelineController.TimelineUnitSnapshot? snapsho)
        {
            return "";
        }
    }
}