﻿using Kairos.Controllers;
using Kairos.Timeline;
using Kairos.Units;

namespace Kairos.Cards.EffectSelectors
{
    [CardGenerationPattern("alpha")]
    public class AlphaEffectSelector : CardEffectSelector
    {
        public override bool CheckEffect(TimelineController.TimelineAction action, UnitController source, UnitController[] targets, TimelineController timeline, BoardController board, out bool overrideEffects, out int repetitions)
        {
            overrideEffects = false;
            repetitions = 1;
            if (timeline.IsFirstPlayedAction(action))
                return true;
            return false;
        }

        public override bool CheckEffectOnSnapshot(ref TimelineController.TimelineSnapshot snapshot, TimelineController.TimelineAction action, TimelineController timeline, out bool overrideEffects, out int repetitions)
        {
            overrideEffects = false;
            repetitions = 1;
            if (timeline.IsFirstPlayedAction(action))
                return true;
            return false;
        }

        public override string GetSelectorValue(ref TimelineController.TimelineUnitSnapshot? snapsho)
        {
            return "";
        }
    }
}