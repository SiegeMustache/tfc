﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Xml;
using Kairos.Timeline;
using Kairos.Units;
using TorkFramework.Serialization;
using UnityEditor;
using UnityEngine;

namespace Kairos.Cards
{
    public static class CardHelper
    {
        public static void SaveCard(CardInfo card, string path)
        {
#if UNITY_EDITOR
            if (!Directory.Exists(Path.GetDirectoryName(path)))
                Directory.CreateDirectory(Path.GetDirectoryName(path));

            var serializer = new Serializer(new XmlWriterSettings());
            serializer.SerializeToFile(path, card);

            AssetDatabase.Refresh();
#endif
        }

        public static CardInfo LoadCard(string path)
        {
            if (!File.Exists(path))
                throw new FileNotFoundException();

            if (Path.GetExtension(path).ToLower() != ".card")
                throw new BadImageFormatException();

            var deserializer = new Deserializer();
            return deserializer.DeserializeFromFile<CardInfo>(path);
        }

        private static string GenerateCardId()
        {
            var result = default(byte[]);

            using (var stream = new MemoryStream())
            {
                using (var writer = new BinaryWriter(stream, Encoding.UTF8, true))
                {
                    writer.Write(DateTime.Now.Ticks);
                }

                stream.Position = 0;

                using (var hash = SHA256.Create())
                {
                    result = hash.ComputeHash(stream);
                }
            }

            var resultText = "";
            for (int i = 0; i < result.Length; i++)
            {
                resultText += result[i].ToString("x2");
            }

            return "card-" + resultText;
        }

        public static CardInfo GenerateNewCard()
        {
            return new CardInfo {CardId = GenerateCardId()};
        }

        public static int GetEffectiveCardSpeed(CardInfo card, TimelineController.TimelineUnitSnapshot? unitSnapshot, out int changed)
        {
            if (!unitSnapshot.HasValue)
            {
                changed = 0;
                return card.CardSpeed;
            }
            else
            {
                var result = Mathf.Clamp(card.CardSpeed + unitSnapshot.Value.SpeedModifier, 1, Globals.TIMELINE_SIZE);
                if (result > card.CardSpeed)
                    changed = -1;
                else if (result < card.CardSpeed)
                    changed = 1;
                else
                    changed = 0;
                return result;
            }
        }

        public static string GetEffectiveCardDescription(CardInfo card, TimelineController.TimelineUnitSnapshot? unitSnapshot)
        {
            StringBuilder result = new StringBuilder();

            foreach (var sentence in card.CardGeneratedDescription.Split('.'))
            {
                var clearedSentence = sentence.Replace("\n", "");
                if (clearedSentence.Contains("#") || clearedSentence.Contains("@"))
                {
                    var newLine = "";

                    foreach (var word in clearedSentence.Split(' '))
                    {
                        if (word.Length > 0)
                        {
                            if (word[0] == '#')
                            {
                                var subWord = word.Replace("#", "");
                                var sentenceIndex = int.Parse(subWord.Split('-')[0]);
                                var effectIndex = int.Parse(subWord.Split('-')[1]);

                                newLine += card.Sentences[sentenceIndex].Effects[effectIndex].GetEffectValue(unitSnapshot) + " ";
                            }
                            else if (word[0] == '@')
                            {
                                var subWord = word.Replace("@", "");
                                var sentenceIndex = int.Parse(subWord.Split('-')[0]);
                                var selectorIndex = int.Parse(subWord.Split('-')[1]);

                                newLine += card.Sentences[sentenceIndex].EffectSelectors[selectorIndex].GetSelectorValue(ref unitSnapshot) + " ";
                            }
                            else
                            {
                                newLine += word + " ";
                            }
                        }
                    }

                    result.AppendLine(newLine.Trim());
                }
                else
                {
                    result.AppendLine(clearedSentence);
                }
            }

            return result.ToString();
        }
    }
}