﻿using System;

namespace Kairos.Cards
{
    public class CardGenerationPatternAttribute : Attribute
    {
        public readonly string Pattern;

        public CardGenerationPatternAttribute(string pattern)
        {
            Pattern = pattern;
        }
    }
}