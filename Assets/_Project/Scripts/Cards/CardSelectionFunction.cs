﻿using System;
using Kairos.Controllers;
using Kairos.Timeline;
using Kairos.Units;

namespace Kairos.Cards
{
    public abstract class CardSelectionFunction
    {
        public abstract void GetTargets(CardInfo sourceCard, UnitController sourceUnit, SelectionController selection, BoardController board, TimelineController timeline, int timeframe, Action<bool, UnitController[]> targetsAcquiredCallback);
    }
}