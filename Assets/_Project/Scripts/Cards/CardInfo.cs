﻿using System;
using System.Collections.Generic;
using Kairos.Controllers;
using Kairos.Timeline;
using Kairos.Units;
using TorkFramework.ResourceManagement;
using TorkFramework.Serialization;
using UnityEngine;

namespace Kairos.Cards
{
    public class CardInfo
    {
        private struct CardPlayInfo
        {
            public readonly int SentenceIndex;
            public readonly UnitController[] Units;
        }
        
        public enum CardRarity
        {
            Base,
            Common,
            Rare,
            Legendary,
            Enemy
        }

        public enum Priority
        {
            HeroPassivePriority = 0,
            SentinelPriority = 1,
            Priority2 = 2,
            Priority3 = 3,
            Priority4 = 4,
            HeroPriority = 5,
            Priority6 = 6,
            Priority7 = 7,
            Priority8 = 8,
            EnemyPriority = 9,
            DO_NOT_USE = 10
        }

        [XmlSerialized] public string CardId;
        [XmlSerialized] public string CardName;
        [XmlSerialized] public string CardDescription;
        [XmlSerialized] public string CardGeneratedDescription;
        [XmlSerialized] public int CardSpeed;
        [XmlSerialized] public Priority CardPriority;
        [XmlSerialized] public ResourceReference CardImage;
        [XmlSerialized] public ResourceReference CardIcon;
        [XmlSerialized] public Color CardColor;
        [XmlSerialized] public bool ShineIcon;
        [XmlSerialized] public bool AllowDrop;
        [XmlSerialized] public CardRarity Rarity;
        [XmlSerialized] public List<CardSentence> Sentences;

        public CardInfo()
        {
            CardId = "";
            CardName = "";
            CardDescription = "";
            CardGeneratedDescription = "";
            CardSpeed = 1;
            CardPriority = Priority.HeroPriority;
            CardImage = new ResourceReference();
            CardIcon = new ResourceReference();
            CardColor = Color.white;
            ShineIcon = false;
            AllowDrop = false;
            Rarity = CardRarity.Base;
            Sentences = new List<CardSentence>();
        }

        public CardInfo(CardInfo source)
        {
            CardId = source.CardId;
            CardName = source.CardName;
            CardDescription = source.CardDescription;
            CardGeneratedDescription = source.CardGeneratedDescription;
            CardSpeed = source.CardSpeed;
            CardPriority = source.CardPriority;
            CardImage = source.CardImage;
            CardIcon = source.CardIcon;
            CardColor = source.CardColor;
            ShineIcon = source.ShineIcon;
            AllowDrop = source.AllowDrop;
            Sentences = source.Sentences;
            Rarity = source.Rarity;
        }

        public void Play(TimelineController.TimelineAction action, BoardController board, TimelineController timeline, Action cardPlayedCallback)
        {
            if (Sentences.Count <= 0)
            {
                cardPlayedCallback?.Invoke();
                return;
            }

            List<int> sentecesToExecute = new List<int>();

            int overridesCount = 0;
            
            for (var i = 0; i < Sentences.Count; i++)
            {
                if (Sentences[i].CheckPlayable(action, action.Source, action.Targets[i], timeline, board, out var overrideSentences, out int repetitions))
                {
                    if (overrideSentences && overridesCount <= 0)
                    {
                        sentecesToExecute.Clear();
                        overridesCount++;
                    }

                    for(int r = 0; r < repetitions; r++)
                        sentecesToExecute.Add(i);
                }
            }
            
            ExecuteSentence(0, sentecesToExecute, action, board, timeline, cardPlayedCallback);
        }

        public void PlayOnSnapshot(ref TimelineController.TimelineSnapshot snapshot, TimelineController.TimelineAction action, TimelineController timeline, BoardController board)
        {
            List<int> sentecesToExecute = new List<int>();

            int overridesCount = 0;
            
            for (var i = 0; i < Sentences.Count; i++)
            {
                if (Sentences[i].CheckPlayableOnSnapshot(ref snapshot, action, timeline, out var overrideSentences, out int repetitions))
                {
                    if (overrideSentences && overridesCount <= 0)
                    {
                        sentecesToExecute.Clear();
                        overridesCount++;
                    }

                    for(int r = 0; r < repetitions; r++)
                        sentecesToExecute.Add(i);
                }
            }

            foreach (var sentenceIndex in sentecesToExecute)
            {
                Sentences[sentenceIndex].ExecuteEffectOnSnapshot(ref snapshot, action.Source, action.Targets[sentenceIndex], timeline, board);
            }
        }

        private void ExecuteSentence(int index, List<int> sentencesToExecute, TimelineController.TimelineAction action, BoardController board, TimelineController timeline, Action cardPlayedCallback)
        {
            if (index >= sentencesToExecute.Count)
            {
                cardPlayedCallback?.Invoke();
            }
            else
            {
                Sentences[sentencesToExecute[index]].ExecuteEffect(this, action.Source, action.Targets[sentencesToExecute[index]], timeline, board, () =>
                {
                    ExecuteSentence(index+1, sentencesToExecute, action, board, timeline, cardPlayedCallback);
                });
            }
        }

        public void GetTargets(bool checkSentences, UnitController sourceUnit, SelectionController selection, BoardController board, TimelineController timeline, int timeframe, Action<bool, List<UnitController[]>> targetsAcquiredCallback)
        {
            List<int> sentecesToExecute = new List<int>();

            int overridesCount = 0;
            //var counter = Sentences.Count;
            for (var i = 0; i < Sentences.Count; i++)
            {
                if (checkSentences)
                {
                    if (Sentences[i].CheckPlayable(new TimelineController.TimelineAction(this, sourceUnit, new List<UnitController[]>(), timeframe), sourceUnit, new UnitController[0], timeline, board,
                        out var overrideSentences, out int repetitions))
                    {
                        if (overrideSentences && overridesCount <= 0)
                        {
                            sentecesToExecute.Clear();
                            overridesCount++;
                        }

                        sentecesToExecute.Add(i);
                    }
                }
                else
                {
                    sentecesToExecute.Add(i);
                }
            }
            GetTargetsRecursive(0, sentecesToExecute, sourceUnit, selection, board, timeline, timeframe, new List<UnitController[]>(), targetsAcquiredCallback);
        }

        private void GetTargetsRecursive(int actualIndex, List<int> selectedSentences, UnitController sourceUnit, SelectionController selection, BoardController board, TimelineController timeline, int timeframe, List<UnitController[]> targets,
            Action<bool, List<UnitController[]>> targetsAcquiredCallback)
        {
            if (actualIndex >= Sentences.Count)
            {
                targetsAcquiredCallback?.Invoke(true, targets);
                return;
            }

            if (selectedSentences.Contains(actualIndex))
            {
                Sentences[actualIndex].GetTargets(this, sourceUnit, selection, board, timeline, timeframe, (result, output) =>
                {
                    if (!result)
                    {
                        targetsAcquiredCallback?.Invoke(false, null);
                    }
                    else
                    {
                        targets.Add(output);
                        GetTargetsRecursive(actualIndex + 1, selectedSentences, sourceUnit, selection, board, timeline, timeframe, targets, targetsAcquiredCallback);
                    }
                });
            }
            else
            {
                targets.Add(new UnitController[0]);
                GetTargetsRecursive(actualIndex + 1, selectedSentences, sourceUnit, selection, board, timeline, timeframe, targets, targetsAcquiredCallback);
            }
        }
    }
}