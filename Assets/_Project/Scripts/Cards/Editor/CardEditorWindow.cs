﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Kairos.UI.Cards;
using Kairos.Units;
using Kairos.Utility.Editor;
using TorkFramework.Editor.Utility;
using TorkFramework.Utility;
using TorkFramework.VFX.Animations;
using UnityEditor;
using UnityEngine;

namespace Kairos.Cards.Editor
{
    public class CardEditorWindow : EditorWindow
    {
        private string m_SelectedCardPath;
        private CardInfo m_SelectedCard;
        private Vector2 m_ScrollPosition;
        private List<bool> m_FoldSentences = new List<bool>();
        private Texture2D m_CardPreviewTexture;

        [MenuItem("Custom/Cards/Reload")]
        public static void ReloadCardDatabase()
        {
            CardDatabase.Reload();
        }

        [MenuItem("Custom/Cards/New Card %#c")]
        public static void CreateNewCard()
        {
            var newCard = CardHelper.GenerateNewCard();
            CardHelper.SaveCard(newCard, Globals._CARDS_FOLDER + "newCard.card");
        }

        [MenuItem("Custom/Cards/Editor")]
        public static void Init()
        {
            var window = GetWindow<CardEditorWindow>();
            window.name = "Card Editor";
            window.titleContent = new GUIContent("Card Editor");
            window.Show();
        }

        private void OnEnable()
        {
            OnSelectionChange();
        }

        private void OnSelectionChange()
        {
            if (UnityEditor.Selection.objects.Length == 1 && Path.GetExtension(AssetDatabase.GetAssetPath(UnityEditor.Selection.objects[0])).ToLower() == ".card")
            {
                m_SelectedCardPath = Application.dataPath.Substring(0, Application.dataPath.Length - 6) + AssetDatabase.GetAssetPath(UnityEditor.Selection.objects[0]);
                m_SelectedCard = CardHelper.LoadCard(m_SelectedCardPath);
                UpdatePreview();
            }

            Repaint();
        }

        private void OnGUI()
        {
            if(m_SelectedCard == null)
                return;
            
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.BeginVertical(new GUIStyle {fixedWidth = position.width / 2});
            m_ScrollPosition = EditorGUILayout.BeginScrollView(m_ScrollPosition);
            DrawFields();
            DrawSentences();
            EditorGUILayout.EndScrollView();
            EditorGUILayout.EndVertical();
            DrawPreview();
            EditorGUILayout.EndHorizontal();
        }

        private void DrawFields()
        {
            dynamic swapValue = null;
            
            EditorGUI.BeginDisabledGroup(true);
            EditorGUILayout.TextField("ID", m_SelectedCard.CardId);
            EditorGUI.EndDisabledGroup();

            swapValue = FieldUtils.AdaptiveField("Name", m_SelectedCard.CardName);
            if (swapValue != m_SelectedCard.CardName)
            {
                m_SelectedCard.CardName = swapValue;
                UpdatePreview();
            }
            
            EditorGUILayout.LabelField("Description");
            swapValue = EditorGUILayout.TextArea(m_SelectedCard.CardDescription);
            if (swapValue != m_SelectedCard.CardDescription)
            {
                m_SelectedCard.CardDescription = swapValue;
                UpdatePreview();
            }
            
            EditorGUILayout.LabelField("Generated Description");
            swapValue = EditorGUILayout.TextArea(m_SelectedCard.CardGeneratedDescription);
            if (swapValue != m_SelectedCard.CardGeneratedDescription)
            {
                m_SelectedCard.CardGeneratedDescription = swapValue;
                UpdatePreview();
            }
            
            swapValue = FieldUtils.AdaptiveField("Speed", m_SelectedCard.CardSpeed);
            if (swapValue != m_SelectedCard.CardSpeed)
            {
                m_SelectedCard.CardSpeed = swapValue;
                UpdatePreview();
            }
            
            swapValue = FieldUtils.AdaptiveField("Priority", m_SelectedCard.CardPriority);
            if (swapValue != m_SelectedCard.CardPriority)
            {
                m_SelectedCard.CardPriority = swapValue;
                UpdatePreview();
            }
            
            swapValue = FieldUtils.AdaptiveField("Rarity", m_SelectedCard.Rarity);
            if (swapValue != m_SelectedCard.Rarity)
            {
                m_SelectedCard.Rarity = swapValue;
                UpdatePreview();
            }
            
            swapValue = FieldUtils.AdaptiveField("Image", m_SelectedCard.CardImage);
            if (swapValue != m_SelectedCard.CardImage)
            {
                m_SelectedCard.CardImage = swapValue;
                UpdatePreview();
            }
            
            swapValue = FieldUtils.AdaptiveField("Icon", m_SelectedCard.CardIcon);
            if (swapValue != m_SelectedCard.CardIcon)
            {
                m_SelectedCard.CardIcon = swapValue;
                UpdatePreview();
            }
            
            swapValue = FieldUtils.AdaptiveField("Color", m_SelectedCard.CardColor);
            if (swapValue != m_SelectedCard.CardColor)
            {
                m_SelectedCard.CardColor = swapValue;
                UpdatePreview();
            }
            
            swapValue = FieldUtils.AdaptiveField("Shine Icon", m_SelectedCard.ShineIcon);
            if (swapValue != m_SelectedCard.ShineIcon)
            {
                m_SelectedCard.ShineIcon = swapValue;
                UpdatePreview();
            }
            
            swapValue = FieldUtils.AdaptiveField("Allow Drop", m_SelectedCard.AllowDrop);
            if (swapValue != m_SelectedCard.AllowDrop)
            {
                m_SelectedCard.AllowDrop = swapValue;
                UpdatePreview();
            }
            
            EditorGUILayout.BeginHorizontal();

            var oldColor = GUI.backgroundColor;
            GUI.backgroundColor = Color.yellow;

            if (GUILayout.Button("Generate"))
            {
                GenerateCardSentences();
                UpdatePreview();
                Repaint();
            }

            GUI.backgroundColor = Color.green;

            if (GUILayout.Button("Save"))
            {
                CardHelper.SaveCard(m_SelectedCard, m_SelectedCardPath);
                Repaint();
            }

            GUI.backgroundColor = oldColor;

            EditorGUILayout.EndHorizontal();
        }
        
        private void DrawSentences()
        {
            while (m_FoldSentences.Count > m_SelectedCard.Sentences.Count)
            {
                m_FoldSentences.RemoveAt(m_FoldSentences.Count - 1);
            }

            while (m_FoldSentences.Count < m_SelectedCard.Sentences.Count)
            {
                m_FoldSentences.Add(false);
            }

            for (var sentenceIndex = 0; sentenceIndex < m_SelectedCard.Sentences.Count; sentenceIndex++)
            {
                var style = new GUIStyle(EditorStyles.foldoutHeader);
                style.fixedWidth = position.width / 2;

                m_FoldSentences[sentenceIndex] = EditorGUILayout.Foldout(m_FoldSentences[sentenceIndex], "Sentence " + sentenceIndex , style);
                if (m_FoldSentences[sentenceIndex])
                {
                    EditorGUI.indentLevel++;
                    //Draw Selections
                    EditorGUILayout.LabelField("Selection Functions", EditorStyles.boldLabel);
                    for (int selectionIndex = 0; selectionIndex < m_SelectedCard.Sentences[sentenceIndex].Selections.Count; selectionIndex++)
                    {
                        DrawSingleSelectionFunction(sentenceIndex, selectionIndex);
                    }

                    if (GUILayout.Button("Add Selection"))
                    {
                        var menu = new GenericMenu();
                        var types = TypeUtils.GetDerivedTypes(typeof(CardSelectionFunction));
                        
                        foreach (var type in types)
                        {
                            var targetIndex = sentenceIndex;
                            menu.AddItem(new GUIContent(type.Name), false, () => { OnAddSelectionMenuClicked(type, targetIndex);} );
                        }
                        
                        menu.ShowAsContext();
                    }
                    
                    //Draw Selectors
                    EditorGUILayout.LabelField("Selectors", EditorStyles.boldLabel);
                    for (int selectorIndex = 0; selectorIndex < m_SelectedCard.Sentences[sentenceIndex].EffectSelectors.Count; selectorIndex++)
                    {
                        DrawSingleEffectSelector(sentenceIndex, selectorIndex);
                    }

                    if (GUILayout.Button("Add Selector"))
                    {
                        var menu = new GenericMenu();
                        var types = TypeUtils.GetDerivedTypes(typeof(CardEffectSelector));
                        
                        foreach (var type in types)
                        {
                            var targetIndex = sentenceIndex;
                            menu.AddItem(new GUIContent(type.Name), false, () => { OnAddSelectorMenuClicked(type, targetIndex);} );
                        }
                        
                        menu.ShowAsContext();
                    }

                    //Draw Effects
                    EditorGUILayout.LabelField("Effects", EditorStyles.boldLabel);

                    for (int effectIndex = 0; effectIndex < m_SelectedCard.Sentences[sentenceIndex].Effects.Count; effectIndex++)
                    {
                        DrawSingleEffect(sentenceIndex, effectIndex);
                    }
                    
                    if (GUILayout.Button("Add Effect"))
                    {
                        var menu = new GenericMenu();
                        var types = TypeUtils.GetDerivedTypes(typeof(CardEffect));
                        foreach (var type in types)
                        {
                            var targetIndex = sentenceIndex;
                            menu.AddItem(new GUIContent(type.Name), false, () => { OnAddEffectMenuClicked(type, targetIndex); });
                        }

                        menu.ShowAsContext();
                    }

                    EditorGUI.indentLevel--;
                }
            }
        }

        /// <summary>
        /// Draw the image and frame preview of the card
        /// </summary>
        private void DrawPreview()
        {
            EditorGUILayout.BeginVertical(new GUIStyle {fixedWidth = position.width / 2});
            if (m_SelectedCard != null)
            {
                if (m_CardPreviewTexture == null)
                    UpdatePreview();

                var frameImage = m_CardPreviewTexture;

                float aspect = (float) frameImage.width / (float) frameImage.height;

                float width = position.width / 2;
                float height = position.height;

                bool useHeight = width / height > aspect;

                float calculateWidth = 0f;
                float calculatedHeight = 0f;

                if (useHeight)
                {
                    calculatedHeight = height;
                    calculateWidth = aspect * height;
                }
                else
                {
                    calculateWidth = width;
                    calculatedHeight = width / aspect;
                }

                var controlRect = EditorGUILayout.GetControlRect();

                EditorGUI.DrawPreviewTexture(new Rect(controlRect.position.x, controlRect.position.y, calculateWidth, calculatedHeight), frameImage);
            }

            EditorGUILayout.EndVertical();
        }

        private void OnAddSelectionMenuClicked(Type targetType, int selectedSelectionEffect)
        {
            m_SelectedCard.Sentences[selectedSelectionEffect].Selections.Add(Activator.CreateInstance(targetType) as CardSelectionFunction);
            Repaint();
        }

        private void OnAddSelectorMenuClicked(Type targetType, int selectedSelector)
        {
            m_SelectedCard.Sentences[selectedSelector].EffectSelectors.Add(Activator.CreateInstance(targetType) as CardEffectSelector);
            Repaint();
        }

        private void OnAddEffectMenuClicked(Type targetType, int selectedSelectionEffect)
        {
            m_SelectedCard.Sentences[selectedSelectionEffect].Effects.Add(Activator.CreateInstance(targetType) as CardEffect);
            Repaint();
        }

        private void OnAddVFXMenuClicked(Type targetType, CardEffect effect)
        {
            Array.Resize(ref effect.EffectVFXs, effect.EffectVFXs.Length + 1);
            effect.EffectVFXs[effect.EffectVFXs.Length - 1] = Activator.CreateInstance(targetType) as VFXAnimation;
            Repaint();
        }

        private void DrawSingleSelectionFunction(int sentenceIndex, int selectionIndex)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(m_SelectedCard.Sentences[sentenceIndex].Selections[selectionIndex].GetType().Name);
            var oldColor = GUI.backgroundColor;
            GUI.backgroundColor = Color.red;
            
            if (GUILayout.Button("X"))
            {
                m_SelectedCard.Sentences[sentenceIndex].Selections.RemoveAt(selectionIndex);
                Repaint();
            }
            
            GUI.backgroundColor = oldColor;
            EditorGUILayout.EndHorizontal();

            var selection = m_SelectedCard.Sentences[sentenceIndex].Selections[selectionIndex];

            try
            {
                var result = FieldUtils.AdaptiveField("", selection);
                m_SelectedCard.Sentences[sentenceIndex].Selections[selectionIndex] = (CardSelectionFunction) result;
            }
            catch (Exception ex)
            {
                Debug.LogError(ex.Message);
            }
        }
        
        private void DrawSingleEffectSelector(int sentenceIndex, int selectorIndex)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(m_SelectedCard.Sentences[sentenceIndex].EffectSelectors[selectorIndex].GetType().Name);
            var oldColor = GUI.backgroundColor;
            GUI.backgroundColor = Color.red;
            
            if (GUILayout.Button("X"))
            {
                m_SelectedCard.Sentences[sentenceIndex].EffectSelectors.RemoveAt(selectorIndex);
                Repaint();
            }
            
            GUI.backgroundColor = oldColor;
            EditorGUILayout.EndHorizontal();

            var selector = m_SelectedCard.Sentences[sentenceIndex].EffectSelectors[selectorIndex];

            try
            {
                var result = FieldUtils.AdaptiveField("", selector);
                m_SelectedCard.Sentences[sentenceIndex].EffectSelectors[selectorIndex] = (CardEffectSelector) result;
            }
            catch (Exception ex)
            {
                Debug.LogError(ex.Message);
            }
        }
        
        private void DrawSingleEffect(int sentenceIndex, int effectIndex)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(m_SelectedCard.Sentences[sentenceIndex].Effects[effectIndex].GetType().Name);
            var oldColor = GUI.backgroundColor;
            GUI.backgroundColor = Color.red;
            if (GUILayout.Button("X"))
            {
                m_SelectedCard.Sentences[sentenceIndex].Effects.RemoveAt(effectIndex);
            }

            GUI.backgroundColor = oldColor;

            EditorGUILayout.EndHorizontal();

            var effect = m_SelectedCard.Sentences[sentenceIndex].Effects[effectIndex];

            try
            {
                var result = FieldUtils.AdaptiveField("", effect);
                m_SelectedCard.Sentences[sentenceIndex].Effects[effectIndex] = (CardEffect) result;
                EditorGUILayout.LabelField("VFX", EditorStyles.helpBox);
            }
            catch (Exception ex)
            {
                Debug.LogError(ex.Message);
            }
            
            EditorGUI.indentLevel++;

            if(m_SelectedCard.Sentences[sentenceIndex].Effects[effectIndex].EffectVFXs == null)
                m_SelectedCard.Sentences[sentenceIndex].Effects[effectIndex].EffectVFXs = new VFXAnimation[0];
            
            var vfxArray = m_SelectedCard.Sentences[sentenceIndex].Effects[effectIndex].EffectVFXs;

            Stack<int> vfxToRemove = new Stack<int>();

            for (int vfxIndex = 0; vfxIndex < vfxArray.Length; vfxIndex++)
            {
                var vfxType = vfxArray[vfxIndex].GetType();
                
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField(vfxType.Name, EditorStyles.boldLabel);
                oldColor = GUI.backgroundColor;
                GUI.backgroundColor = Color.red;
                if (GUILayout.Button("X"))
                {
                    vfxToRemove.Push(vfxIndex);
                }

                GUI.backgroundColor = oldColor;
                EditorGUILayout.EndHorizontal();
                EditorGUI.indentLevel++;

                vfxArray[vfxIndex] = FieldUtils.AdaptiveField("Effect " + vfxIndex, (dynamic) vfxArray[vfxIndex]);
                
                EditorGUI.indentLevel--;
            }
            
            if (GUILayout.Button("Add VFX"))
            {
                var menu = new GenericMenu();
                var types = TypeUtils.GetDerivedTypes(typeof(VFXAnimation));
                foreach (var type in types)
                {
                    var targetEffect = m_SelectedCard.Sentences[sentenceIndex].Effects[effectIndex];
                    menu.AddItem(new GUIContent(type.Name), false, () => { OnAddVFXMenuClicked(type, targetEffect); });
                }

                menu.ShowAsContext();
            }

            if (vfxToRemove.Count > 0)
            {
                var vfxList = vfxArray.ToList();

                while (vfxToRemove.Count > 0)
                {
                    vfxList.RemoveAt(vfxToRemove.Pop());
                }

                m_SelectedCard.Sentences[sentenceIndex].Effects[effectIndex].EffectVFXs = vfxList.ToArray();
            }

            EditorGUI.indentLevel--;
        }

        /// <summary>
        /// Try to generate the card selection effect, based on the description
        /// </summary>
        private void GenerateCardSentences()
        {
            if (m_SelectedCard.Sentences != null && m_SelectedCard.Sentences.Count > 0)
            {
                m_SelectedCard.Sentences.Clear();
            }

            if (!string.IsNullOrEmpty(m_SelectedCard.CardDescription) && !string.IsNullOrWhiteSpace(m_SelectedCard.CardDescription))
            {
                var possibleSelections = TypeUtils.GetDerivedTypes(typeof(CardSelectionFunction)).Where(prop => prop.IsDefined(typeof(CardGenerationPatternAttribute)));
                var possibleEffects = TypeUtils.GetDerivedTypes(typeof(CardEffect)).Where(prop => prop.IsDefined(typeof(CardGenerationPatternAttribute)));
                var possibleSelectors = TypeUtils.GetDerivedTypes(typeof(CardEffectSelector)).Where(prop => prop.IsDefined(typeof(CardGenerationPatternAttribute)));

                StringBuilder generatedSentenceBuilder = new StringBuilder();

                var lowerText = m_SelectedCard.CardDescription.ToLower();
                var splitSentences = lowerText.Replace(".", " . ").Replace(",", " , ").Replace("\n", " ").Split('.');
                foreach (var sentence in splitSentences)
                {
                    bool addedFunction = false;
                    var splitWords = sentence.Split(' ');
                    var newline = "";

                    int matchedCounter = 0;

                    for (int i = 0; i < splitWords.Length; i++)
                    {
                        #region Check for card effects

                        foreach (var effect in possibleEffects)
                        {
                            var effectAttribute = effect.GetCustomAttribute<CardGenerationPatternAttribute>();

                            var splitPattern = effectAttribute.Pattern.Split(' ');

                            if (splitPattern.Length <= splitWords.Length - i)
                            {
                                var matched = true;
                                for (int j = 0; j < splitPattern.Length && j + i < splitWords.Length; j++)
                                {
                                    if (splitPattern[j] != splitWords[i + j] && splitPattern[j][0] != '<')
                                    {
                                        matched = false;
                                        break;
                                    }
                                }

                                if (matched)
                                {
                                    if (!addedFunction)
                                    {
                                        m_SelectedCard.Sentences.Add(new CardSentence());
                                        addedFunction = true;
                                    }

                                    var newEffect = Activator.CreateInstance(effect) as CardEffect;
                                    m_SelectedCard.Sentences[m_SelectedCard.Sentences.Count - 1].Effects.Add(newEffect);
                                    for (int j = 0; j < splitPattern.Length; j++)
                                    {
                                        if (splitPattern[j][0] == '<')
                                        {
                                            ApplyValue(newEffect, splitPattern[j], splitWords[i + j]);
                                        }
                                    }

                                    matchedCounter = 0;

                                    foreach (var patternWord in splitPattern)
                                    {
                                        if (patternWord[0] == '<')
                                        {
                                            newline += "#" + (m_SelectedCard.Sentences.Count - 1) + "-" +
                                                       (m_SelectedCard.Sentences[m_SelectedCard.Sentences.Count - 1].Effects.Count - 1) + " ";
                                        }
                                        else
                                        {
                                            newline += patternWord + " ";
                                        }

                                        matchedCounter++;
                                    }
                                }
                            }
                        }

                        #endregion
                        
                        #region Check for card selectors

                        foreach (var selector in possibleSelectors)
                        {
                            var selectorAttribute = selector.GetCustomAttribute<CardGenerationPatternAttribute>();

                            var splitPattern = selectorAttribute.Pattern.Split(' ');

                            if (splitPattern.Length <= splitWords.Length - i)
                            {
                                var matched = true;
                                for (int j = 0; j < splitPattern.Length && j + i < splitWords.Length; j++)
                                {
                                    if (splitPattern[j] != splitWords[i + j] && splitPattern[j][0] != '<')
                                    {
                                        matched = false;
                                        break;
                                    }
                                }

                                if (matched)
                                {
                                    if (!addedFunction)
                                    {
                                        m_SelectedCard.Sentences.Add(new CardSentence());
                                        addedFunction = true;
                                    }

                                    var newSelector = Activator.CreateInstance(selector) as CardEffectSelector;
                                    m_SelectedCard.Sentences[m_SelectedCard.Sentences.Count - 1].EffectSelectors.Add(newSelector);
                                    for (int j = 0; j < splitPattern.Length; j++)
                                    {
                                        if (splitPattern[j][0] == '<')
                                        {
                                            ApplyValue(newSelector, splitPattern[j], splitWords[i + j]);
                                        }
                                    }

                                    matchedCounter = 0;

                                    foreach (var patternWord in splitPattern)
                                    {
                                        if (patternWord[0] == '<')
                                        {
                                            newline += "@" + (m_SelectedCard.Sentences.Count - 1) + "-" +
                                                       (m_SelectedCard.Sentences[m_SelectedCard.Sentences.Count - 1].EffectSelectors.Count - 1) + " ";
                                        }
                                        else
                                        {
                                            newline += patternWord + " ";
                                        }

                                        matchedCounter++;
                                    }
                                }
                            }
                        }

                        #endregion

                        #region Check for selection functions

                        foreach (var function in possibleSelections)
                        {
                            var functionAttribute = function.GetCustomAttribute<CardGenerationPatternAttribute>();

                            var splitPattern = functionAttribute.Pattern.Split(' ');

                            if (splitPattern.Length <= splitWords.Length - i)
                            {
                                var matched = true;
                                for (int j = 0; j < splitPattern.Length && j + i < splitWords.Length; j++)
                                {
                                    if (splitPattern[j] != splitWords[i + j] && splitPattern[j][0] != '<')
                                    {
                                        matched = false;
                                        break;
                                    }
                                }

                                if (matched)
                                {
                                    if (!addedFunction)
                                    {
                                        m_SelectedCard.Sentences.Add(new CardSentence());
                                        addedFunction = true;
                                    }

                                    var newFunction = Activator.CreateInstance(function) as CardSelectionFunction;
                                    m_SelectedCard.Sentences[m_SelectedCard.Sentences.Count - 1].Selections.Add(newFunction);
                                    for (int j = 0; j < splitPattern.Length; j++)
                                    {
                                        if (splitPattern[j][0] == '<')
                                        {
                                            ApplyValue(newFunction, splitPattern[j], splitWords[i + j]);
                                        }
                                    }
                                }
                            }
                        }

                        #endregion

                        if (matchedCounter <= 0)
                        {
                            newline += splitWords[i] + " ";
                        }

                        matchedCounter--;
                    }

                    newline = newline.Trim();
                    newline += ".";

                    generatedSentenceBuilder.AppendLine(newline);
                }

                m_SelectedCard.CardGeneratedDescription = generatedSentenceBuilder.ToString();
            }
        }

        /// <summary>
        /// Set a value from the description to an effect or a selection function
        /// </summary>
        /// <typeparam name="T"></typeparam>
        private void ApplyValue(object target, string parameter, string descriptionValue)
        {
            var valueType = parameter.Replace("<", "").Replace(">", "").Split(':')[0];
            var valueName = parameter.Replace("<", "").Replace(">", "").Split(':')[1];

            switch (valueType)
            {
                //int
                case "%i":
                {
                    var value = int.Parse(descriptionValue);
                    var field = target.GetType().GetField(valueName);
                    field.SetValue(target, value);
                    break;
                }
                //target hero or enemy
                case "%t":
                {
                    Team value = Team.Player;
                    switch (descriptionValue)
                    {
                        case "hero":
                        {
                            value = Team.Player;
                            break;
                        }
                        case "heroes":
                        {
                            value = Team.Player;
                            break;
                        }
                        case "enemy":
                        {
                            value = Team.Enemy;
                            break;
                        }
                        case "enemies":
                        {
                            value = Team.Enemy;
                            break;
                        }
                    }

                    var field = target.GetType().GetField(valueName);
                    field.SetValue(target, value);
                    break;
                }
            }
        }

        /// <summary>
        /// Update the card visualization preview in the editor
        /// </summary>
        private void UpdatePreview()
        {
            var parent = new GameObject("Preview");
            var canvas = new GameObject("PreviewCanvas").AddComponent<Canvas>();
            canvas.transform.SetParent(parent.transform, false);

            var card = Instantiate(GlobalEditorSettings.Instance.CardPreview);
            card.transform.SetParent(canvas.transform, false);
            card.GetComponent<UICardView>().ReferencedCard = m_SelectedCard;
            (card.transform as RectTransform).anchorMin = new Vector2(0.5f, .5f);
            (card.transform as RectTransform).anchorMax = new Vector2(0.5f, .5f);
            (card.transform as RectTransform).pivot = new Vector2(0.5f, .5f);
            (card.transform as RectTransform).anchoredPosition = Vector2.zero;

            var camera = new GameObject("PreviewCamera").AddComponent<Camera>();
            camera.transform.SetParent(parent.transform, false);
            camera.cullingMask = LayerMask.GetMask("Editor");
            camera.clearFlags = CameraClearFlags.Depth;
            camera.backgroundColor = Color.clear;

            canvas.renderMode = RenderMode.WorldSpace;
            canvas.transform.position = new Vector3(0, 0, 1);
            camera.orthographic = true;
            camera.orthographicSize = 290f;

            canvas.gameObject.layer = LayerMask.NameToLayer("Editor");
            card.gameObject.layer = LayerMask.NameToLayer("Editor");
            foreach (Transform child in card.transform)
            {
                child.gameObject.layer = LayerMask.NameToLayer("Editor");
            }

            RenderTexture targetTexture = new RenderTexture(418, 576, 32);
            camera.targetTexture = targetTexture;

            camera.Render();

            Canvas.ForceUpdateCanvases();

            camera.Render();

            RenderTexture.active = targetTexture;

            m_CardPreviewTexture = new Texture2D(418, 576, TextureFormat.RGBA32, false);
            m_CardPreviewTexture.ReadPixels(new Rect(0, 0, targetTexture.width, targetTexture.height), 0, 0);
            m_CardPreviewTexture.Apply();

            RenderTexture.active = null;

            targetTexture.Release();

            DestroyImmediate(parent);
        }

    }
}