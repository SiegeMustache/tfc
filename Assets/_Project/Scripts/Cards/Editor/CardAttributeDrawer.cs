﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Kairos.Cards.Editor
{
    [CustomPropertyDrawer(typeof(CardAttribute))]
    public class CardAttributeDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var cardIds = CardDatabase.GetIdList();

            var actualId = Array.IndexOf(cardIds, property.stringValue);
            if (actualId < 0)
                actualId = cardIds.Length;
            
            var cardNames = new List<string>();
            foreach (var card in cardIds)
            {
                cardNames.Add(CardDatabase.LoadedCards[card].CardName);
            }
            
            EditorGUI.LabelField(new Rect(position.x, position.y, 100, position.height), label);
            var newId = EditorGUI.Popup(new Rect(position.x + 100, position.y, position.width-100, position.height), actualId, cardNames.ToArray());


            if (newId != actualId)
                property.stringValue = newId < cardIds.Length ? cardIds[newId] : "";
        }

        public static string CardField(string label, string value)
        {
            var cardIds = CardDatabase.GetIdList();

            var actualId = Array.IndexOf(cardIds, value);
            if (actualId < 0)
                actualId = cardIds.Length;
            
            var cardNames = new List<string>();
            foreach (var card in cardIds)
            {
                cardNames.Add(CardDatabase.LoadedCards[card].CardName);
            }

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(label, GUILayout.ExpandWidth(false));
            var newId = EditorGUILayout.Popup(actualId, cardNames.ToArray());
            EditorGUILayout.EndHorizontal();

            if (newId != actualId)
                return newId < cardIds.Length ? cardIds[newId] : "";

            return value;
        }
    }
}