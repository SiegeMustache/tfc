﻿using System;
using System.Collections.Generic;
using System.Linq;
using Kairos.Controllers;
using Kairos.Timeline;
using Kairos.Units;
using TorkFramework.Serialization;
using TorkFramework.VFX;
using UnityEngine;

namespace Kairos.Cards.Effects
{
    [CardGenerationPattern("haste <%i:HasteAmount>")]
    public class HasteEffect : CardEffect
    {
        [XmlSerialized] public int HasteAmount = 0;
        [XmlSerialized] public int Turns = 1;
        
        public override void ExecuteEffect(CardInfo sourceCard, UnitController sourceUnit, UnitController[] targets, TimelineController timeline, Action effectExecutedCallback)
        {
            var remainingTargetsCount = targets?.Length ?? 0;
            if (remainingTargetsCount > 0)
            {
                for (int i = 0; i < targets.Length; i++)
                {
                    var targetIndex = i;
                    if (VFXPrefab?.Resource == null)
                    {
                        targets[targetIndex].AddSpeedModifier(sourceUnit, -HasteAmount, Turns, () =>
                        {
                            remainingTargetsCount--;
                            if (remainingTargetsCount <= 0)
                            {
                                effectExecutedCallback?.Invoke();
                            }
                        });
                    }
                    else
                    {
                        sourceUnit.Entity.GameManager.GetManager<VFXManager>().PlayVFX(
                            (VFXPrefab.Resource as GameObject).GetComponent<VFXEntity>(),
                            EffectVFXs.ToList(),
                            sourceUnit.transform,
                            targets[targetIndex].transform,
                            () =>
                            {
                                targets[targetIndex].AddSpeedModifier(sourceUnit, -HasteAmount, Turns, () =>
                                {
                                    remainingTargetsCount--;
                                    if (remainingTargetsCount <= 0)
                                    {
                                        effectExecutedCallback?.Invoke();
                                    }
                                });
                            });
                    }
                }
            }
            else
            {
                effectExecutedCallback?.Invoke();
            }
        }
        
        public override void ExecuteEffectOnSnapshot(ref TimelineController.TimelineSnapshot snapshot, UnitController sourceUnit, UnitController[] targets, TimelineController timeline, BoardController board)
        {
            if (targets != null && targets.Length > 0)
            {
                if (targets[0].Team == Team.Player)
                {
                    snapshot.HeroSnapshot.AddSpeedModifier(-HasteAmount, Turns);
                }
                else
                {
                    var indexes = new List<int>();
                    foreach (var target in targets)
                    {
                        indexes.Add(Array.IndexOf(board.EnemyUnits, target));
                    }

                    foreach (var index in indexes)
                    {
                        snapshot.EnemiesSnapshots[index].AddSpeedModifier(-HasteAmount, Turns);
                    }
                }
            }
        }

        public override string GetEffectValue(TimelineController.TimelineUnitSnapshot? unitSnapshot)
        {
            return HasteAmount.ToString();
        }
    }
}