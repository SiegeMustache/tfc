﻿using Kairos.Controllers;
using Kairos.Timeline;
using Kairos.Units;

namespace Kairos.Cards
{
    public abstract class CardEffectSelector
    {
        public abstract bool CheckEffect(TimelineController.TimelineAction action, UnitController source, UnitController[] targets, TimelineController timeline, BoardController board, out bool overrideEffects, out int repetitions);
        public abstract bool CheckEffectOnSnapshot(ref TimelineController.TimelineSnapshot snapshot, TimelineController.TimelineAction action, TimelineController timeline, out bool overrideEffects, out int repetitions);
        public abstract string GetSelectorValue(ref TimelineController.TimelineUnitSnapshot? snapsho);
    }
}