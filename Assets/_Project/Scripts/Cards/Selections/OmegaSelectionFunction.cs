﻿using System;
using Kairos.Controllers;
using Kairos.Timeline;
using Kairos.Units;

namespace Kairos.Cards.Selection
{
    [CardGenerationPattern("omega")]
    public class OmegaSelectionFunction : CardSelectionFunction
    {
        public override void GetTargets(CardInfo sourceCard, UnitController sourceUnit, SelectionController selection, BoardController board, TimelineController timeline, int timeframe, Action<bool, UnitController[]> targetsAcquiredCallback)
        {
            sourceUnit.DisableCardPlay = true;
            targetsAcquiredCallback?.Invoke(true, null);
        }
    }
}