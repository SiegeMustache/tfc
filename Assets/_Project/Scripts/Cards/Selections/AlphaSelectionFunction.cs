﻿using System;
using Kairos.Controllers;
using Kairos.Timeline;
using Kairos.Units;

namespace Kairos.Cards.Selection
{
    [CardGenerationPattern("alpha-")]
    public class AlphaSelectionFunction : CardSelectionFunction
    {
        public override void GetTargets(CardInfo sourceCard, UnitController sourceUnit, SelectionController selection, BoardController board, TimelineController timeline, int timeframe, Action<bool, UnitController[]> targetsAcquiredCallback)
        {
            if(timeline.GetFirstPlayableTimeframe(sourceUnit) == 0)
                targetsAcquiredCallback?.Invoke(true, null);
            else
                targetsAcquiredCallback?.Invoke(false, null);
        }
    }
}