﻿using System;
using Kairos.Controllers;
using Kairos.Timeline;
using Kairos.Units;

namespace Kairos.Cards.Selection
{
    [CardGenerationPattern("to the hero")]
    public class HeroSelectionFunction : CardSelectionFunction
    {
        public override void GetTargets(CardInfo sourceCard, UnitController sourceUnit, SelectionController selection, BoardController board, TimelineController timeline, int timeframe, Action<bool, UnitController[]> targetsAcquiredCallback)
        {
            targetsAcquiredCallback?.Invoke(true, new[] {board.HeroUnit});
        }
    }
}