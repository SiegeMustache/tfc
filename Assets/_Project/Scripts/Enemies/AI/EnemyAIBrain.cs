﻿using System;
using System.Collections.Generic;
using System.Linq;
using TorkFramework.Serialization;

namespace Kairos.Enemies.AI
{
    public class EnemyAIBrain
    {
        [XmlSerialized] private EnemyAIBox[] m_Boxes;
        [XmlSerialized] private EnemyAITransition[] m_Transitions;

        private int m_ActualBox = 0;
        private Stack<int> m_SequenceStack;

        public EnemyAIBox[] Boxes => m_Boxes;
        public EnemyAITransition[] Transitions => m_Transitions;

        public EnemyAIBrain()
        {
            m_ActualBox = 0;
            m_SequenceStack = new Stack<int>();
            m_Boxes = new EnemyAIBox[0];
            m_Transitions = new EnemyAITransition[0];
        }

        public void AddBox()
        {
            Array.Resize(ref m_Boxes, m_Boxes.Length + 1);
            m_Boxes[m_Boxes.Length - 1] = new EnemyAIBox();
        }

        public void RemoveBox(int index)
        {
            var transitionsList = new List<EnemyAITransition>();
            foreach (var transition in Transitions)
            {
                if (transition.Source != index && transition.Target != index)
                {
                    var newSource = transition.Source > index ? transition.Source - 1 : transition.Source;
                    var newTarget = transition.Target > index ? transition.Target - 1 : transition.Target;
                    transitionsList.Add(new EnemyAITransition(newSource, newTarget) {Condition = transition.Condition, Priority = transition.Priority});
                }
            }

            m_Transitions = transitionsList.ToArray();

            var boxesList = new List<EnemyAIBox>();
            for (var i = 0; i < Boxes.Length; i++)
            {
                if (i != index)
                    boxesList.Add(Boxes[i]);
            }

            m_Boxes = boxesList.ToArray();
        }

        public void AddTransition()
        {
            Array.Resize(ref m_Transitions, m_Transitions.Length + 1);
            m_Transitions[m_Transitions.Length - 1] = new EnemyAITransition();
        }

        public void RemoveTransition(int source, int target)
        {
            for (var i = 0; i < m_Transitions.Length; i++)
            {
                if (m_Transitions[i].Source == source && m_Transitions[i].Target == target)
                {
                    RemoveTransition(i);
                    return;
                }
            }
        }

        public void RemoveTransition(int transitionID)
        {
            var transitionsList = m_Transitions.ToList();
            transitionsList.RemoveAt(transitionID);
            m_Transitions = transitionsList.ToArray();
        }

        public EnemyAITurnAction Tick(EnemyController enemy)
        {
            CheckUpdateTransitions(enemy);

            var result = Boxes[m_ActualBox].GetTurn(out bool pop);
            if (pop)
                Pop();

            return result;
        }

        private void CheckUpdateTransitions(EnemyController enemy)
        {
            bool checkedRecursive = false;

            while (!checkedRecursive)
            {
                checkedRecursive = true;
                var possibleTransitions = new List<EnemyAITransition>();

                foreach (var transition in Transitions)
                {
                    if (transition.Source == m_ActualBox)
                    {
                        possibleTransitions.Add(transition);
                    }
                }

                foreach (var transition in TorkFramework.Utility.ListUtils.Sort(possibleTransitions))
                {
                    if (transition.Condition.CheckCondition(enemy))
                    {
                        checkedRecursive = false;
                        EnterBox(transition.Target);
                        break;
                    }
                }
            }
        }

        private void EnterBox(int boxIndex)
        {
            m_SequenceStack.Push(m_ActualBox);
            m_ActualBox = boxIndex;
        }

        private void Pop()
        {
            m_ActualBox = m_SequenceStack.Pop();
        }
    }
}