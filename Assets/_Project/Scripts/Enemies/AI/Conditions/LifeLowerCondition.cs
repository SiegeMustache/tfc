﻿using TorkFramework.Serialization;

namespace Kairos.Enemies.AI.Conditions
{
    public class LifeLowerCondition : EnemyAITransitionCondition
    {
        [XmlSerialized] public int TargetLife;
        public override bool CheckCondition(EnemyController sourceEnemy)
        {
            if (sourceEnemy.UnitController.ReferencedUnit.ActualLifePoints <= TargetLife)
                return true;
            return false;
        }
    }
}