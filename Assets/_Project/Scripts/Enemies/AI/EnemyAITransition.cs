﻿using System;
using TorkFramework.Serialization;

namespace Kairos.Enemies.AI
{
    public class EnemyAITransition : IComparable
    {
        [XmlSerialized] private EnemyAITransitionCondition m_Condition;
        [XmlSerialized] private int m_Source;
        [XmlSerialized] private int m_Target;
        [XmlSerialized] private int m_Priority;

        public EnemyAITransitionCondition Condition
        {
            get => m_Condition;
            set => m_Condition = value;
        }

        public int Priority
        {
            get => m_Priority;
            set => m_Priority = value;
        }

        public int Source
        {
            get => m_Source;
            set => m_Source = value;
        }

        public int Target
        {
            get => m_Target;
            set => m_Target = value;
        }

        public EnemyAITransition()
        {
            m_Source = 0;
            m_Target = 0;
            m_Condition = null;
        }

        public EnemyAITransition(int source, int target)
        {
            m_Source = source;
            m_Target = target;
            m_Condition = null;
        }

        public int CompareTo(object obj)
        {
            var other = (EnemyAITransition) obj;
            return m_Priority.CompareTo(other.m_Priority);
        }
    }
}