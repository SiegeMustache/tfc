﻿using System;
using System.Xml;
using Kairos.Enemies.AI;
using Kairos.Units;
using TorkFramework.Serialization;

namespace Kairos.Enemies
{
    [Serializable]
    public class EnemyInfo
    {
        [XmlSerialized] public string EnemyId;
        [XmlSerialized, Unit] public string Unit;
        [XmlSerialized] public EnemyAIBrain Brain;

        public EnemyInfo()
        {
            Brain = new EnemyAIBrain();
        }

        public EnemyAIBrain GetBrainInstance()
        {
            var serializer = new Serializer(new XmlWriterSettings());
            var deserializer = new Deserializer();
            var serializedBrain = serializer.SerializeToString(Brain);
            return deserializer.DeserializeFromString<EnemyAIBrain>(serializedBrain);
        }
    }
}