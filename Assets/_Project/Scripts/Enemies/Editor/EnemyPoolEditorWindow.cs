﻿using System;
using System.IO;
using Kairos.Enemies.EnemyPools;
using UnityEditor;
using UnityEngine;

namespace Kairos.Enemies.Editor
{
    public class EnemyPoolEditorWindow : EditorWindow
    {
        public string m_SelectedPoolPath;
        private Vector2 m_ScrollPosition;
        public EnemyPoolInfo SelectedPool;

        [MenuItem("Custom/Enemies/Pools/Reload")]
        public static void ReloadEnemyPoolsDatabase()
        {
            EnemyPoolDatabase.Reload();
        }

        [MenuItem("Custom/Enemies/Pools/New")]
        public static void CreateNewPool()
        {
            var newPool = EnemyPoolHelper.GenerateNewEnemyPool();
            EnemyPoolHelper.SavePool(newPool, Globals._ENEMY_POOLS_FOLDER + "/newEnemyPool.enemypool");
        }

        [MenuItem("Custom/Enemies/Pools/Editor")]
        public static void Init()
        {
            var window = GetWindow<EnemyPoolEditorWindow>();
            window.name = "Enemy Pool Editor";
            window.titleContent = new GUIContent("Enemy Pool Editor");
            window.Show();
        }

        private void OnEnable()
        {
            OnSelectionChange();
        }
        
        private void OnSelectionChange()
        {
            if (Selection.objects.Length == 1 && Path.GetExtension(AssetDatabase.GetAssetPath(Selection.objects[0])).ToLower() == ".enemypool")
            {
                m_SelectedPoolPath = Application.dataPath.Substring(0, Application.dataPath.Length - 6) + AssetDatabase.GetAssetPath(Selection.objects[0]);
                SelectedPool = EnemyPoolHelper.LoadEnemyPool(m_SelectedPoolPath);

                Repaint();
            }
        }
        
        private void OnGUI()
        {
            if (SelectedPool == null)
                return;

            m_ScrollPosition = EditorGUILayout.BeginScrollView(m_ScrollPosition);
            SelectedPool.Name = EditorGUILayout.TextField("Pool Name", SelectedPool.Name);
            SelectedPool.EnemyPoolTier = EditorGUILayout.IntField("Pool Tier Level", SelectedPool.EnemyPoolTier);

            var so = new SerializedObject(this);
            var soGroup = so.FindProperty("SelectedPool");

            EditorGUILayout.LabelField("Enemies");

            var size = EditorGUILayout.IntField("Size", SelectedPool.Enemies.Length);

            if (size != SelectedPool.Enemies.Length)
            {
                Array.Resize(ref SelectedPool.Enemies, size);
                Repaint();
            }

            EditorGUI.indentLevel++;
            var soEnemies = soGroup.FindPropertyRelative("Enemies");
            for (int j = 0; j < SelectedPool.Enemies.Length; j++)
            {
                EditorGUILayout.PropertyField(soEnemies.GetArrayElementAtIndex(j));
            }

            so.ApplyModifiedProperties();

            EditorGUI.indentLevel--;

            if (GUILayout.Button("Save"))
            {
                EnemyPoolHelper.SavePool(SelectedPool, m_SelectedPoolPath);
            }

            EditorGUILayout.EndScrollView();
        }
    }
}