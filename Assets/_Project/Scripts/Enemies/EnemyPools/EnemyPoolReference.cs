﻿using TorkFramework.Serialization;

namespace Kairos.Enemies.EnemyPools
{
    public class EnemyPoolReference
    {
       [XmlSerialized] public string PoolId;
       public EnemyPoolInfo Pool => EnemyPoolDatabase.LoadedEnemyPools[PoolId];

       public EnemyPoolReference()
       {
           PoolId = string.Empty;
       }
    }
}