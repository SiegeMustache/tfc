﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Xml;
using TorkFramework.Serialization;
using UnityEditor;

namespace Kairos.Enemies.EnemyPools
{
    public static class EnemyPoolHelper
    {
        public static EnemyPoolInfo LoadEnemyPool(string path)
        {
            if (!File.Exists(path))
                throw new FileNotFoundException();

            if (Path.GetExtension(path).ToLower() != ".enemypool")
                throw new BadImageFormatException();

            var deserializer = new Deserializer();
            return (EnemyPoolInfo) deserializer.DeserializeFromFile<EnemyPoolInfo>(path);
        }
        
        public static void SavePool(EnemyPoolInfo enemyPool, string path)
        {
#if UNITY_EDITOR
            var serializer = new Serializer(new XmlWriterSettings() );
            serializer.SerializeToFile(path, enemyPool);

            AssetDatabase.Refresh();
#endif
        }
        
        private static string GenerateEnemyPoolId()
        {
            var result = default(byte[]);

            using (var stream = new MemoryStream())
            {
                using (var writer = new BinaryWriter(stream, Encoding.UTF8, true))
                {
                    writer.Write(DateTime.Now.Ticks);
                }

                stream.Position = 0;

                using (var hash = SHA256.Create())
                {
                    result = hash.ComputeHash(stream);
                }
            }

            var resultText = "";
            for(int i = 0; i< result.Length; i++)
            {
                resultText += result[i].ToString("x2");
            }

            return "enemypool-" + resultText;
        }

        public static EnemyPoolInfo GenerateNewEnemyPool()
        {
            return new EnemyPoolInfo { PoolId = GenerateEnemyPoolId()};
        }
    }
}