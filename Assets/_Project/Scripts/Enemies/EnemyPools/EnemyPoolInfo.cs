﻿using System;
using TorkFramework.Serialization;

namespace Kairos.Enemies.EnemyPools
{
    [Serializable]
    public class EnemyPoolInfo
    {
        [XmlSerialized] public string PoolId;
        [XmlSerialized] public string Name;
        [XmlSerialized, Enemy] public string[] Enemies;
        [XmlSerialized] public int EnemyPoolTier;

        public EnemyPoolInfo()
        {
            Enemies = new string[0];
            EnemyPoolTier = 0;
        }

        public void AddEnemyToPool(string enemyId)
        {
            Array.Resize(ref Enemies, Enemies.Length + 1);
            Enemies[Enemies.Length - 1] = enemyId;
        }

        public void SetPoolTier(int tierInput)
        {
            EnemyPoolTier = tierInput;
        }
    }
}