﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Kairos.Enemies
{
    public static class EnemyDatabase
    {
        private static Dictionary<string, EnemyInfo> m_LoadedEnemies;

        public static Dictionary<string, EnemyInfo> LoadedEnemies
        {
            get
            {
                if (m_LoadedEnemies == null)
                    Reload();
                return m_LoadedEnemies;
            }
        }

        public static EnemyInfo[] GetEnemiesList()
        {
            EnemyInfo[] result = new EnemyInfo[LoadedEnemies.Count];

            int counter = 0;
            foreach (var loadedEnemy in LoadedEnemies)
            {
                result[counter] = loadedEnemy.Value;
                counter++;
            }

            return result;
        }
        
        public static string[] GetIdList()
        {
            string[] result = new string[LoadedEnemies.Count];

            int counter = 0;
            foreach (var loadedEnemy in LoadedEnemies)
            {
                result[counter] = loadedEnemy.Key;
                counter++;
            }
        
            return result;
        }
        
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterAssembliesLoaded)]
        public static void Reload()
        {
            m_LoadedEnemies = new Dictionary<string, EnemyInfo>();

            var filesToLoad = Directory.GetFiles(Globals._ENEMIES_FOLDER);
            foreach (var file in filesToLoad)
            {
                if (Path.GetExtension(file).ToLower() == ".enemy")
                {
                    var newEnemy = EnemyHelper.LoadEnemy(file);
                    LoadedEnemies.Add(newEnemy.EnemyId, newEnemy);
                }
            }
        }
    }
}