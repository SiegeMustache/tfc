﻿using Kairos.Progression;
using TorkFramework;
using TorkFramework.Save;

namespace Kairos.Managers
{
    public class LevelManager : Manager
    {
        public void ShowLevelSelection()
        {
            GameManager.ChangeState(GameStates.Loading, () =>
            {
                GameManager.GetManager<ProgressionManager>().GenerateMap(() =>
                {
                    GameManager.GetManager<SaveManager>().SaveGameData(() =>
                    {
                        GameManager.ChangeState(GameStates.LevelSelection);
                    });
                });
            });
        }

        public void StartLevel()
        {
            //GameManager.GetManager<RoundManager>().StartNewRound();
        }
    }
}