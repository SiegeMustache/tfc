﻿using System.Collections;
using System.Collections.Generic;
using Kairos.Units;
using UnityEngine;

public abstract class StatusCheck
{
    public enum ComparisionType
    {
        Greater,
        Lower
    }
    
    public abstract bool CheckStatus(UnitController targetUnit);
    public abstract int GetStatusValue(UnitController targetUnit);

}
