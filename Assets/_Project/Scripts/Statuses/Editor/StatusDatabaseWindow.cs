﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using TorkFramework.Editor.Utility;
using TorkFramework.ResourceManagement;
using TorkFramework.Serialization;
using TorkFramework.Utility;
using UnityEditor;
using UnityEngine;

namespace Kairos.Statuses.Editor
{
    public class StatusDatabaseWindow : EditorWindow
    {
        [MenuItem("Custom/Status Database")]
        public static void Init() {
            StatusDatabaseWindow myWindow = (StatusDatabaseWindow) EditorWindow.GetWindow(typeof(StatusDatabaseWindow));
            myWindow.name = "Status Database";
            myWindow.titleContent = new GUIContent("Status Database");
            myWindow.Show();
        }
        
        private void OnEnable()
        {
            StatusDatabase.Load();
        }

        private void OnGUI()
        {
            EditorGUILayout.LabelField("Statuses", EditorStyles.boldLabel);
            EditorGUI.indentLevel++;

            Queue<StatusInfo> statusToRemove = new Queue<StatusInfo>();
            
            foreach (var status in StatusDatabase.Statuses)
            {
                EditorGUILayout.BeginHorizontal();
                
                EditorGUILayout.LabelField(status.ApplyCheck.GetType().Name);
                if (GUILayout.Button("X"))
                {
                    statusToRemove.Enqueue(status);
                }
                EditorGUILayout.EndHorizontal();
                EditorGUI.indentLevel++;
                
                var newDescription = (string)FieldUtils.AdaptiveField("Description", status.StatusDescription);
                if (newDescription != status.StatusDescription)
                {
                    status.StatusDescription = newDescription;
                    StatusDatabase.Save();
                }

                var newSprite = FieldUtils.AdaptiveField("Sprite", status.StatusSprite) as ResourceReference;
                if (newSprite != status.StatusSprite)
                {
                    status.StatusSprite = newSprite;
                    StatusDatabase.Save();
                }

                var newVfx = FieldUtils.AdaptiveField("VFX", status.StatusAppliedVFX) as ResourceReference;
                if (newVfx != status.StatusAppliedVFX)
                {
                    status.StatusAppliedVFX = newVfx;
                    StatusDatabase.Save();
                }
                
                BindingFlags flags = BindingFlags.Public |
                                     BindingFlags.NonPublic |
                                     BindingFlags.Static |
                                     BindingFlags.Instance;

                var type = status.ApplyCheck.GetType();
                var fields = type.GetFields(flags).Where(x => x.IsDefined(typeof(XmlSerializedAttribute), true));

                foreach (var field in fields)
                {
                    var fieldValue = field.GetValue(status.ApplyCheck);
                    var newValue = FieldUtils.AdaptiveField(field.Name, (dynamic) fieldValue);
                    if (newValue != (dynamic) fieldValue)
                    {
                        field.SetValue(status.ApplyCheck, newValue);
                        StatusDatabase.Save();
                    }
                }
                
                EditorGUI.indentLevel--;
            }

            if (statusToRemove.Count > 0)
            {
                while (statusToRemove.Count > 0)
                {
                    StatusDatabase.Statuses.Remove(statusToRemove.Dequeue());
                }
                StatusDatabase.Save();
            } 

            EditorGUI.indentLevel--;
            EditorGUILayout.Separator();
            if (GUILayout.Button("Add Status"))
            {
                var possibleTypes = TypeUtils.GetDerivedTypes(typeof(StatusCheck));
                var menu = new GenericMenu();
                foreach (var type in possibleTypes)
                {
                    var effectiveType = type;
                    menu.AddItem(new GUIContent(effectiveType.Name), false, () => OnAddStatusClicked(effectiveType));
                }
                menu.ShowAsContext();
            }
        }

        private void OnAddStatusClicked(Type newStatusType)
        {
            StatusDatabase.Statuses.Add(new StatusInfo{ApplyCheck = Activator.CreateInstance(newStatusType) as StatusCheck});
            StatusDatabase.Save();
        }
    }

}