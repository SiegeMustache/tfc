﻿using System;
using TorkFramework.ResourceManagement;
using TorkFramework.Serialization;

namespace Kairos.Statuses
{
    [Serializable]
    public class StatusInfo
    {
        [XmlSerialized] private string m_StatusDescription;
        [XmlSerialized] private ResourceReference m_StatusSprite;
        [XmlSerialized] private ResourceReference m_StatusAppliedVFX;
        [XmlSerialized] private StatusCheck m_ApplyCheck;

        public string StatusDescription
        {
            get => m_StatusDescription;
            set => m_StatusDescription = value;
        }
        
        public ResourceReference StatusSprite
        {
            get => m_StatusSprite;
            set => m_StatusSprite = value;
        }
        public ResourceReference StatusAppliedVFX
        {
            get => m_StatusAppliedVFX;
            set => m_StatusAppliedVFX = value;
        }
        public StatusCheck ApplyCheck
        {
            get => m_ApplyCheck;
            set => m_ApplyCheck = value;
        }
    }
}