﻿using Kairos.Units;
using TorkFramework.Serialization;

namespace Kairos.Statuses.Checks
{
    public class GuardModifierStatusCheck : StatusCheck
    {
        [XmlSerialized] private ComparisionType m_Comparison;

        public override bool CheckStatus(UnitController targetUnit)
        {
            if (m_Comparison == ComparisionType.Greater)
                return targetUnit.ArmorModifier > 0;
            else
                return targetUnit.ArmorModifier < 0;
        }

        public override int GetStatusValue(UnitController targetUnit)
        {
            if (m_Comparison == ComparisionType.Greater)
                return targetUnit.ArmorModifier;
            else
                return -targetUnit.ArmorModifier;
        }
    }
}