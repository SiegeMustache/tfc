﻿using Kairos.Units;
using TorkFramework.Serialization;

namespace Kairos.Statuses.Checks
{
    public class SpeedModifierStatusCheck : StatusCheck
    {
        [XmlSerialized] private StatusCheck.ComparisionType m_Comparison;
        
        public override bool CheckStatus(UnitController targetUnit)
        {
            if (m_Comparison == ComparisionType.Greater)
                return targetUnit.SpeedModifer > 0;
            else
                return targetUnit.SpeedModifer < 0;
        }

        public override int GetStatusValue(UnitController targetUnit)
        {
            if (m_Comparison == ComparisionType.Greater)
                return targetUnit.SpeedModifer;
            else
                return -targetUnit.SpeedModifer;
        }
    }

}