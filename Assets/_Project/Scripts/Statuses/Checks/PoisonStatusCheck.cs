﻿using System.Collections;
using System.Collections.Generic;
using Kairos.Units;
using UnityEngine;

namespace Kairos.Statuses.Checks
{
    public class PoisonStatusCheck : StatusCheck
    {
        public override bool CheckStatus(UnitController targetUnit)
        {
            return targetUnit.Poison > 0;
        }

        public override int GetStatusValue(UnitController targetUnit)
        {
            return targetUnit.Poison;
        }
    }
}